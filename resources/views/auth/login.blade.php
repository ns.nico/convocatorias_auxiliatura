@extends('auth.layout')
@section('content')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span12 ">
                <div class="comment-post">
                    <div class="span3 offset4 border">
                        <div class="text-center">
                            <h4 >Ingrese a su <strong>cuenta</strong></h4>
                        </div>
                        <form class="contactForm" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="span3 form-group">
                                    @if ($errors->has('email'))
                                        <div class="alert alert-error">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>{{ $errors->first('email') }}</strong> 
                                        </div>
                                    @elseif ($errors->has('password'))
                                        <div class="alert alert-error">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>{{ $errors->first('password') }}</strong> 
                                        </div>
                                    @endif
                                </div>
                                <div class="span3 form-group">
                                    <label for="inputText">Email</label>
                                    <input id="email" type="email" class="form-control"
                                        name="email"
                                        value="{{ old('email') }}" required autofocus
                                        placeholder="Email">
                                </div>

                                <div class="span3 form-group">
                                    <label for="inputSigninPassword">Contraseña</label>
                                    <input id="password" type="password" class="form-control"
                                        name="password" required
                                        placeholder="Contraseña">
                                </div>

                                <div class="span3 form-group text-center">
                                    <div class="controls mb-5">
                                        <button type="submit" class="btn btn-color">Iniciar Sesion</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span3 form-group text-center">
                                    <p class="aligncenter margintop20">
                                        No tienes una cuenta? <a href="{{ route('register') }}">Crear Cuenta</a>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span3 form-group text-center">
                                    <div class="controls">
                                        <a class="btn btn-inverse" onclick="back()" >Atras</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</section>  
@endsection


