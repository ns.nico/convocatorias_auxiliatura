<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
</head>

<body>
    <section id="subintro-cl">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="centered">
                        <a class="brand logo" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div>
        @yield('content')
    </div>

    <footer>
        <div class="verybottom">
            <div class="container">
                <div class="row">
                    <div class="span6">
                        <p>
                            &copy; Convocatorias - Todos los derechos reservados
                        </p>
                    </div>
                    <div class="span6">
                        <div class="credits">
                        Desarrollado por: <a href="">Neosoft Technologies</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    

    @include('includes.script')

</body>
</html>


