@extends('auth.layout')
@section('content')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="comment-post">
                    <div class="span5 offset3 border">
                        <div class="text-center">
                            <h4 >Crea una <strong>cuenta</strong></h4>
                        </div>
                        
                        <form class="contactForm" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="span5 form-group">
                                    @if ($errors->has('name'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                    @elseif ($errors->has('email'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    @elseif ($errors->has('password'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    @elseif ($errors->has('codeSis'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('codeSis') }}</strong>
                                    </div>
                                    @elseif ($errors->has('ci'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('ci') }}</strong>
                                    </div>
                                    @endif
                                </div>
                                <div class="span5 form-group">
                                    <label for="inputText">Nombre de usuario</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">
                                </div>
                                <div class="span5 form-group">
                                    <label for="inputEmail">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">
                                </div>
                                <div class="span3 form-group">
                                    <label>Codigo Sis</label>
                                    <input type="number" class="form-control" name="codeSis" value="{{ old('codeSis') }}" required placeholder="Codigo Sis">
                                </div>
                                <div class="span2 form-group">
                                    <label>Carnet de Identidad</label>
                                    <input type="number" class="form-control" name="ci" value="{{ old('ci') }}" required placeholder="CI">
                                </div>
                                <div class="span5 form-group">
                                    <label for="inputSignupPassword">Contraseña</label>
                                    <input  type="password" class="form-control" name="password" required placeholder="Contraseña">
                                    
                                </div>
                                <div class="span5 form-group">
                                    <label class="control-label" for="inputSignupPassword2">Confirmar Contraseña</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
                                </div>
                                <div class="span5 form-group text-center">
                                    <div class="controls ">
                                        <button type="submit" class="btn btn-color">Crear cuenta</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="control-group">
                                    <p class="aligncenter margintop20">
                                        Ya tienes una cuenta? <a href="{{ route('login') }}">Iniciar Sesion</a>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span5 form-group text-center">
                                    <div class="controls">
                                        <a class="btn btn-inverse" onclick="back()" >Atras</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
