<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet">
    <title>{{$data['title']['title']}}</title>
    
    

</head>

<body>
    @foreach ($data as $tag => $content)
        @include('pdf.' . $tag, ['content' => $content])
    @endforeach

</body>
</html>