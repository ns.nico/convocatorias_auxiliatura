<div><h2 class='subtitle'>RESULTADOS DE LAS PRUEBAS DE CONOCIMIENTO </h2></div>
{{-- {{dd($content)}} --}}
<div>
<table style="border-collapse: collapse">
    <thead>
    <tr style="padding-right: 2px">
        <th>NRO</th>
        <th>ESTUDIANTE</th>
        @foreach ($content['specification_list'] as $index=>$list_spe)
            <th style="padding-bottom: 10px;padding-top: 10px;padding-left: 2px;padding-right: 2px"><p class="verticalText">{{$list_spe->name}}</p></th>
        @endforeach
        <th><p style="font-size: 10px">NOTA FINAL</p></th>
    </tr>
    </thead>
    <tbody>
        @foreach ($content['list_students'] as $index=>$list_estudent)
                <tr>
                    <td>
                        {{$index+1}}
                    </td>
                    <td>
                        {{$list_estudent->user_name}}
                    </td>
                    @foreach ($content['specification_list'] as $list_spe)
                        @foreach ($content['notas_speci'] as $index=>$notas_esp)
                            @if ($list_spe->name == $notas_esp->spe_name && $list_estudent->user_name == $notas_esp->user_name)
                                <td style="text-align: center">
                                    {{$notas_esp->grade}}
                                    {{-- <p class="verticalText"> {{$notas_esp->spe_name}}</p></th> --}}
                                </td>
                            @endif
                        @endforeach
                    @endforeach
                    @foreach ($content['notas_desc'] as $index=>$notas_des)
                            @if ($list_estudent->user_name == $notas_des->user_name)
                            <td style="text-align: right">
                                {{$notas_des->grade}}
                            </td>
                            @endif
                    @endforeach
                </tr>
        @endforeach
    </tbody>
</table>
</div>
