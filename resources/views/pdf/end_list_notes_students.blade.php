<div><h2 class='subtitle'>1.  Notas finales </h2></div>
{{-- {{dd($content)}} --}}
<div>
<table style="border-collapse: collapse">
    <thead>
    <tr>
        <th>NRO</th>
        <th>ESTUDIANTE</th>
        {{-- <th>AUXILIATURA</th> --}}
        <th><Canvas>CONOCIMIENTO</Canvas></th>
        <th>MERITO</th>
        <th>NOTA FINAL</th>
    </tr>
    </thead>
    <tbody>
        {{-- {{dd($content)}} --}}
        @foreach ($content['conocimiento'] as $index=>$conocimiento)
            @foreach ($content['merito'] as $index=>$merito)
                @if ($conocimiento->user_name == $merito->user_name)
                    <tr>
                        <td>
                            {{$index+1}}
                        </td>
                        <td>
                            {{$conocimiento->user_name}}
                        </td>
                        <td>
                            @php
                                $nota_c = intval(($conocimiento->grade)*0.8);
                            @endphp
                            {{$nota_c}}
                        </td>
                        <td>
                            @php
                                $nota_m = intval(($merito->grade)*0.2);
                            @endphp
                            {{$nota_m}}
                        </td>
                        <td>
                            @php
                                $nota_total = $nota_c + $nota_m
                            @endphp
                            {{$nota_total}}
                        </td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    </tbody>
</table>
</div>
