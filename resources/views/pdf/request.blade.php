<div><h2 class='subtitle'>{{$index}}. REQUERIMIENTOS</h2></div>
<div>
  <table>
    <thead>
      <tr>
        <th>ITEM</th>
        <th>CANTIDAD</th>
        <th>HRS. ACADEMICAS</th>
        <th>DESTINO</th>
      </tr>
    </thead>
    <tbody>
          @foreach ($content as $item=>$data)
            <tr>
              <td>
                {{$item+1}}
              </td>
              <td>
                {{$data->quantity}}
              </td>
              <td>
                {{$data->academic_hours}}
              </td>
              <td>
                {{$data->name}}
              </td>
            </tr>
          @endforeach
    </tbody>
  </table>
</div>