<div><h2 class='subtitle'>{{$index}}. FECHAS DE LAS PRUEBAS</h2></div>
<div>
    <p>Las pruebas escritas serán sobre el contenido de la materia a la que postula y la nota de
        aprobación mayor o igual a 51.Las pruebas orales, se tomarán solo a los postulantes que hayan vencido la prueba escrita
        y de acuerdo a pertinencia y contenido de la materia a la que se postula.
        Fechas importantes a considerar</p>
        <table>
            <thead>
            <tr>
                <th>EVENTO</th>
                <th>DESCRIPCION</th>
                <th>FECHA</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($content as $data)
                    <tr>
                    <td>
                        {{$data->name}}
                    </td>
                    <td>
                        {{$data->description}}
                    </td>
                    <td>
                        @php
                            $date = explode(" ",$data->start)
                            
                        @endphp
                        {{ $date[0]}}                     
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    <ol>NOTA.- Para Introducción a la Programación y Elementos de Programación y Estructura de
        Datos, el lenguaje es Java como tecnología de aplicación en la toma de exámenes. Para
        Computación I se considera el entorno Visual Basic como tecnología de aplicación en la
        toma de exámenes.</ol>
</div>
