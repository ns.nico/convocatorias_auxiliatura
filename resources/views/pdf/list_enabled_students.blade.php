    <div><h2 class='subtitle'>1. LISTA DE ESTUDIANTES HABILITADOS</h2></div>
    <div>
    <table>
        <thead>
        <tr>
            <th>NRO</th>
            <th>NOMBRE ESTUDIANTES</th>
            <th>NOMBRE AUXILIATURA</th>
            <th>HABILITADO</th>
            {{-- <th>nota</th> --}}
        </tr>
        </thead>
        <tbody>
            @foreach ($content as $index=>$data)
                <tr>
                    <td>
                        {{$index+1}}
                    </td>
                    <td>
                        {{$data->user_name}}
                    </td>
                    <td>
                        {{$data->aux_name}}
                    </td>
                    <td>
                        @if ($data->enabled == 1)
                            Habilitado
                        @else
                            Deshabilitado
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
