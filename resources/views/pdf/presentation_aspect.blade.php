<div><h2 class='subtitle'>{{$index}}. DE LA FORMA</h2></div>
<div>
    <p>Presentación de la documentación en sobre manila cerrado y rotulado con:</p>
    <ol>
        @foreach ($content as $item)
            <li>{{$item->description}}</li>
        @endforeach
    </ol>
</div>
