<div><h2 class='subtitle'>1.  RESULTADOS DE LAS PRUEBAS DE CONOCIMIENTO </h2></div>
{{-- {{dd($content)}} --}}
<div>
<table style="border-collapse: collapse">
    <thead>
    <tr>
        <th>NRO</th>
        <th>ESTUDIANTE</th>
        {{-- <th>AUXILIATURA</th> --}}
        <th>Exam conocimiento</th>
        <th>Exam oral</th>
        <th>Total </th>
    </tr>
    </thead>
    <tbody>
        @foreach ($content['list_students'] as $index=>$list_estudent)

                <tr>
                    <td>
                        {{$index+1}}
                    </td>
                    <td>
                        {{$list_estudent->user_name}}
                    </td>
                    <td>
                        @foreach ($content['notas'] as $index=>$notas)
                            @if ($list_estudent->user_name == $notas->user_name && $notas->des_name == 'Examen de Conocimientos'  )
                                {{$notas->grade}}
                                @php
                                    $nota_conocimiento = $notas->grade;
                                @endphp
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach ($content['notas'] as $index=>$notas)
                            @if ($list_estudent->user_name == $notas->user_name && $notas->des_name == 'Examen Oral'  )
                                {{$notas->grade}}
                                @php
                                    $nota_oral  = $notas->grade;
                                @endphp
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @php
                            $nota_total = $nota_conocimiento + $nota_oral
                        @endphp
                        {{$nota_total}}
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
</div>
