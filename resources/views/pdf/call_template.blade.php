<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{ asset('css/pdf.css') }}" rel="stylesheet">
    <title>{{$header['title']['title']}}</title>
    
    

</head>

<body>
    @include('pdf.title', ['content' => $header['title']])
    @include('pdf.description', ['content' => $header['description']])
    @foreach ($data as $tag => $content)
        @include('pdf.' . $tag, ['content' => $content, 'index' => $loop->iteration])
    @endforeach

</body>
</html>