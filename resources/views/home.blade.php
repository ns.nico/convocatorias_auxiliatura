@extends('layouts.app')
@section('content')
    <section id="intro">
        <div class="jumbotron masthead">
            <div class="container">
                <!-- slider navigation -->
                <div class="sequence-nav">
                    <div class="prev">
                        <span></span>
                    </div>
                    <div class="next">
                        <span></span>
                    </div>
                </div>
                <!-- end slider navigation -->
                <div class="row">
                    <div class="span12">
                        <div id="slider_holder">
                            <div id="sequence">
                                <ul>
                                    <!-- Layer 1 -->
                                    <li>
                                        <div class="info animate-in">
                                            <h2>Convocatorias</h2><br>
                                            <h3>Publicacion de Convocatorias</h3>
                                            <p>
                                                Convocatorias de las distintas Unidades Academicas de la UMSS, para la provision de auxiliares.
                                            </p>
                                        </div>
                                        <img class="slider_img animate-in" src="{{ asset('img/slides/img-1.png') }}" alt="">
                                    </li>
                                    <!-- Layer 2 -->
                                    <li>
                                        <div class="info">
                                            <h2>Postulantes</h2><br>
                                            <h3>Registro de postulantes</h3>
                                            <p>
                                                Los postulantes podran registrarse a un item especifico de una convocatoria, 
                                                presentando los documentos especificados en la convocatoria.
                                            </p>
                                        </div>
                                        <img class="slider_img" src="{{ asset('img/slides/img-2.png') }}" alt="">
                                    </li>
                                    <!-- Layer 3 -->
                                    <li>
                                        <div class="info">
                                            <h2>Postulantes</h2><br>
                                            <h3>Habilitacion de postulantes</h3>
                                            <p>
                                                Resultados de los postulantes habilitados o inhabilitados de acuerdo a los criterios 
                                                especificados en la convocatoria.
                                            </p>
                                        </div>
                                        <img class="slider_img" src="{{ asset('img/slides/img-3.png') }}" alt="">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Sequence Slider::END-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="tagline centered">
                        <div class="row">
                            <div class="span12">
                                <div class="tagline_text">
                                    <h2>Te interesa postularte a algun item de las convocatorias</h2>
                                    <p>
                                        Informate sobre las los requisitos, fechas, requerimientos sobre las distintas convocatorias 
                                    </p>
                                </div>
                                <div class="btn-toolbar cta">
                                    @if(!Auth::guest())
                                    <a class="btn btn-large btn-color" href="{{ route('enroll') }}">
                                            <i class="icon-eye-open icon-white"></i> Ver convocatorias </a>
                                    <a class="btn btn-large btn-inverse" href="{{ route('enroll') }}">
                                            <i class="icon-star icon-white"></i> Postularse </a>
                                    @else
                                    <a class="btn btn-large btn-color" href="{{ route('login') }}">
                                            <i class="icon-eye-open icon-white"></i> Ver convocatorias </a>
                                    <a class="btn btn-large btn-inverse" href="{{ route('login') }}">
                                            <i class="icon-star icon-white"></i> Postularse </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- end tagline -->
                </div>
            </div>
        </div>
    </section>
@endsection