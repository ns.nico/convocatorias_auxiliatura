<div id="modal-public-{{$call->id}}" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySigninModalLabel" aria-hidden="true">
    <form action="{{route ('public_call',$call->id)}}" method="post"> 
    {{ csrf_field()}}
    {{ method_field('PUT') }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="mySigninModalLabel">Publicar  <strong>convocatoria</strong></h3>
    </div>
    <div class="modal-body">
        <h4>Esta seguro de publicar esta convocatoria: </h4>
        <p>Al publicar una convocatoria ya no se podran realizar ediciones, 
           y seran visibles para todos los usuarios</p>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-color left">Si, publicar</button>
        <button class="btn btn-inverse right" data-dismiss="modal">Cancelar</button>
    </div>
    </form>
</div>