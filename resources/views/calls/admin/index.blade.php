@extends('calls.layout')
@section('section')
<div class="span8">
    @if(session('message'))
    <div class="alert alert-success" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{ session('message') }}</strong>
    </div>
    @endif
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="create-call">
                    @include('calls.admin.tabs.index_tabs.create_call')
                </div>

                <div class="tab-pane " id="all-calls">
                    @include('calls.admin.tabs.index_tabs.all_calls')
                </div>

                <div class="tab-pane " id="public-calls">
                    @include('calls.admin.tabs.index_tabs.public_calls')
                </div>

                <div class="tab-pane" id="list_users">
                    @include('calls.admin.tabs.index_tabs.list_users')
                </div>

                <div class="tab-pane" id="commission">
                    @include('calls.admin.tabs.index_tabs.commission')
                </div>

                <div class="tab-pane" id="my-commissions">
                    @include('calls.admin.tabs.index_tabs.my_commissions')
                </div>
            </div>
        </div>
    </article>
</div>
@endsection
