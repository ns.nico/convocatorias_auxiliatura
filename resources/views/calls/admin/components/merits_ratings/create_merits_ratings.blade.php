@extends('calls.layout')
@section('section')
<div class="span8">
<article class="blog-post">
    <form action="{{ route('merit_store')}}" method="POST" class="comment-form" name="comment-form">
        {{ csrf_field()}}

        <input type="hidden" name="call_type" value="{{ $call_type }}">
        <div class="post-heading">

            <input type="text" style="width : 300px " name="merit_ratings_name" value="{{ $merits->name }}">
            </div>
            <div class="span7">

                @foreach ( $availableMeritRating as $Available)
                    @if ($merits->id == $Available->merit_ratings_id)
                    <input type="hidden" name="type" value=" {{$Available->call_types_id}} ">
                    @endif
                @endforeach
                <div class="span7">
                    <textarea rows="2" name='merit_ratings_descrip'class="input-block-level" style="text-align: justify" >{{$merits->description}}</textarea>
                </div>

                <div class="span7">
                    <table class="tableMerit">
                        <tr class="myDescription">
                            <td > <b class="white">Descripcion de meritos </b></td>
                            <td class="Porcentaje"><b class="white">Porcentaje </b> </td>
                        </tr>

                        @foreach($availableDescription as $desc)
                            @if($desc->merit_ratings_id == $merits->id )
                                @if($desc->description()->percentage == 0)
                                    <tr>
                                        <input type="hidden" name="defec_description[]" value="{{ $desc->description()->name }}">
                                        <input type="hidden" name="defec_percentage[]" value="{{$desc->description()->percentage}}">
                                        <td colspan="2" class="myDescription">
                                            <b class="white"><input type="text"  style="width : 500px " name="act_descrip[]" value="{{$desc->description()->name}}"></b>
                                            <input type="hidden" name="act_perce[]" value="{{$desc->description()->percentage}}">
                                        </td>

                                        <input type="hidden" name="descriptions_available_desc_id[]" value="{{ $desc->description()->id }}">
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="span6">
                                                @foreach($availableEspecifications as $esp)
                                                    @if($esp->description_metirs_id == $desc->description_metirs_id && $esp->merits_id == $merits->id)
                                    {{-- aqui se agrego unno nuevo --}}<input type="hidden" value="{{ $esp->especification()->id }}" name="availableEspecification_esp_id[]">
                                                    <input type="hidden" value="{{ $esp->description_metirs_id }}" name="availableEspecification_desc_id[]">
                                                    <input type="hidden" name="defec_esp_description[]" value="{{ $esp->especification()->name }}">
                                                    <input type="hidden" name="defec_esp_percentage[]" value="{{$esp->especification()->percentage}}">
                                                    <textarea rows="3" name='act_esp_descrip[]'class="input-block-level" style="text-align:justify;width : 600px" >{{$merits->description}}</textarea>
                                                    <input type="hidden" name="act_esp_perce[]" style="width : 50px " value="{{$esp->especification()->percentage}}">
                                                    @endif
                                                @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                @else
                                <tr>
                                    <input type="hidden" name="defec_description[]" value="{{ $desc->description()->name }}">
                                    <input type="hidden" name="defec_percentage[]" value="{{$desc->description()->percentage}}">
                                    <td ><b><input type="text"  style="width : 500px " name="act_descrip[]" value="{{$desc->description()->name}}"></b></td>
                                    <td class="Porcentaje-desc"><input type="text" name="act_perce[]" style="width : 80px " value="{{$desc->description()->percentage}}"></td>
                                    <input type="hidden" name="descriptions_available_desc_id[]" id="des" value="{{ $desc->description()->id }}">
                                </tr>

                                    <tr>
                                        <td colspan="2">
                                            <ol id="description" class="m">
                                                @foreach($availableEspecifications as $esp)
                                                @if($esp->description_metirs_id == $desc->description_metirs_id && $esp->merits_id == $merits->id)
                                                <input type="hidden" value="{{ $esp->especification()->id }}" name="availableEspecification_esp_id[]">
                                                <input type="hidden" value="{{ $esp->description_metirs_id }}" name="availableEspecification_desc_id[]">
                                                    <li style="font-weight:bold;" >
                                                        <input type="hidden" name="defec_esp_description[]" value="{{ $esp->especification()->name }}">
                                                        <input type="hidden" name="defec_esp_percentage[]" value="{{$esp->especification()->percentage}}">
                                                        <input type="text"  style="width : 300px " name="act_esp_descrip[]" value="{{ $esp->especification()->name }}">
                                                        ...................
                                                        <input type="text" name="act_esp_perce[]" style="width : 50px " value="{{$esp->especification()->percentage}}">
                                                        <ul>
                                                            @foreach($availablePoints as $point)
                                                            @if($point->especification_id == $esp->especifications_id && $point->merits_id == $merits->id)
                                                            <input type="hidden" value="{{ $point->points()->id }}" name="available_points_id[]">
                                                            <input type="hidden" value="{{ $point->especification_id }}" name="availablePoints_esp_id[]">
                                                            <li style="font-weight:100 ">
                                                                    <input type="hidden" name="default_points[]" value="{{ $point->points()->points }}">
                                                                    <input type="hidden" name="default_points_description[]" value="{{ $point->points()->description }}">
                                                                    <input type="text"  style="width : 50px " name="act_points[]" value="{{ $point->points()->points }}">
                                                                    ptos/
                                                                    <input type="text" name="act_points_description[]" style="width : 300px " value="{{ $point->points()->description }}">
                                                                </li>
                                                            @endif
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @endif
                                                @endforeach
                                            </ol>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </table>

                </div>
                <div class="span7">
                    <div class=" to-right">
                        <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Crear tablas </button>
                    </div>
                </div>
            </div>

    </form>
</article>
</div>
@endsection
