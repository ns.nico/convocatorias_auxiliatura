@extends('calls.layout')
@section('section')
<div class="span8">
<article class="blog-post">
    <div class="comment-post">
        <form action="{{ route('request_store')}}" method="POST" class="comment-form" name="comment-form">
            {{ csrf_field()}}
            <div class="tabbable tabs-left">
                <div class="tab-content">
                    <div class="tab-pane active" id="main">
                        <div class="post-heading">
                            <h3>Nuevo Requerimiento</h3>
                            <input style="display:none" name="call_type" value="{{$call_type}}">
                        </div>
                        <div class="span7">
                            @if ($errors->has('name'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="span7">
                            <label for="inputText">Nombre de la Materia </label>
                            <input id="name" type="text" class="input-block-level" name="name" value="{{ old('name') }}" placeholder="Nombre de la materia" >
                        </div>
                        <div class="span7">
                            @if ($errors->has('code'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('code') }}</strong>
                                </div>
                            @elseif ($errors->has('quantity'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('quantity') }}</strong>
                                </div>
                            @elseif ($errors->has('academic_hours'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('academic_hours') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="span3">
                            <label for="inputText">Codigo de la Materia </label>
                            <input id="code" type="text" class="input-block-level" name="code" value="{{ old('code') }}" placeholder="Codigo" >
                        </div>
                        <div class="span2">
                            <label for="inputText">Cantidad de auxliares</label>
                            <input id="quantity" type="number" class="input-block-level" name="quantity" value="{{ old('quantity') }}"  list="maximos"
                            min="1" max="20" step="1">    
                        </div>
                        <div class="span2">
                            <label for="inputText">Horas/mes </label>
                            <input id="academic_hours" type="number" class="input-block-level" name="academic_hours" value="{{ old('academic_hours') }}" 
                            list="maximos" min="1" max="100" step="1">
                        </div>
                        <div class="span7">
                            <div class=" to-left">
                                <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                            </div>
                            <div class=" to-right">
                                <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Crear Requerimiento </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</article>
</div>
@endsection


