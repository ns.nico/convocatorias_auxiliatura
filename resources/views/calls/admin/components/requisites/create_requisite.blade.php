@extends('calls.layout')
@section('section')
<div class="span8">
<article class="blog-post">
    <div class="comment-post">
        <form action="{{ route('requisite_store')}}" method="POST" class="comment-form" name="comment-form">
            {{ csrf_field()}}
            <div class="tabbable tabs-left">
                <div class="tab-content">
                    <div class="tab-pane active" id="main">
                        <div class="post-heading">
                            <h3>Nuevo Requisito</h3>
                            <input style="display:none" name="call_type" value="{{$call_type}}">
                        </div>
                        <div class="span7">
                            @if ($errors->has('name'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="span7">
                            <label for="inputText">Nombre</label>
                            <input id="name" type="text" class="input-block-level" name="name" value="{{ old('name') }}"  placeholder="Nombre del requisito">
                        </div>
                        <div class="span7">
                            @if ($errors->has('description'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="span7">
                            <label for="inputText">Descripcion</label>
                            <textarea rows="3" name='description'class="input-block-level"  placeholder="Descripcion del requisito">{{ old('description') }}</textarea>
                        </div>
                        <div class="span7">
                            <div class=" to-left">
                                <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                            </div>
                            <div class=" to-right">
                                <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Crear Requisito </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</article>
</div>
@endsection
