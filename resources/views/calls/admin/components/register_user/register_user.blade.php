@extends('calls.layout')
@section('section')
<div class="span8">
<article class="blog-post">
    <div class="comment-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">

                <div class="tab-pane active" id="create-user">

                    <div class="post-heading">
                        <h3>Registrar Nuevos Usuarios</h3>
                    </div>
                    <div class="post-body">
                        <form action="{{ route('roll_store') }}" method="post" class="comment-form" name="comment-form">
                            {{ csrf_field() }}
                            {{-- alertas --}}
                            <div class="span6 form-group">
                                @if ($errors->has('name'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                                @elseif ($errors->has('email'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                                @elseif ($errors->has('password'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                                @endif
                            </div>
                            {{-- formulario --}}
                            <div class="span7">
                                <label for="">Nombre <span>*</span></label>
                                <input  type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">
                            </div>
                            <div class="span7">
                                <label>Correo Electronico <span>*</span></label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">
                            </div>

                            <div id='codeSis' class="span4 form-group" style="display: none">
                                <label for="inputEmail">Codigo Sis</label>
                                <input id="codigoSIS" type="number" class="form-control" name="codeSis" value="{{ old('codeSis') }}" placeholder="Codigo Sis" min="100000000" max="999999999">
                            </div>
                            <div id="ci" class="span3 form-group" style="display: none">
                                <label for="inputEmail">Carnet de Identidad</label>
                                <input id="CI" type="number" class="form-control" name="ci" value="{{ old('ci') }}" placeholder="CI" min="100000" max="9999999999">
                            </div>

                            <div class="span3">
                                <label for="">Contraseña  <span>*</span></label>
                                <input  type="password" class="form-control" name="password" required placeholder="Contraseña">
                            </div>
                            <div class="span3">
                                <label for="">Confirmar contraseña  <span>*</span></label>
                                <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
                            </div>

                            <div class="span3">
                                <label>Seleccione el Roll </label>
                                <select name="role_de_user" onchange="onChangeRole(this.value)">
                                    @foreach ( $rolls as $roll)
                                        <option value="{{ $roll->id }}"> {{ $roll->rol }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span7">
                                <div class=" to-left">
                                    <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-remove icon-white"></i> Cancelar </a>
                                </div>
                                <div class=" to-right">
                                    <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Asignar </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</article>
</div>
@endsection


