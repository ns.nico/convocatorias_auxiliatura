@extends('calls.layout')
@section('section')
<div class="span8">
    @if(session('message'))
    <div class="alert alert-success" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{ session('message') }}</strong>
    </div>
    @endif
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="items-call">
                    @include('calls.admin.tabs.selected_call_tabs.items')
                </div>
            </div>
        </div>
    </article>
</div>
@endsection
