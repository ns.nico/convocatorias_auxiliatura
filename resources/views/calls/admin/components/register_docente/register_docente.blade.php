@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="tab-pane " id="Registrar-teachers">
                        <div class="post-heading">
                            <h3>Registrar Nuevos Docentes</h3>
                        </div>
                            <form action="{{ route('roll_store') }}" method="post" >
                            {{ csrf_field() }}
                                {{-- alertas --}}
                                <div class="span6 form-group">
                                    @if ($errors->has('name'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </div>
                                    @elseif ($errors->has('email'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    @elseif ($errors->has('password'))
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    @endif
                                </div>
                            {{-- formulario --}}
                                <div class="span3">
                                    <label for="">Nombre <span>*</span></label>
                                    <input  type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre">
                                </div>
                                <div class="span3">
                                    <label for="">Contrasenia  <span>*</span></label>
                                    <input  type="password" class="form-control" name="password" required placeholder="Contraseña">
                                </div>
                                <div class="span3">
                                    <label for="">Confirmar contrasenia  <span>*</span></label>
                                    <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirmar Contraseña">
                                </div>
                                <div class="span6">
                                    <label>Correo Electronico <span>*</span></label>
                                    <input type="email" class="form-control" style="width : 500px" name="email" value="{{ old('email') }}" required placeholder="Email">
                                </div>
                                <div class="span6">
                                    <label>Seleccione el Roll </label>
                                    <select name="role_de_user">
                                        @foreach ( $rolls as $roll)
                                            <option value="{{ $roll->id }}"> {{ $roll->rol }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span7">
                                    <div class=" to-right">
                                        <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Asignar </button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
@endsection
