<div class="post-heading">
    <h3>Comisiones de Conocimiento</h3>
</div>
<div class="post-body">
    @if(count($commissions_knowledge) == 0  && count($commissions_knowledge_lab) == 0 &&
        count($commissions_knowledge_nl) == 0  && count($commissions_knowledge_lab_nl) == 0)
    <div class="">
        <div class="alert alert-info" >
            <strong> No ha sido asignado a ninguna comision de conocimiento </strong>
        </div>
    </div>
    @else
    <table class="table table-striped table-hover">
        <thead>
            <tr>
            <th>
                Nombre
            </th>
            <th>
                Gestion
            </th>
            <th>
                Item
            </th>
            <th>
                Opciones
            </th>
            </tr>
        </thead>
        <tbody>
            @foreach($commissions_knowledge_lab as $ck)
            <tr>
            <td>
                {{ $ck->call_name }}
            </td>
            <td>
                {{ $ck->management }}-{{$ck->year}}
            </td>
            <td>
                {{ $ck->name}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                <a class="btn btn-small btn-color" href="{{ route('item_rating',[$ck->call_id, $ck->request_id]) }}"><i class="icon-pencil icon-white"></i> Calificar </a>
                </div>
            </td>
            </tr>
            @endforeach
            
            @foreach($commissions_knowledge as $ck)
            <tr>
            <td>
                {{ $ck->call_name }}
            </td>
            <td>
                {{ $ck->management }}-{{$ck->year}}
            </td>
            <td>
                {{ $ck->name}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('qualification_k', $ck->request_id) }}"> Postulantes</a>
                </div>
            </td>
            </tr>
            @endforeach

            @foreach($commissions_knowledge_lab_nl as $ck)
            <tr>
            <td>
                {{ $ck->call_name }}
            </td>
            <td>
                {{ $ck->management }}-{{$ck->year}}
            </td>
            <td>
                {{ $ck->name}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                <a class="btn btn-small btn-color" href="{{ route('commission_show',$ck->id) }}"><i class="icon-pencil icon-white"></i> Mas ... </a>
                </div>
            </td>
            </tr>
            @endforeach
            
            @foreach($commissions_knowledge_nl as $ck)
            <tr>
            <td>
                {{ $ck->call_name }}
            </td>
            <td>
                {{ $ck->management }}-{{$ck->year}}
            </td>
            <td>
                {{ $ck->name}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('commission_show',$ck->id) }}"> Mas ...</a>
                </div>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
<hr>
<div class="post-heading">
    <h3>Comisiones de Meritos</h3>
</div>
<div class="post-body">
    @if(count($commissions_merits) == 0 && count($commissions_merits_nl) == 0)
    <div class="">
        <div class="alert alert-info" >
            <strong> No ha sido asignado a ninguna comision de meritos </strong>
        </div>
    </div>
    @else
    <table class="table table-striped table-hover">
        <thead>
            <tr>
            <th>
                Nombre
            </th>
            <th>
                Gestion
            </th>
            <th>
                Opciones
            </th>
            </tr>
        </thead>
        <tbody>
            @foreach($commissions_merits as $cm)
            <tr>
            <td>
               {{ $cm->call_name}}
            </td>
            <td>
              {{ $cm->management }}-{{$cm->year}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('call_options',$cm->call_id) }}"> Opciones </a>
                    <!-- <a class="btn btn-small btn-color" href="{{ route('merit_qualification',$cm->call_id) }}"><i class="icon-pencil icon-white"></i> Calificar </a> -->
                </div>
            </td>
            </tr>
            @endforeach

            @foreach($commissions_merits_nl as $cm)
            <tr>
            <td>
               {{ $cm->call_name}}
            </td>
            <td>
              {{ $cm->management }}-{{$cm->year}}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('commission_show',$cm->id) }}"> Mas ... </a>
                </div>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
