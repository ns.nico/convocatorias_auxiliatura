<div class="post-heading">
    <h3>Lista de Usuarios</h3>
</div>
<div class="row">
    <div class="span6 btn-toolbar cta">
        <a class="btn btn-small btn-color" href="{{ route('roll') }}"> <i class="icon-plus icon-white"></i> Nuevo Usuario </a>
    </div>
</div>
<div class="post-body">
    @if(count($users)==0)
        <div class="alert alert-info">
            <strong>No se registro ningun usuario</strong>
        </div>
    @else
    <table class="table table-striped table-hover">
        <thead>
            <tr>
            <th>
                Nombre
            </th>
            <th>
                Email
            </th>
            <th>
                Rol
            </th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
            <td>
                {{ $user->name }}
            </td>
            <td>
                {{ $user->email }}
            </td>
            <td>
                {{ $user->role }}
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
<div class="row">
    <div class="pagination pagination-centered">
        <ul class="pager">
            {{$users->render()}}
        </ul>
    </div>
</div>
