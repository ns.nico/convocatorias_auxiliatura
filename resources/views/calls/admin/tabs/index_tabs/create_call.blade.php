<div class="post-heading">
    <h3>Crear nueva convocatoria</h3>
</div>
<div class="row-fluid">
    <ul class="portfolio-area da-thumbs">
        <li class="span4">
            <form action="{{url('call')}}" method="get" class="comment-form" name="comment-form">
                <div class="thumbnail">
                    <div class="image-wrapp">
                        <img src="{{ asset('/img/convocatorias/conv-1.png') }}"/>
                        <article class="da-animate da-slideFromRight" style="display: block;">
                            <h4>Nuevo</h4>
                            <input type="text" style="display:none" name="type" value="0">
                            <div class="btn-toolbar cta">
                                <button class="btn btn-large btn-color" type="submit"><i class="icon-pencil icon-white"></i> Crear </button>
                            </div>
                        </article>
                    </div>
                    <label><strong>En blanco</strong></label>
                </div>
            </form>
        </li>
        @foreach($templates as $template)
        <li class="span4">
            <form action="{{url('call')}}" method="get" class="comment-form" name="comment-form">
                <div class="thumbnail">
                    <div class="image-wrapp">
                        <img src="{{ asset('/img/convocatorias/conv-1.png') }}"/>
                        <article class="da-animate da-slideFromRight" style="display: block;">
                            <h4>{{ $template->template_name }}</h4>
                            <input type="text" style="display:none" name="type" value="{{ $template->call_type_id }}">
                            <div class="btn-toolbar cta">
                                <button class="btn btn-large btn-color" href="{{ route('call') }}"><i class="icon-pencil icon-white"></i> Crear </button>
                            </div>
                        </article>
                    </div>
                    <label><strong>{{ $template->template_name }}</strong> :: plantilla</label>
                </div>
            </form>
        </li>
        @endforeach
    </ul>
</div>