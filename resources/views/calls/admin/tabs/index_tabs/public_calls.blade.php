<div class="post-heading">
    <h3>Lista de Convocatorias publicas</h3>
</div>
<div class="post-body">
    @if(count($public_calls)==0)
        <div class="alert alert-info">
            <strong>No se publico ninguna convocatoria</strong>
        </div>
    @else
    <table class="table table-striped table-hover">
        <thead>
            <tr>
            <th>
                Nombre
            </th>
            <th>
                Gestion
            </th>
            <th>
                Unidad Academica
            </th>
            <th>
                Opciones
            </th>
            </tr>
        </thead>
        <tbody>
            @foreach($public_calls as $call)
            <tr>
            <td>
                {{ $call->name }}
            </td>
            <td>
                {{ $call->management }} - {{ $call->year }}
            </td>
            <td>
                {{ $call->academic_unit()->name }}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('pdf_download',$call->id) }}"> PDF  </a>
                </div>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('pdf_list_download',$call->id) }}"> Habilitados </a>
                </div>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('seecall',$call->id) }}"> Notas </a>
                </div>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

<div class="row">
    <div class="pagination pagination-centered">
        <ul class="pager">
            {{$public_calls->render()}}
        </ul>
    </div>
</div>
