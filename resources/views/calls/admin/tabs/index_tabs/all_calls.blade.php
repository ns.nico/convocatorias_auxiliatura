<div class="post-heading">
    <h3>Lista de Convocatorias pendientes</h3>
</div>
<div class="post-body">
    @if(count($all_calls)==0)
        <div class="alert alert-info">
            <strong>No se creo ninguna convocatoria</strong>
        </div>
    @else
    <table class="table table-striped table-hover">
        <thead>
            <tr>
            <th>
                Nombre
            </th>
            <th>
                Gestion
            </th>
            <th>
                Unidad Academica
            </th>
            <th>
                Opciones
            </th>
            </tr>
        </thead>
        <tbody>
            @foreach($all_calls as $call)
            <tr>
            <td>
                {{ $call->name }}
            </td>
            <td>
                {{ $call->management }} - {{ $call->year }}
            </td>
            <td>
                {{ $call->academic_unit()->name }}
            </td>
            <td>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('pdf_download',$call->id) }}"> PDF </a>
                </div>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-color" href="{{ route('call_edit',$call->id) }}"> Editar </a>
                </div>
                <div class="btn-toolbar cta">
                    <a class="btn btn-small btn-inverse" href="" 
                                data-target="#modal-public-{{$call->id}}" 
                                data-toggle="modal"> Publicar </a>
                </div>
            </td>
            </tr>
            @include('calls.admin.modals.public_call_modal')
            @endforeach
        </tbody>
    </table>
    @endif
</div>
<div class="row">
    <div class="pagination pagination-centered">
        <ul class="pager">
            {{$all_calls->render()}}
        </ul>
    </div>
</div>
