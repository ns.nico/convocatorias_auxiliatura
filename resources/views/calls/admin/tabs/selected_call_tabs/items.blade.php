<div class="post-heading">
    <h3>Items de la Convocatoria</h3>
</div>
<div class="post-body">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>
                <th>
                    Codigo
                </th>
                <th>
                    Notas Conocimiento
                </th>
                <th>
                    Notas Finales
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
            <tr>
                <td>
                    {{ $item->name }}
                </td>
                <td>
                    {{ $item->code }}
                </td>
                <td style="text-align: center">
                    @if ( $item->call_type_id == 1)
                        <a class="btn btn-small btn-color" href="{{ route('pdf_knowledge_results_list_doc',[$item->call_id,$item->id_auxi]) }}"> Notas </a>
                    @else
                        <a class="btn btn-small btn-color" href="{{ route('pdf_knowledge_results_list_lab',[$item->call_id,$item->id_auxi]) }}"> Notas </a>
                    @endif
                </td>
                <td style="text-align: center">
                    <a class="btn btn-small btn-color" href="{{ route('pdf_end_note',[$item->call_id,$item->id_auxi,$item->call_type_id]) }}"> Notas finales</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
