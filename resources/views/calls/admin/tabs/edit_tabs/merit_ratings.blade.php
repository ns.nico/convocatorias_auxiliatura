<select class="span7" name="merito_disponible" id="mySelect" onchange="myFunction()">
    @foreach ( $availableMeritRatings_select as $me)
        @foreach ($merits_ratings as $merits)
            @if ($me->merit_ratings_id == $merits->id)
                <option value="{{ $me->id }}"> {{ $me->merits()->name }}</option>
            @endif
        @endforeach
    @endforeach
</select>

@php
    if ($call_type==0) {
        $type = 1;
    }else {
        $type = $call_type;
    }
@endphp

<div id="padre">
    @foreach ( $availableMeritRatings_select as $me)
            @foreach ( $merits_ratings as $merits)
                @if ($merits->id ==  $me->merit_ratings_id )
                    {{-- asignar id dinamica --}}
                    <div id="new{{ $me->id }}" {{($merits->id == $type)? "style= display:block": "style= display:none"}}>
                        <div class="post-heading">
                            <h3>{{ $merits->name }}</h3>
                            </div>
                        <div class="span7">
                            {{$merits->description}}
                        </div>
                        <div class="span7">
                            <br>
                            <table class="tableMerit">
                                <tr class="myDescription">
                                    <td > <b class="white">Descripcion de meritos </b></td>
                                    <td class="Porcentaje"><b class="white">Porcentaje </b> </td>
                                </tr>
                                @foreach($availableDescription as $desc)
                                    @if($desc->merit_ratings_id == $merits->id )
                                        @if($desc->description()->percentage == 0)
                                            <tr>
                                                <td colspan="2" class="myDescription">
                                                    <b class="white">{{$desc->description()->name}}</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="span6">
                                                        @foreach($availableEspecifications as $esp)
                                                            @if($esp->description_metirs_id == $desc->description_metirs_id && $esp->merits_id == $merits->id)
                                                            {{ $esp->especification()->name}}
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </td>
                                            </tr>
                                        @else
                                        <tr>
                                            <td ><b>{{$desc->description()->name}}</b></td>
                                            <td class="Porcentaje-desc">{{$desc->description()->percentage}}</td>
                                        </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <ol id="description" class="m">
                                                        @foreach($availableEspecifications as $esp)
                                                        @if($esp->description_metirs_id == $desc->description_metirs_id && $esp->merits_id == $merits->id)
                                                            <li style="font-weight:bold;" >
                                                                {{ $esp->especification()->name }}
                                                                ...................
                                                                {{$esp->especification()->percentage}}
                                                                <ul>
                                                                    @foreach($availablePoints as $point)
                                                                    @if($point->especification_id == $esp->especifications_id && $point->merits_id == $merits->id)
                                                                        <li style="font-weight:100 ">
                                                                            {{ $point->points()->points }} ptos/ {{ $point->points()->description }}
                                                                        </li>
                                                                    @endif
                                                                    @endforeach
                                                                </ul>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ol>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </table>
                                {{--  @php
                                    if ($call_type==0) {
                                        $merit_id = 1;
                                    }else {
                                        $merit_id = $call_type;
                                    }

                                    @endphp

                                <label for="">Opciones</label>
                                <div class="btn-toolbar cta">
                                    <a class="btn btn-small btn-color" href="{{ route('merits',[$call_type,$merit_id]) }}"><i class="icon-pencil icon-white"></i> Editar tabla de meritos</a>
                                </div> --}}
                        </div>
                    </div>
                @endif
            @endforeach
    @endforeach
</div>



<div class="span7" style="margin-top: 20px">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem();comparar()" href="#document-requireds"  data-toggle="tab" ><i class="icon-arrow-left icon-white"></i> Anterior </a>
    </div>
    <div class=" to-right">
        <a class="btn btn-large btn-color" onClick="nextItem()" href="#events"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente </a>
    </div>
</div>

