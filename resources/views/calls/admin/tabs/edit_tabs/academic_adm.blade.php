<div class="span7">
    @if ($errors->has('management') || $errors->has('year'))
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <ul>
                <li><strong>{{ $errors->first('management') }}</strong></li>
                <li><strong>{{ $errors->first('year') }}</strong></li>
            </ul>
        </div>
    @endif
</div>

<div class="post-heading">
    <h3>Gestion y Unidad Academica</h3>
</div>

<div class="span3">
    <label for="">Periodo <span>*</span></label>
    <input type="number" class="input-block-level" name="management" min="1" max="4" value="{{$management}}"></input>
</div>
<div class="span3">
    <label for="">Año <span>*</span></label>
    <input type="number" class="input-block-level" name="year" min="2018" max="2100" value="{{$year}}"></input>
</div>
<div class="span6">
    <label>Unidad Academica <span>*</span></label>
    @if(count($academic_units)==0)
    <div class="alert alert-error">
        <strong>Ninguna unidad academica registrada</strong> 
    </div>
    @else
    <select  class="input-block-level" name="academic_unit">
        <option class="input-block-level" value="{{ $call_academic->id}}">{{$call_academic->name}}</option>
        @foreach($academic_units as $unit)
            @if($unit != $call_academic)
            <option class="input-block-level" value="{{ $unit->id}}">{{$unit->name}}</option>
            @endif
        @endforeach
    </select>
    @endif
</div>
<div class="span7">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem()" href="#name-description"  data-toggle="tab" ><i class="icon-arrow-left icon-white"></i> Anterior </a>
    </div>
    <div class=" to-right">
        <a class="btn btn-large btn-color" onClick="nextItem()" href="#requests"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente  </a>
    </div>
</div>