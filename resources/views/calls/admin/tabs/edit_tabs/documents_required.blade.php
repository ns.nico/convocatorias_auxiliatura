<div class="post-heading">
    <h3>Documentos Requeridos</h3>
</div>
<div class="span7">
    @if(count($documents)==0 && count($call_documents)==0)
    <div class="alert alert-error">
        <strong>Ningun documento registrado</strong>
    </div>
    @else
    <label for="">Documentos que estaran en la convocatoria</label>
        <div id="accepted">
            @foreach($call_documents as $doc)
            <div id='documents{{$doc->id}}' class="alert-mdf alert-success"
                onClick='off("documents{{$doc->id}}","documents","{{$doc->id}}")'>
                <a class="close" data-dismiss=""><i class="icon-mdf-s icon-minus"></i></a>
                <input type="hidden" name="documents[]" value="{{$doc->id}}">
                <strong>{{ $doc->description }}</strong>
            </div>
            @endforeach
        </div>
        <hr>
    <label for="">Documentos que estan fuera de la convocatoria</label>
        <div id="rejected">
        @foreach($documents as $doc)
        <div id='documents{{$doc->id}}' class="alert-mdf alert-error"
            onClick='on("documents{{$doc->id}}","documents","{{$doc->id}}")'>
            <a class="close" data-dismiss=""><i class="icon-mdf-e icon-plus"></i></a>
            <strong>{{ $doc->description }}</strong>
        </div>
        @endforeach
        </div>
    @endif
    <hr>
    <label for="">Opciones</label>
    <div class="btn-toolbar cta">
        <a class="btn btn-small btn-color" href="{{ route('document',$call_type) }}"><i class="icon-pencil icon-white"></i> Nuevo Documento</a>
    </div>
</div>
<div class="span7">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem()" href="#requisites"  data-toggle="tab" ><i class="icon-arrow-left icon-white"></i> Anterior </a>
    </div>
    <div class=" to-right">
        <a class="btn btn-large btn-color" onClick="nextItem()" href="#merit_ratings"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente </a>
    </div>
</div>
