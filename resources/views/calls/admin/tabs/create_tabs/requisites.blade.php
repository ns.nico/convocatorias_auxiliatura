<div class="post-heading">
    <h3>Requisitos</h3>
</div>
<div class="span7">
    @if(count($requisites)==0)
    <div class="alert alert-error">
        <strong>Ningun requisito registrado</strong> 
    </div>
    @else
    <label for="">Requisitos que estaran en la convocatoria</label>
        <div id="accepted"></div>
        <hr>
    <label for="">Requisitos que estan fuera de la convocatoria</label>
        <div id="rejected">
            @foreach($requisites as $req)
            <div id='requisites{{$req->id}}' class="alert-mdf alert-error" 
                onClick='on("requisites{{$req->id}}","requisites","{{$req->id}}")'>
                <a class="close" ><i class="icon-mdf-e icon-plus"></i></a>
                <strong>{{ $req->description }}</strong>
            </div>
            @endforeach
        </div>
    @endif
    <label for="">Opciones</label>
    <div class="btn-toolbar cta">
        <a class="btn btn-small btn-color" href="{{ route('requisite',$call_type) }}"><i class="icon-pencil icon-white"></i> Nuevo Requisitos</a>
    </div>
</div>
<div class="span7">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem()" href="#requests"  data-toggle="tab" ><i class="icon-arrow-left icon-white"></i> Anterior </a>
    </div>
    <div class=" to-right">
        <a class="btn btn-large btn-color" onClick="nextItem()" href="#document-requireds"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente </a>
    </div>
</div>