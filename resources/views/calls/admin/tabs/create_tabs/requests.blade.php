<div class="post-heading">
    <h3>Requerimientos</h3>
</div>
<div class="span7">
    @if(count($requests)==0)
    <div class="alert alert-error">
        <strong>Ningun requerimiento registrado</strong>
    </div>
    @else
    <label for="">Requerimientos que estaran en la convocatoria</label>
        <div id="accepted">
        </div>
        <hr>
    <label for="">Requerimientos que estan fuera de la convocatoria</label>
        <div id="rejected">
            @foreach($requests as $req)
            <div id='requests{{$req->id}}' class="alert-mdf alert-error"
                onClick='on("requests{{$req->id}}","requests",{{$req->id}})'>
                <a  class="close" ><i class="icon-mdf-e icon-plus"></i></a>
                <div class="elem-1"><strong>{{ $req->quantity }} Aux. </strong></div>
                <div class="elem-2"><strong>{{ $req->academic_hours }} Horas/Mes</strong></div>
                <div class="elem-1"><strong>{{ $req->auxiliary()->code }}</strong></div>
                <div class="elem-4"><strong>{{ $req->auxiliary()->name }}</strong></div>
            </div>
            @endforeach
        </div>
    @endif
    <hr>
    <label for="">Opciones</label>
    <div class="btn-toolbar cta">
        <a class="btn btn-small btn-color" href="{{ route('request',$call_type) }}"><i class="icon-pencil icon-white"></i> Nuevo Requerimiento</a>
    </div>
</div>
<hr>
<div class="span7">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem()" href="#academic-adm"  data-toggle="tab" ><i class="icon-arrow-left icon-white"></i> Anterior </a>
    </div>
    <div class=" to-right">
        <a class="btn btn-large btn-color" onClick="nextItem()" href="#requisites"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente </a>
    </div>
</div>
