<div class="span7">
    @if ($errors->has('name'))
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ $errors->first('name') }}</strong>
        </div>
    @endif
</div>
<div class="span7">
    @if ($errors->has('description'))
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ $errors->first('description') }}</strong>
        </div>
    @endif
</div>

<div class="post-heading">
    <h3>Nombre y Descripcion</h3>
</div>

<div class="span7">
    <label>Nombre <span>*</span></label>
    <textarea rows="2" name='name' class="input-block-level" style="text-align: center;font-weight:bold;"  placeholder="NOMBRE DE LA CONVOCATORIA">{{$name}}</textarea>
</div>
<div class="span7">
    <label>Descripción <span>*</span></label>
    <textarea rows="5" name='description'class="input-block-level" style="text-align: justify"  placeholder="Descripcion de la convocatoria">{{$description}}</textarea>
</div>
<div class="span7">
    <div class=" to-right">
        <a class="btn btn-large btn-color"  onClick="nextItem()" href="#academic-adm"  data-toggle="tab" ><i class="icon-arrow-right icon-white"></i> Siguiente </a>
    </div>
</div>