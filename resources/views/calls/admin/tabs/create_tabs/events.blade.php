<div class="post-heading">
    <h3>Eventos y Fechas</h3>
</div>
<div class="span7">
        @if ($errors->has('management') || $errors->has('year'))
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <ul>
                <li><strong>{{ $errors->first('management') }}</strong></li>
                <li><strong>{{ $errors->first('year') }}</strong></li>
            </ul>
        </div>
    @endif
</div>
<div class="span7">
    <div id="datesCalls">
    <label for="">Fechas</label>
        <datalist id="list_events" >
            @foreach($events as $event)
                <option value="{{$event->name}}"></option>
            @endforeach
        </datalist>
    </div>
    <div class="btn-toolbar cta">
        <a class="btn btn-small btn-color" href="#" onClick="addFieldDate()"><i class="icon-pencil icon-white"></i> Agregar Nueva Fecha </a>
    </div>
</div>
<div class="span7">
    <div class=" to-left">
        <a class="btn btn-large btn-inverse" onClick="prevItem()" href="#merit_ratings" data-toggle="tab" ><i class="icon-arrow-left icon-white"></i>Anterior</a>
    </div>
    <div class=" to-right">
        <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i>Crear convocatoria</button>
    </div>
</div>
