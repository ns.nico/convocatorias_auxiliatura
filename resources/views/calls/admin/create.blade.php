@extends('calls.layout')
@section('section')
<div class="span8">
<article class="blog-post">
    <div class="comment-post">
        <form action="{{route ('call_store',$call_type)}}" method="post" class="comment-form" name="comment-form">
            {{ csrf_field()}}
            <div class="tabbable tabs-left">
                <div class="tab-content">
                    <div class="tab-pane active" id="name-description">
                        @include('calls.admin.tabs.create_tabs.name_description')
                    </div>

                    <div class="tab-pane" id="academic-adm">
                        @include('calls.admin.tabs.create_tabs.academic_adm')
                    </div>

                    <div class="tab-pane" id="requests">
                        @include('calls.admin.tabs.create_tabs.requests')
                    </div>

                    <div class="tab-pane" id="requisites">
                        @include('calls.admin.tabs.create_tabs.requisites')
                    </div>

                    <div class="tab-pane" id="document-requireds">
                        @include('calls.admin.tabs.create_tabs.documents_required')
                    </div>

                    <div class="tab-pane" id="merit_ratings">
                        @include('calls.admin.tabs.create_tabs.merit_ratings')
                    </div>

                    <div class="tab-pane" id="events">
                        @include('calls.admin.tabs.create_tabs.events')
                    </div>
                </div>
            </div>
        </form>
    </div>
</article>
</div>
@endsection
