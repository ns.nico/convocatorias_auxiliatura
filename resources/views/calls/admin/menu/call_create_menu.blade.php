<ul class="cat">
    <li><a href="#name-description"  data-toggle="tab" class="selected-a"> Nombre y Descripcion</a></li>
    <li><a href="#academic-adm"  data-toggle="tab"> Gestion y Unidad Academica</a></li>
    <li><a href="#requests"  data-toggle="tab"> Requerimientos</a></li>
    <li><a href="#requisites"  data-toggle="tab"> Requisitos</a></li>
    <li><a href="#document-requireds" data-toggle="tab"> Documentos requeridos</a></li>
    <li><a href="#merit_ratings" data-toggle="tab"> Calificación de Meritos</a></li>
    <li><a href="#events" data-toggle="tab"> Eventos y Fechas</a></li>
</ul>
