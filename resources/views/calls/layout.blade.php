@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="row">
            @include('calls.menu')
            @yield('section')
        </div>
    </div>
</section>
@endsection