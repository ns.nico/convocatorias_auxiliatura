@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="postulantes">
                    <div class="post-heading">
                        <h3>Lista de Postulantes</h3>
                    </div>
                    <div class="row">
                        @if(count($enrolls)==0)
                        <div class="span8">
                            <div class="alert alert-info" >
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong> Ningun postulante se registro a esta convocatoria o ya se registraron los documentos de todos los postulantes</strong>
                            </div>
                        </div>
                        @else
                        
                        <div class="span8">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Opciones
                                    </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($enrolls as $enroll)
                                    <tr>
                                    <td>
                                        {{ $enroll->applicant }}
                                    </td>
                                    <td>
                                        {{ $enroll->item }}
                                    </td>
                                    <td>
                                        <div class="btn-toolbar cta">
                                            <a class="btn btn-small btn-color" href="{{ route('register_document', [$call->id, $enroll->id] ) }}"><i class="icon-pencil icon-white"></i> Registrar Documentos</a>
                                        </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif

                        <div class="span8">
                            <div class=" to-left">
                                <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
@endsection