@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span4">
                <aside>
                    <div class="widget">
                    <h4>Informacion del Postulante</h4>
                    <ul>
                        <li><label><strong>Nombre : </strong>{{ $user->name }}</label>
                        <p>
                        </p>
                        </li>
                        <li><label><strong>Cedula de Identidad : </strong> {{ $user->codeSis }} </label>
                        <p>

                        </p>
                        </li>
                        <li><label><strong>Codigo Sis : </strong> {{ $user->ci }}</label>
                        <p>

                        </p>
                        </li>
                    </ul>
                    </div>
                    <div class="widget">
                    <h4>Informacion de la Convocatoria</h4>
                    <ul class="social-links">
                        <li><label><strong>Nombre : </strong> {{ $call->name }}</label>
                        <p>
                        </p>
                        </li>
                        <li><label><strong>Gestion : </strong> {{ $call->management }} - {{ $call->year }}</label>
                        <p>

                        </p>
                        </li>
                    </ul>
                    </div>
                </aside>
            </div>
            <div class="span8">
            <h3> {{ $title }} </h3>
            </div>
            <form action="{{ route('document_store_secretary', [ $call->id,$enroll->id]) }}" method="POST">
            {{ csrf_field()}}
            <div class="span8">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                        <th>
                            Marcar
                        </th>
                        <th>
                            Documento
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $document)
                        <tr>
                        <td>
                        <label class="checkbox"> <input type="checkbox" name="documents[]" value="{{ $document->id }}"> </label>
                        </td>
                        <td>
                            {{ $document->available_document()->document()->description }}
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="span8">
                <div class=" to-left">
                    <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                </div>
                <div class=" to-right">
                    <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Registrar Documentos</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>

@endsection
