@extends('calls.layout')
@section('section')
<div class="span8">
    @if(session('message'))
    <div class="alert alert-success" >
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{ session('message') }}</strong>
    </div>
    @endif
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="all-calls">
                    <div class="post-heading">
                        <h3>Lista de Convocatorias</h3>
                    </div>
                    <div class="post-body">
                        @if(count($calls)==0)
                            <div class="alert alert-error">
                                <strong>No se publico ninguna convocatoria</strong>
                            </div>
                        @else
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Gestion
                                </th>
                                <th>
                                    Unidad Academica
                                </th>
                                <th>
                                    Comision
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($calls as $call)
                                <tr>
                                <td>
                                    {{ $call->name }}
                                </td>
                                <td>
                                    {{ $call->management }} - {{ $call->year }}
                                </td>
                                <td>
                                    {{ $call->academic_unit()->name }}
                                </td>
                                <td>
                                    <div class="btn-toolbar cta">
                                    <!-- <a class="btn btn-small btn-color" href="{{ route('call_options',$call->id) }}"> Opciones </a> -->
                                        <a class="btn btn-small btn-color" href="{{ route('reception_book',$call->id) }}"> Libro de Recepcion </a>
                                    </div>
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                    <div class="row">
                        <div class="pagination pagination-centered">
                            <ul class="pager">
                                {{$calls->render()}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
@endsection