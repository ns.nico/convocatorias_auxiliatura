@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="reception">
                    <div class="post-heading">
                        <h3>Registrar en el libro de recepcion</h3>
                    </div>
                    <div class="post-body">
                        @if(count($applicants)==0)
                        <div class="span7">
                            <div class="alert alert-info" >
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong> Ningun postulante se registro a esta convocatoria o ya se registraron los documentos de todos los postulantes</strong>
                            </div>
                        </div>
                        @else
                        <form action="{{ route('reception_book_store') }}" method="POST" class="comment-form" name="comment-form">
                            {{ csrf_field()}}

                            @if ($errors->has('count'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('count') }}</strong>
                                </div>
                            @endif

                            <div class="span4">
                                <label>Postulante <span>*</span></label>
                                <select id="applicant" name="id_applicant" onChange="onChangeApplicants({{ $applicants }} ,this.value)" >
                                    @foreach($applicants as $ap)
                                        <option value="{{$ap->id}}"> {{ $ap->applicant }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span3">
                                <label>Documentos Entregados  <span>*</span></label>
                                <input  type="number" class="form-control" name="count" max='50' required placeholder="Cantidad de documentos" value='0' min="0">
                            </div>
                            @foreach($applicants as $ap)
                            <div id="{{$ap->id}}" style="display:none">
                                <div class="span5">
                                    <label>Item <span>*</span></label>
                                    <label>{{$ap->item}}</label>
                                </div>
                            </div>
                            @endforeach
                            
                            <div class="span7">
                                <div class=" to-right">
                                    <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Registrar </button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>
@endsection