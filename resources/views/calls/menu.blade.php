<div class="span4">
    <aside>
        <div class="widget">
        </div>
        <div class="widget">
            <h4>{{ $title }}</h4>
                @if (\Request::is('call') || \Request::is('call-edit/*'))
                    @include('calls.admin.menu.call_create_menu')
                @elseif(\Request::is('home-admin'))
                    @include('calls.admin.menu.call_index_menu')
                @elseif(\Request::is('request/*'))
                    @include('calls.admin.menu.request_menu')
                @elseif(\Request::is('academic-unit/*'))
                    @include('calls.admin.menu.academic_unit_menu')
                @elseif(\Request::is('requisite/*'))
                    @include('calls.admin.menu.requisite_menu')
                @elseif(\Request::is('document-required/*'))
                    @include('calls.admin.menu.document_required_menu')
                @elseif(\Request::is('roll'))
                    @include('calls.admin.menu.create_user_menu')

                @elseif(\Request::is('enroll/*'))
                    @include('calls.student.menu.enroll_request_menu')

                @elseif(\Request::is('home-secretary'))
                    @include('calls.secretary.menu.secretary_menu')
                @elseif(\Request::is( 'qualification-knowledge/*') )
                    @include('calls.teacher.menu.teacher_menu')
                @elseif(\Request::is('call-options/*'))
                    @include('commission.menu.merits_commission_menu')
                @elseif(\Request::is('reception_book/*'))
                    @include('calls.secretary.menu.reception_book_menu')
                @elseif(\Request::is('commission/*'))
                    @include('commission.menu.create_commission_menu')

                @elseif(\Request::is('home-teacher'))
                    @include('calls.teacher.menu.teacher_menu')

                @elseif(\Request::is('seecall/*') )
                    @include('calls.admin.menu.selected_call_menu')
                @endif

        </div>
    </aside>
</div>
