@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <form action="{{ route('enroll_store') }}" method="POST" class="comment-form" name="comment-form">
                        {{ csrf_field()}}
                        <div class="post-heading">
                            <h3>Items</h3>
                            <input style="display:none" name="call_id" value="{{$call_id}}">
                        </div>
                        <div class="span7">
                            @if ($errors->has('requests'))
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>{{ $errors->first('requests') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="span7">
                            @if(count($required_requests)==0)
                            <div class="alert alert-error">
                                <strong>La convocatoria no tiene ningun item</strong> 
                            </div>
                            @else
                            <label for="">Items a los que desea postularse</label>
                            <div id="accepted"></div>
                            <label for="">Items a los que no desea postularse</label>
                            <div id="rejected">
                                @foreach($required_requests as $req)
                                <div id='requests{{$req->id}}' class="alert-mdf alert-error" 
                                    onClick='on("requests{{$req->id}}","requests",{{$req->id}})'>
                                    <a  class="close" ><i class="icon-mdf-e icon-plus"></i></a>
                                    <div class="elem-1"><strong>{{ $req->available_request()->request()->quantity }} Aux. </strong></div>
                                    <div class="elem-2"><strong>{{ $req->available_request()->request()->academic_hours }} Horas/Mes</strong></div>
                                    <div class="elem-1"><strong>{{ $req->available_request()->request()->auxiliary()->code }}</strong></div>
                                    <div class="elem-4"><strong>{{ $req->available_request()->request()->auxiliary()->name }}</strong></div>
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                        <div class="span7" style="margin-top:20px">
                            <div class="to-left">
                                <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Postularse </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </article>
</div>
@endsection