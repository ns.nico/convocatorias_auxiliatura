@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span12">
                @if(session('message'))
                <div class="alert alert-info" >
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>{{ session('message') }}</strong>
                </div>
                @endif
            </div>
            <div class="span12">
                <ul class="filter">
                    <li class="all active"><a href="#" class="btn btn-color">Todos</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            @if(count($calls)==0)
                <div class="alert alert-error">
                    <strong>No se publico ninguna convocatoria</strong>
                </div>
            @else
            <ul class="portfolio-area da-thumbs">
                @foreach($calls as $call)
                <li class="portfolio-item2" data-id="id-0" data-type="all">
                    <div class="span4">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                            <img src="{{ asset('/img/convocatorias/conv-1.png') }}" alt="Portfolio" title="" />
                            <article class="da-animate da-slideFromRight" style="display: block;">
                                <h4>Auxiliatura - Docencia</h4>
                                <div class="btn-toolbar cta">
                                    <a class="btn btn-large btn-inverse" href="{{route('pdf_download',$call->id)}}"><i class="icon-eye-open icon-white"></i> Ver </a>
                                    <a class="btn btn-large btn-color" href="{{ route('enroll_request',$call->id) }}"><i class="icon-star icon-white"></i> Postularse </a>
                                </div>
                            </article>
                            </div>
                        </div>
                        <h5>{{$call->name}}
                            Gestion {{$call->management }} - {{$call->year}} 
                        </h5>
                    </div>
                </li>
                @endforeach
            </ul>
            @endif
        </div>
    </div>
</selection>
@endsection