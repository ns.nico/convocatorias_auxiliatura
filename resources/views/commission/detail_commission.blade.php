@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span4">
                <aside>
                    <div class="widget">
                    <h4>Detalle de la comision </h4>
                    <ul>
                        <li><label><strong>Items o Especificacion : </strong>  </label>
                        @foreach($item as $i)
                        <p>
                            {{ $i->name }}
                        </p>
                        @endforeach
                        </li>
                        <li><label><strong>Tipo de Comision : </strong> {{ $commission->type }} </label>
                        <p>

                        </p>
                        </li>
                    </ul>
                    </div>
                </aside>
            </div>
            <div class="span8">
                <h3> Lista de los que conforman la comision </h3>

                <div class="row">
                    <div class="span8">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                <th>
                                    Lider
                                </th>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Rol
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                @if($user->leader)
                                <tr class="success">
                                <td >
                                    <label class="checkbox"> <input type="radio" disabled checked> </label>
                                </td>
                                <td>
                                    {{$user->name}}
                                </td>
                                <td>
                                    {{ $user->role }}
                                </td>
                                </tr>
                                @else
                                <tr>
                                <td>
                                    <label class="checkbox"> <input type="radio" disabled > </label>
                                </td>
                                <td>
                                    {{$user->name}}
                                </td>
                                <td>
                                    {{ $user->role }}
                                </td>
                                </tr>
                                @endif
                                
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="span8">
                        <div class="alert alert-info" >
                            <strong> Nota: </strong> Solo el lider puede registrar las notas en esta comision.
                        </div>
                    </div>
                    <div class="span8">
                        <div class=" to-left">
                            <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                        </div>
                    </div>
                </div>

            </div>
            
            
        </div>
    </div>
</section>

@endsection
