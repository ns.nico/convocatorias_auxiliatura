@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="post-heading">
            <h3>Crear Comision</h3>
        </div>
        <div class="post-body">
            <form action="{{ route('commission_store',$call_id)}}" method="post">
                {{ csrf_field()}}
                <div class="span6">
                    <label><strong>Comision</strong> <span>*</span></label>
                    @if(count($commission_types)==0)
                    <div class="alert alert-error">
                        <strong>Ninguna comision registrada</strong> 
                    </div>
                    @else
                    <select id="commission_type" class="input-block-level" name="commission_type" onchange="reloadItems()">
                        @foreach($commission_types as $commission)
                        <option class="input-block-level" value="{{ $commission->id}}">{{ $commission->name}}</option>
                        @endforeach
                    </select>
                    @endif
                </div>

                <div class="span6" id="items">
                    <label><strong>Item</strong> <span>*</span></label>
                    <select class="input-block-level" name="item_id">
                        @foreach($items as $item)
                        <option class="input-block-level" value="{{ $item->id}}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="span8">
                    @if ($errors->has('users'))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ $errors->first('users') }}</strong>
                        </div>
                    @endif
                    @if ($errors->has('leader'))
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ $errors->first('leader') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="span8">
                    <label><strong>Selecciona a los usuarios que perteneceran a la comision</strong> <span>*</span></label>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                            <th>
                                Marcar
                            </th>
                            <th>
                                Lider
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Rol
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                            <td>
                            <label class="checkbox"> <input type="checkbox" onclick='setDisabledRadioBtn(this)' name="users[]" value="{{ $user->id }}"> </label>
                            </td>
                            <td>
                            <label class="checkbox"> <input type="radio" id="leader{{ $user->id }}" name="leader" disabled value="{{ $user->id }}"> </label>
                            </td>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->rol}}
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
                <div class="span8">
                    <div class="alert alert-alert">
                        <strong>Nota: </strong> Solo el lider podra registrar las notas de las respectivas comisiones a la que se le asigne. 
                    </div>
                </div>
                
                <div class="span8">
                    <div class=" to-left">
                        <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Crear Comision</button>
                    </div>
                </div>
            </form>
        </div>
    </article>
</div>
@endsection