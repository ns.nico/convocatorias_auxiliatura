@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="span4">
            <aside>
                <div class="widget">
                    <h2><b>{{$item->name}}</b></h2>
                </div>
                <div class="widget">
                    <ul>
                        <li><b>Nombre:</b> Nombre del postulante</li>
                        <li><b>Item:</b> Codigo del Item al que pertenece su postulación</li>
                        <li><b>Porcentaje:</b> la calificación se va promediar de forma automatica segun al porcentaje indicado </li>
                        <li><b>Calificación:</b> La calificación sera hasta un maximo de 100 puntos</li>
                    </ul>
                </div>
            </aside>
        </div>
        @if(count($list_of_applicants)!=0)
        <div class="span7" id="doc_user">
            <form action="{{ route('item_rating_store') }}" method="post" >
                {{ csrf_field()}}
                <table class="tableMerit">
                        <tr class="myDescription">
                            <td style="text-align: center"> <b class="white">Nombre </b></td>
                            <td style="text-align: center;width: 20%"> <b class="white">Item </b></td>
                            <td style="text-align: center;width: 10%"> <b class="white">Porcentaje </b></td>
                            <td class="Porcentaje"><b class="white">Calificación<br> 100ps</b> </td>
                        </tr>
                    @foreach($list_of_applicants as $postulantes)
                        @foreach($description as $desc)
                            @if ($postulantes->id == $desc->id && $postulantes->id_specification == $desc->especifications_id)
                            @php
                                $valor = (($postulantes->grade)*100)/$postulantes->percentage;
                            @endphp
                            <input type="hidden" name="id_esp_grade[]" value="{{$postulantes->id_esp_grade}}">
                            <input type="hidden" name="id_descrition_grade[]" value="{{$desc->descrip_grade_id}}">
                            <input type="hidden" name="perdentage[]" value="{{$postulantes->percentage}}">
                                <tr>
                                    <td ><b style="color: black">{{$postulantes->name}}</b></td>
                                    <td style="text-align: center"><b>{{$desc->descrip}}</b></td>
                                    <td style="text-align: center"><b>{{$postulantes->percentage}}</b></td>
                                    <td class="Porcentaje-desc">
                                    <input name="grade[]" value="{{$valor}}" type="number" min="0" max="100" style="width : 50px " required>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </table>
                <br>
                <div class="span7">
                    <div class=" to-left">
                        <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                    </div>
                    <div class=" to-right">
                        <button class="btn btn-large btn-color" type="submit" >Registrar Nota </button>
                    </div>
                </div>
            </form>
        </div>
        @else
            <div class="span7">
                <h3>No hay postulantes para este Item</h3>
            </div>
            <div class="span7">
                    <div class=" to-left">
                        <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                    </div>
            </div>
        @endif

    </div>
    <br>
</section>
@endsection
