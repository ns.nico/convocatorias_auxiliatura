@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
            <div class="span4">
                @foreach ( $enrollRequest as $en_User)
                    <aside>
                        <div class="widget">
                            <h4>Informacion del Postulante</h4>
                            <ul>
                                <li><label><strong>Nombre : </strong>{{$en_User->user()->name }}</label></li>
                            </ul>
                        </div>
                        <div class="widget">
                            <h4>Informacion de la Convocatoria</h4>
                            <ul class="social-links">
                                <li><label><strong>Nombre : </strong> {{ $call->calls()->name }}</label></li>
                                <li><label><strong>Gestion : </strong> {{ $call->calls()->management }} - {{ $call->calls()->year }}</label></li>
                            </ul>
                        </div>
                    </aside>
                @endforeach
            </div>
            <div class="span7" id="doc_user">
                {{-- tabla de Calificación de Docencia --}}
                @foreach ($enrollRequest as $User)
                <h3>Postulante: <b>{{ $User->user()->name }}</b>  </h3>
                <h4>Item: <b> {{$item->name}} </b></h4>
                <h4>Codigo: <b> {{$item->code}} </b></h4>
                    <div id="divUser{{$User->user_id}}">
                        @foreach ($type_conv as $type)
                            @if ($type->id == $User->required_request_id && $type->available_request()->call_type_id == 1)
                                <form action="{{ route('doc_qualification_store') }}" method="post" >
                                    {{ csrf_field()}}
                                    <input type="hidden" name="request_id" value="{{ $User->required_request_id }}">
                                        <table class="tableMerit">
                                            <tr class="myDescription">
                                                <td style="width: 100px"> <b class="white">Documento </b></td>
                                                <td class="Porcentaje"><b class="white">Porcentage </b> </td>
                                                <td class="Porcentaje"><b class="white">Calificación 100pts </b> </td>
                                            </tr>
                                            @foreach($Description_grades as $desc_grades)
                                                @if ($desc_grades->enroll_request_id == $User->id)
                                                    @foreach($calif_doc as $doc_cal)
                                                        @if ($desc_grades->description_metirs()->id == $doc_cal->description_metirs_id )
                                                            <tr>
                                                                <td ><b>{{$doc_cal->description()->name}}</b></td>
                                                                <td class="Porcentaje-desc">{{ $doc_cal->description()->percentage }} %</td>
                                                                <td class="Porcentaje-desc">
                                                                @php
                                                                    $valor = intval((($desc_grades->grade)*100)/$doc_cal->description()->percentage);
                                                                @endphp
                                                                <input name="grade[]" type="number" value="{{$valor}}" min="0" max="100" style="width : 50px " required>
                                                                <input type="hidden" name="grade_percentage[]" value="{{ $doc_cal->description()->percentage }}">
                                                                <input type="hidden" name="description_grade_id[]" value="{{ $desc_grades->id }}">
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </table>
                                        <br>
                                    <div class="span7">
                                        <div class=" to-left">
                                            <a class="btn btn-large btn-inverse" onclick="back()" ><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                                        </div>
                                        <div class=" to-right">
                                            <button class="btn btn-large btn-color" type="submit" >Registrar Nota </button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            </div>
    </div>
    <br>
</section>
@endsection
