@extends('layouts.app')
@section('content')
@include('includes.subintro')
@include('includes.bread_crumb')
<section id="maincontent">
    <div class="container">
        <div class="row">
            <div class="span3 bs-docs-sidebar">
                <ul class="nav nav-list bs-docs-sidenav">
                    <h3><b style="color: black">Lista de postulantes</b></h3>
                    @foreach ( $list_of_applicants as $postulantes)
                        <li><a href="#" onClick="functionMeritQualification({{$postulantes->enroll_id}})"><i class="icon-chevron-right"></i><b style="color: black">{{$postulantes->name }}</b>  / {{$postulantes->code}} </a></li>
                    @endforeach
                </ul>
            </div>

            <div class="span9" id="merit_registration_form">
                @foreach ($list_of_applicants as $postulantes)
                    <div id="divUser{{$postulantes->enroll_id}}" {{($postulantes->enroll_id == $primer_id->enroll_id)? "style= display:block": "style= display:none"}}>
                        <h3>Postulante: <b>{{ $postulantes->name }}</b></h3>
                        <h4>Item <b>{{$postulantes->auxi_name}}</b></h4>
                        @foreach ($specification_grade as $speci_grade)
                            @if ($speci_grade->enroll_request_id == $postulantes->enroll_id)
                                @if ($speci_grade->grade == 0  )
                                    @continue
                                @else
                                    <h2><b style="color: black">"Este postulante ya fue calificado"</b></h2>
                                    @break
                                @endif
                            @endif
                        @endforeach

                        <form action="{{ route('merit_qualification_store') }}" method="post" class="comment-form" name="comment-form">
                            {{ csrf_field() }}
                        <input type="hidden" name="call_id" value="{{$call_id}}">
                            {{-- creacion de la tabla para calificar meritos --}}
                                <table class="tableMerit">
                                    <tr class="myDescription">
                                        <td > <b class="white">Descripcion de meritos </b></td>
                                        <td class="Porcentaje"><b class="white">Porcentaje </b> </td>
                                        <td class="Porcentaje"><b class="white" style="text-align: center">Nota </b> </td>
                                    </tr>
                                    @foreach($description_merit as $description)
                                        @if($description->percentage != 0)
                                            <tr>
                                                <td class="myDescription" ><b class="white">{{$description->name}}</b></td>
                                                <td class="myDescription" style="text-align: center"><b class="white">{{$description->percentage}}</b></td>
                                                <td class="myDescription" style="text-align: center">
                                                    @foreach ($description_grade as $desc_grade)
                                                        @if ($desc_grade->description_merit_id == $description->description_id && $desc_grade->enroll_request_id == $postulantes->enroll_id )
                                                            <b class="white"><span id="spTotal">{{$desc_grade->grade}}</span></b>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                            @foreach($specification_merit as $especification)
                                                @if($especification->description_metirs_id == $description->description_id)
                                                    <tr>
                                                        <td>
                                                            <b style="color:black">{{ $especification->name }}</b>
                                                            <ul>
                                                                @foreach($points_merit as $point)
                                                                    @if($point->especification_id == $especification->especifications_id)
                                                                        <li style="font-weight:100 ">
                                                                            {{ $point->points }} ptos/ {{ $point->description }}
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        </td>
                                                        <td class="Porcentaje-desc">
                                                            {{$especification->percentage}}
                                                        </td>
                                                        <td>
                                                            @foreach ($specification_grade as $speci_grade)
                                                                @if ($speci_grade->specification_id == $especification->specifi_id && $speci_grade->enroll_request_id == $postulantes->enroll_id )
                                                                    <input name="specif_grade[]" value="{{$speci_grade->grade}}" type="number" min="0" max="{{$especification->percentage}}" style="width : 50px " required {{-- class="monto" --}} {{-- onkeyup=" sumar();" --}}>
                                                                    <input type="hidden" name="id_specification_grade[]" value="{{$speci_grade->id}}">
                                                                @endif
                                                            @endforeach
                                                            @foreach ($description_grade as $desc_grade)
                                                                @if ($desc_grade->description_merit_id == $description->description_id && $desc_grade->enroll_request_id == $postulantes->enroll_id )
                                                                    <input type="hidden" name="id_description_grade[]" value="{{$desc_grade->id}}">
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </table>
                                <br>
                            <div class="span8">
                                <div class=" to-left">
                                    <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
                                </div>
                                <div class=" to-right">
                                    <button class="btn btn-large btn-color" type="submit" ><i class="icon-ok icon-white"></i> Calificar </button>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <br>
</section>
@endsection
