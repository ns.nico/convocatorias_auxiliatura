@extends('calls.layout')
@section('section')
<div class="span8">
    <article class="blog-post">
        <div class="tabbable tabs-left">
            <div class="tab-content">
                <div class="tab-pane active" id="postulantes">
                    <div class="post-heading">
                        <h3>Calificar Postulantes</h3>
                    </div>
                    <div class="row">
                        <div class="span8">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Codigo Sis
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Opciones
                                    </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($applicants as $app)
                                    <tr>
                                    <td>
                                        {{ $app->name }}
                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        {{ $app->item }}
                                    </td>
                                    <td>
                                        <div class="btn-toolbar cta">
                                            <a class="btn btn-small btn-color" href="{{ route('docencia_qualification',[$requirementRequest, $app->id, $app->id_user]) }}"><i class="icon-pencil icon-white"></i> Calificar </a>
                                        </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span7">
            <div class=" to-left">
                <a class="btn btn-large btn-inverse" onclick="back()"><i class="icon-circle-arrow-left icon-white"></i> Atras </a>
            </div>
        </div>
    </article>
</div>
@endsection
