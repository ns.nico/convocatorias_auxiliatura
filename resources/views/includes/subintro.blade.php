<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
        <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>{{ $title }}</h3>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>