<div class="widget">
    <h4>Publicaciones recientes</h4>
    <ul class="recent-posts">
        <li><a href="#">Lista de Habilitados/Inhabilitados</a>
            <div class="clear">
            </div>
            <span class="date"><i class="icon-calendar"></i> 6 Enero, 2020</span>
            <span class="comment"><i class="icon-comment"></i> 4 Comments</span>
        </li>
        <li><a href="#">Evaluacion de conocimientos</a>
            <div class="clear">
            </div>
            <span class="date"><i class="icon-calendar"></i> 7 Enero, 2020</span>
            <span class="comment"><i class="icon-comment"></i> 2 Comments</span>
        </li>
        <li><a href="#">Reunion de aclaracion</a>
            <div class="clear">
            </div>
            <span class="date"><i class="icon-calendar"></i> 17 Enero, 2020</span>
            <span class="comment"><i class="icon-comment"></i> 12 Comments</span>
        </li>
    </ul>
</div>
<div class="widget">
    <h4>Tags</h4>
    <ul class="tags">
        <li><a href="#" class="btn">Convocatorias</a></li>
        <li><a href="#" class="btn">Requisitos</a></li>
        <li><a href="#" class="btn">Requerimientos</a></li>
        <li><a href="#" class="btn">Postulantes</a></li>
        <li><a href="#" class="btn">Calificaciones</a></li>
    </ul>
</div>