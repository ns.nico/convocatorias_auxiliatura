<section id="breadcrumb">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb notop">
                    @if(!Auth::guest() && Auth::user()->role() == 'Administrador')
                    <li class="dropdown active">
                        <a href="{{ url('/home-admin') }}">Inicio</a><span class="divider">/</span>
                    </li>
                    @elseif(!Auth::guest() && Auth::user()->role() == 'Secretario')
                    <li class="dropdown active">
                        <a href="{{ url('/home-secretary') }}">Inicio</a><span class="divider">/</span>
                    </li>
                    @elseif(!Auth::guest() && Auth::user()->role() == 'Docente')
                    <li class="dropdown active">
                        <a href="{{ url('/home-teacher') }}">Inicio</a><span class="divider">/</span>
                    </li>
                    @else
                    <li class="dropdown active">
                        <a href="{{ url('/home') }}">Inicio</a><span class="divider">/</span>
                    </li>
                    @endif
                    <li class="active">{{ $title }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>