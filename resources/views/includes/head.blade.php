<meta charset="utf-8">
<title>Administrador Convocatorias</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- styles -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700" rel="stylesheet">
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-responsive.css') }}" rel="stylesheet">
<link href="{{ asset('css/docs.css') }}" rel="stylesheet">
<link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet">
<link href="{{ asset('js/google-code-prettify/prettify.css') }}" rel="stylesheet">
<link href="{{ asset('css/flexslider.css') }}" rel="stylesheet">
<link href="{{ asset('css/sequence.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/default.css') }}" rel="stylesheet">
<link href="{{ asset('css/dates.css') }}" rel="stylesheet">
<link href="{{ asset('css/merit-ratings.css') }}" rel="stylesheet">
<!-- fav and touch icons -->
<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
