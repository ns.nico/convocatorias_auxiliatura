<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand logo" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
            
            <div class="navigation">
                <div class="navbar-toggler">
                    <i class="icon-32 icon-reorder icon-square" data-toggle="collapse" data-target="#menu-sandwich"></i>
                </div>
                <nav>
                    <ul class="nav topnav">
                        @if (Auth::guest())
                        <li class="dropdown active">
                            <a href="{{ url('/home') }}">Inicio</a>
                        </li>
                        <li>
                            <a href="{{ route('login') }}" data-toggle="modal">Iniciar Sesion</a>
                        </li>
                        <li>
                            <a href="{{ route('register') }}" data-toggle="modal">Registarse</a>

                        </li>
                        @else
                            @if(Auth::user()->role() == 'Administrador')
                            <li class="dropdown active">
                                <a href="{{ url('/home-admin') }}">Inicio</a>
                            </li>
                            @elseif(Auth::user()->role() == 'Secretario')
                            <li class="dropdown active">
                                <a href="{{ url('/home-secretary') }}">Inicio</a>
                            </li>
                            @elseif(Auth::user()->role() == 'Docente')
                            <li class="dropdown active">
                                <a href="{{ url('/home-teacher') }}">Inicio</a>
                            </li>
                            @else
                            <li class="dropdown active">
                                <a href="{{ url('/home') }}">Inicio</a>
                            </li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-transform: capitalize;" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                            Cerrar Sesion
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
