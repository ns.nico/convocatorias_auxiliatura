<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocumentFormRequest;
use App\Models\Document;
use App\Models\AvailableDocument;


class DocumentController extends Controller
{
    public function create(Request $request,$call_type){

        $title = 'Convocatoria - Documento';
        return view('calls.admin.components.document_requireds.create_document',compact('title','call_type'));
    }

    public function store(DocumentFormRequest $request)
    {
        $new_document = Document::create([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);
        $call_type =  $request->call_type;
        if($call_type){
            AvailableDocument::create([
                'document_id' => $new_document->id,
                'call_type_id' => $request->call_type 
            ]);
        }
        return redirect('/call?type='.$request->call_type);

    }
}
