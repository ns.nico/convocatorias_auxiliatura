<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Call;
use App\Models\Description_metir;
use App\Models\Auxiliary;
use PDF;
use Illuminate\Support\Facades\DB;

class PdfController extends Controller
{
    //
    function create_call(Request $request,$pcall_id){
        $call_id = $pcall_id;
        $title = $this->get_title($call_id);
        $data = $this->get_pdf_content($call_id);
        $name_pdf = $title['title']['title'];
        return $this->viewerCall($title, $data, $name_pdf);
    }
    function get_title($call_id){
        $call = $this->get_call($call_id);
        $title = array('title'=>array('title'=>$call->name,
                                    'management'=>$call->management,
                                    'year'=>$call->year),
                        'description'=>$call->description);
        return $title;
    }

    function get_pdf_content($call_id){
        $call = $this->get_call($call_id);
        $data = array_merge(
                    $this->get_requests($call_id),
                    $this->get_requisites($call_id),
                    $this->get_documents($call_id),
                    $this->get_details_call($call_id),
                    $this->get_date_presentation_call($call_id),
                    $this->get_court_call($call_id),
                    $this->get_events($call_id),
                    $this->get_appointments($call_id),
                    $this->get_firms($call_id)
                    );
        return $data;
    }

    function get_call($call_id){
        return Call::firstWhere('id', $call_id);

    }

    function get_requests($call_id){
        $response = array();
        $requests = DB::table('calls')->select('requests.quantity', 'requests.academic_hours', 'auxiliaries.name')
            ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
            ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
            ->join('requests', 'requests.id', '=', 'available_requests.request_id')
            ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($requests)){
            $response = array('request'=>$requests);
        }
        return $response;
    }

    function get_requisites($call_id){
        $response = array();
        $requisites = DB::table('calls')->select('requisites.description')
            ->join('required_requisites', 'required_requisites.call_id', '=', 'calls.id')
            ->join('available_requisites', 'available_requisites.id', '=', 'required_requisites.available_requisite_id')
            ->join('requisites', 'requisites.id', '=', 'available_requisites.requisite_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($requisites)){
            $response = array('requisite'=>$requisites);
        }
        return $response;
    }

    function get_documents($call_id){
        $response = array();
        $documents = DB::table('calls')->select('documents.description')
            ->join('required_documents', 'required_documents.call_id', '=', 'calls.id')
            ->join('available_documents', 'available_documents.id', '=', 'required_documents.available_document_id')
            ->join('documents', 'documents.id', '=', 'available_documents.document_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($documents)){
            $response = array('document'=>$documents);
        }
        return $response;
    }
    function get_details_call($call_id){
        $response = array();
        $aspects = DB::table('calls')->select('details_call.description')
            ->join('required_details_call', 'required_details_call.call_id', '=', 'calls.id')
            ->join('details_call', 'details_call.id', '=', 'required_details_call.detail_call_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($aspects)){
            $response = array('presentation_aspect'=>$aspects);
        }
        return $response;
    }

    function get_date_presentation_call($call_id){
        $response = array();
        $date = array();
        $date_presentation = DB::table('calls')->select('date_presentation.name', 'date_presentation.description')
            ->join('section_date_presentation', 'section_date_presentation.call_id', '=', 'calls.id')
            ->join('date_presentation', 'date_presentation.id', '=', 'section_date_presentation.date_presentation_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($date_presentation)){
            $date = DB::table('calls')->select('dates.start')
            ->join('dates', 'dates.call_id', '=', 'calls.id')
            ->join('available_events', 'available_events.id', '=', 'dates.available_event_id')
            ->join('events', 'events.id', '=', 'available_events.event_id')
            ->where([['calls.id', '=', $call_id],
                    ['events.name', '=', $date_presentation[0]->name]
                    ])
            ->get();
        }
        if(count($date)){
            $description = str_replace('{{date}}', date("d/m/Y", strtotime($date[0]->start)), $date_presentation[0]->description);
            $response = array('date_presentation'=>$description);
        }
        return $response;
    }

    function get_court_call($call_id){
        $response = array();
        $description = DB::table('calls')->select('courts.description')
            ->join('courts_call', 'courts_call.call_id', '=', 'calls.id')
            ->join('courts', 'courts.id', '=', 'courts_call.court_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($description)){
            $response = array('courts'=>$description[0]->description);
        }
        return $response;
    }

    function get_events($call_id){
        $response = array();
        $events = DB::table('calls')->select('events.name', 'events.description', 'dates.start')
            ->join('dates', 'dates.call_id', '=', 'calls.id')
            ->join('available_events', 'available_events.id', '=', 'dates.available_event_id')
            ->join('events', 'events.id', '=', 'available_events.event_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($events)){
            $response = array('testing_dates'=>$events);
        }
        return $response;
    }

    function get_appointments($call_id){
        $response = array();
        $description = DB::table('calls')->select('appointments.description')
            ->join('appointments_call', 'appointments_call.call_id', '=', 'calls.id')
            ->join('appointments', 'appointments.id', '=', 'appointments_call.appointment_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($description)){
            $response = array('appointment' => $description[0]->description);
        }
        return $response;
    }

    function get_firms($call_id){
        $response = array();
        $firms = DB::table('calls')->select('responsible.name', 'responsible.position')
            ->join('responsible_call', 'responsible_call.call_id', '=', 'calls.id')
            ->join('responsible', 'responsible.id', '=', 'responsible_call.responsible_id')
            ->where('calls.id', '=', $call_id)
            ->get();
        if(count($firms)){
            $response = array('firms' => $firms);
        }
        return $response;
    }
    function download($pdf_content, $name_pdf){
        $data = ['data' => $pdf_content];
        $pdf = PDF::loadView('pdf.template', $data);
        return $pdf->download($name_pdf . '.pdf');
    }

    function viewer($pdf_content, $name_pdf){
        $data = ['data' => $pdf_content];
        $pdf = PDF::loadView('pdf.template', $data);
        $pdf->setPaper('letter', 'portrait');
        return $pdf->stream($name_pdf . '.pdf');
    }

    function viewerCall($title, $pdf_content, $name_pdf){
        $header = ['header' => $title];
        $data = ['data' => $pdf_content];
        $pdf = PDF::loadView('pdf.call_template', $header, $data);
        $pdf->setPaper('letter', 'portrait');
        return $pdf->stream($name_pdf . '.pdf');
    }

    function create_list_enabled_students(Request $request,$pcall_id){
        $call_id = $pcall_id;
        $data = $this->get_content_list_enabled_students($call_id);
        $name_pdf = $data['title']['title'];
        return $this->viewer($data, $name_pdf);
    }

    function get_content_list_enabled_students($call_id){
        $call = $this->get_call($call_id);
        $data = array('title'=>array('title'=>$call->name,
                                    'management'=>$call->management,
                                    'year'=>$call->year),
                    'list_enabled_students'=>$this->get_list_enabled_students($call_id),
                );
        return $data;
    }

    function get_list_enabled_students($call_id){
        $students = DB::table('calls')->select('users.name as user_name', 'auxiliaries.name as aux_name' , 'auxiliaries.code','enroll_request.enabled')
            ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
            ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
            ->join('requests', 'requests.id', '=', 'available_requests.request_id')
            ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
            ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
            ->join('users', 'users.id', '=', 'enroll_request.user_id')
            ->where([
                ['calls.id', '=', $call_id],
            ])
            ->get();
        return $students;
    }
    /* ---------------------generando una lista de notas segun el item de postulacion para docencia -------------  */
    function create_knowledge_list_docencia(Request $request,$call_id,$id_auxiliatura){
        $call_id = $call_id;
        $data = $this->get_content_list_enabled_qualified_students($call_id,$id_auxiliatura);
        $name_pdf = $data['title']['title'];
        return $this->viewer($data, $name_pdf);
    }

    function get_content_list_enabled_qualified_students($call_id,$id_auxiliatura){
        $call = $this->get_call($call_id);
        $name_item = Auxiliary::firstWhere('id',$id_auxiliatura);
        /* dd($name_item); */
        $data = array('title'=>array('title'=>$call->name,
                                    'year'=>$call->year,
                                    ),
            'title_notes_item'=>array('item'=>$name_item->name),
            'list_knowledge_results_doc'=>array('notas' => $this->get_list_notes_students_knowledge($call_id,$id_auxiliatura),
                                                'list_students' =>  $this->get_list_students($call_id,$id_auxiliatura) ),
                );
        return $data;
    }

    function get_list_notes_students_knowledge($call_id,$id_auxiliatura){
        $knowledge_notes_doc = DB::table('calls')
        ->select('users.name as user_name','description_metirs.name as des_name','description_grades.grade')
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->join('description_grades', 'description_grades.enroll_request_id', '=', 'enroll_request.id')
        ->join('description_metirs', 'description_metirs.id', '=', 'description_grades.description_merit_id')
        ->join('available_descriptions', 'available_descriptions.description_metirs_id', '=', 'description_metirs.id')
        ->join('merit_ratings', 'merit_ratings.id', '=', 'available_descriptions.merit_ratings_id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliatura ],
                ['enroll_request.enabled', '=', true],
                ['merit_ratings.merit_rating_type_id', '=', 3],
            ])
        ->get();
        /* dd($knowledge_notes_doc); */
        return $knowledge_notes_doc;
    }
    function get_list_students($call_id,$id_auxiliatura){
        $list_student = DB::table('calls')
        ->select('users.name as user_name','auxiliaries.name')
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliatura ],
                ['enroll_request.enabled', '=', true],
            ])
        ->get();
       /*  dd($list_student); */
        return $list_student;
    }

/* ------------------------------mostrar lista para conociento de tipo laboratorio-------------------------------- */
    function create_knowledge_list_laboratorio(Request $request,$call_id,$id_auxiliatura){
        $call_id = $call_id;
        $data = $this->get_content_list_enabled_qualified_students_lab($call_id,$id_auxiliatura);
        $name_pdf = $data['title']['title'];
        return $this->viewer($data, $name_pdf);
    }

    function get_content_list_enabled_qualified_students_lab($call_id,$id_auxiliatura){
        $call = $this->get_call($call_id);
        $name_item = Auxiliary::firstWhere('id',$id_auxiliatura);
        /* dd($name_item); */
        $data = array('title'=>array('title'=>$call->name,
                                    'year'=>$call->year,
                                    ),
            'title_notes_item'=>array('item'=>$name_item->name),
            'list_knowledge_results_lab'=>array('notas_desc' => $this->get_list_description_knowledge_lab($call_id,$id_auxiliatura),
                                                'notas_speci' => $this->get_list_especification_knowledge_lab($call_id,$id_auxiliatura),
                                                'specification_list' => $this->get_general_list_especification_knowledge_lab($id_auxiliatura),
                                                'list_students' =>  $this->get_list_students($call_id,$id_auxiliatura) ),
                );
        return $data;
    }

    function get_list_description_knowledge_lab($call_id,$id_auxiliatura){
        $knowledge_notes_lab = DB::table('calls')
        ->select('users.name as user_name','description_metirs.name as des_name','description_grades.grade','enroll_request.id')
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->join('description_grades', 'description_grades.enroll_request_id', '=', 'enroll_request.id')
        ->join('description_metirs', 'description_metirs.id', '=', 'description_grades.description_merit_id')
        ->join('available_descriptions', 'available_descriptions.description_metirs_id', '=', 'description_metirs.id')
        ->join('merit_ratings', 'merit_ratings.id', '=', 'available_descriptions.merit_ratings_id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliatura ],
                ['enroll_request.enabled', '=', true],
                ['merit_ratings.merit_rating_type_id', '=', 4],
            ])
        ->get();
        /* dd($knowledge_notes_lab); */
        return $knowledge_notes_lab;
    }
    function get_general_list_especification_knowledge_lab($id_auxiliatura){
        $name_item = Auxiliary::firstWhere('id',$id_auxiliatura);
        $description = Description_metir::firstWhere('name',$name_item->code);
        $specification_list = DB::table('description_metirs')
        ->select('especifications.name')
        ->join('available_especifications', 'available_especifications.description_metirs_id', '=', 'description_metirs.id')
        ->join('especifications', 'especifications.id', '=', 'available_especifications.especifications_id')
        ->where([
                ['available_especifications.description_metirs_id', '=', $description->id],
            ])
        ->groupBy('especifications.name')
        ->get();
        /* dd($specification_list); */
        return $specification_list;
    }
    function get_list_especification_knowledge_lab($call_id,$id_auxiliatura){
        $name_item = Auxiliary::firstWhere('id',$id_auxiliatura);
        $description = Description_metir::firstWhere('name',$name_item->code);
        $knowledge_notes_lab = DB::table('calls')
        ->select('auxiliaries.name as auxi','users.name as user_name','especifications.name as spe_name','specification_grades.grade','enroll_request.id','available_especifications.especifications_id')
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->join('specification_grades', 'specification_grades.enroll_request_id', '=', 'enroll_request.id')
        ->join('especifications', 'especifications.id', '=', 'specification_grades.specification_id')
        ->join('available_especifications', 'available_especifications.especifications_id', '=', 'especifications.id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliatura ],
                ['enroll_request.enabled', '=', true],
                ['available_especifications.description_metirs_id', '=', $description->id],
            ])
        ->get();
        /* dd($knowledge_notes_lab); */
        return $knowledge_notes_lab;
    }

/* --------------------------generacion de notas finales conocimiento-merito ------------------------------------ */
    function create_end_note_list_download(Request $request,$pcall_id,$auxiliary,$type_call){
        $call_id = $pcall_id;
        $id_auxiliary = $auxiliary;
        $call_type = $type_call;
        $data = $this->get_content_end_list_enabled_qualified_students($call_id,$id_auxiliary,$call_type);
        $name_pdf = $data['title']['title'];
        return $this->viewer($data, $name_pdf);
    }

    function get_content_end_list_enabled_qualified_students($call_id,$id_auxiliary,$call_type){
        $call = $this->get_call($call_id);
        $name_item = Auxiliary::firstWhere('id',$id_auxiliary);
        $data = array(  'title'=>array( 'title'=>$call->name,
                                        'year'=>$call->year,
                                    ),
                        'title_notes_item'=>array('item'=>$name_item->name),
                        'end_list_notes_students'=>array(   'conocimiento' => $this->get_final_note_of_student_knowledge($call_id,$id_auxiliary,$call_type),
                                                            'merito' =>  $this->get_final_note_of_student_merit($call_id,$id_auxiliary,$call_type) ),
                );
        return $data;
    }

    function get_final_note_of_student_knowledge($call_id,$id_auxiliary,$call_type){
        if ($call_type==1) {
            $type_merit = 3;
        }else {
            $type_merit = 4;
        }
        $knowledge_notes = DB::table('calls')
        ->select('users.name as user_name','auxiliaries.name',DB::raw('SUM(description_grades.grade) as grade'))
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->join('description_grades', 'description_grades.enroll_request_id', '=', 'enroll_request.id')
        ->join('description_metirs', 'description_metirs.id', '=', 'description_grades.description_merit_id')
        ->join('available_descriptions', 'available_descriptions.description_metirs_id', '=', 'description_metirs.id')
        ->join('merit_ratings', 'merit_ratings.id', '=', 'available_descriptions.merit_ratings_id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliary ],
                ['enroll_request.enabled', '=', true],
                ['merit_ratings.merit_rating_type_id', '=', $type_merit],
            ])
        ->groupBy('users.name','auxiliaries.name')
        ->orderBy('grade', 'desc')
        ->get();
        /* dd($knowledge_notes); */
        return $knowledge_notes;
    }
    function get_final_note_of_student_merit($call_id,$id_auxiliary,$call_type){
        if ($call_type==1) {
            $type_merit = 1;
        }else {
            $type_merit = 2;
        }
        $merit_notes = DB::table('calls')->select('users.name as user_name',DB::raw('SUM(description_grades.grade) as grade'))
        ->join('required_requests', 'required_requests.call_id', '=', 'calls.id')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests', 'requests.id', '=', 'available_requests.request_id')
        ->join('auxiliaries', 'auxiliaries.id', '=', 'requests.auxiliary_id')
        ->join('enroll_request', 'enroll_request.required_request_id', '=', 'required_requests.id')
        ->join('users', 'users.id', '=', 'enroll_request.user_id')
        ->join('description_grades', 'description_grades.enroll_request_id', '=', 'enroll_request.id')
        ->join('description_metirs', 'description_metirs.id', '=', 'description_grades.description_merit_id')
        ->join('available_descriptions', 'available_descriptions.description_metirs_id', '=', 'description_metirs.id')
        ->join('merit_ratings', 'merit_ratings.id', '=', 'available_descriptions.merit_ratings_id')
        ->where([
                ['calls.id', '=', $call_id],
                ['auxiliaries.id', '=', $id_auxiliary ],
                ['enroll_request.enabled', '=', true],
                ['merit_ratings.merit_rating_type_id', '=', $type_merit],/* suma de notas de tipo merito docencia */
            ])
        ->groupBy('users.name')
        ->get();
        /* dd($students); */
        return $merit_notes;
    }
}

