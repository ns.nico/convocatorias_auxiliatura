<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EnrollRequest;
use App\Models\AvailableDescription;
use App\Models\AvailableEspecifications;
use App\Models\RequiredRequest;
use App\Models\Description_grades;
use App\Models\Especification_grades;
use App\Models\Auxiliary;
use App\Models\Call;
use App\Http\Requests\DocumentQualificationRequest;

class KnowledgeRatingController extends Controller
{
    public function index($requerimentid,$item_id,$id_user)
    {   /* dd($request_id->all()); */
        $title = 'Calificación de Conocimientos';
        $enrollRequest = EnrollRequest::where('user_id',$id_user)->where('required_request_id',$requerimentid)->get();
        $calif_doc = AvailableDescription::where('merit_ratings_id',3)->get();
        $auxiliary = Auxiliary::where('id',$item_id)->get();

        $call = RequiredRequest::findOrFail($requerimentid);
        $item = Auxiliary::findOrFail($item_id);
        $type_conv = RequiredRequest::all();
        $Description_grades = Description_grades::all();

        return view('commission.components.knowledge_rating', compact('title','enrollRequest','calif_doc','type_conv','auxiliary','call','item','Description_grades'));
    }
    protected function store(DocumentQualificationRequest $request)
    {   /* dd($request->all()); */
        $var1 = $request->input('grade');
        $var2 = $request->input('description_grade_id');
        $var3 = $request->input('grade_percentage');
        for ($h=0; $h<count($var1) ; $h++) {
            $update = Description_grades::where('id',$var2[$h])->first();
            $update->grade = intval((($var1[$h])*$var3[$h])/100);
            $update->save();
        }
        return redirect()->route('qualification_k', $request->request_id);
    }
}
