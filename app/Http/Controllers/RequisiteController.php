<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequisiteFormRequest;
use App\Models\Requisite;
use App\Models\AvailableRequisite;

class RequisiteController extends Controller
{
    public function create(Request $request, $call_type){

        $title = 'Convocatoria - Requisito';
        return view('calls.admin.components.requisites.create_requisite',compact('title','call_type'));
    }

    public function store(RequisiteFormRequest $request)
    {
        $new_requisite = Requisite::create([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);
        $call_type =  $request->call_type;
        if($call_type){
            AvailableRequisite::create([
                'requisite_id' => $new_requisite->id,
                'call_type_id' => $request->call_type 
            ]);
        }
        return redirect('/call?type='.$request->call_type);

    }
}
