<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectedCallController extends Controller
{
    public function index(Request $request, $call_id){
        $title = "Ver Convocatoria";
        $items = DB::table('calls')
                ->select('calls.id as call_id','auxiliaries.name as name','auxiliaries.code as code','auxiliaries.id as id_auxi', 'required_requests.id as id','available_requests.call_type_id')
                ->join('required_requests','required_requests.call_id','=','calls.id')
                ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->where('calls.id',$call_id)
                ->get();
        return view('calls.admin.components.selected_call.index_selected_call',
        compact('call_id','title','items'));
    }
}
