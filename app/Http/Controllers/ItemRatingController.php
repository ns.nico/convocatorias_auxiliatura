<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\EnrollRequest;
use App\Models\AvailableDescription;
use App\Models\AvailableEspecifications;
use App\Models\RequiredRequest;
use App\Models\Description_grades;
use App\Models\Description_metir;
use App\Models\Especification_grades;
use App\Models\Especification;
use App\Models\Auxiliary;
use App\Models\Call;
use App\Http\Requests\DocumentQualificationRequest;

class ItemRatingController extends Controller
{
    public function index($id_call,$item_especification_id)
    {   /* dd($request_id->all()); */
        $title = 'Calificación de Items';
        $item = Especification::findOrFail($item_especification_id);
        $list_of_applicants= DB::table('enroll_request')
        ->select('enroll_request.id','users.name','especifications.percentage','especifications.name as esp_name','specification_grades.id as id_esp_grade','specification_grades.grade','especifications.id as id_specification')
        ->join('users','users.id','=','enroll_request.user_id')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('specification_grades','specification_grades.enroll_request_id','=','enroll_request.id')
        ->join('especifications','especifications.id','=','specification_grades.specification_id')
        ->where('required_requests.call_id','=',$id_call)
        ->where('especifications.name','=',$item->name)
        ->where('enroll_request.enabled','=','true')
        ->get();
        /* dd($list_of_applicants->all()); */
        $description= DB::table('enroll_request')
        ->select('enroll_request.id','description_metirs.name as descrip','description_grades.id as descrip_grade_id','available_especifications.especifications_id')
        ->join('users','users.id','=','enroll_request.user_id')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('description_grades','description_grades.enroll_request_id','=','enroll_request.id')
        ->join('description_metirs','description_metirs.id','=','description_grades.description_merit_id')
        ->join('available_especifications','available_especifications.description_metirs_id','=','description_metirs.id')
        ->where('required_requests.call_id','=',$id_call)
        ->where('enroll_request.enabled','=','true')
        ->get();

        return view('commission.components.item_rating', compact('title','list_of_applicants','item','description'));
    }
    protected function store(DocumentQualificationRequest $request)
    {   /* dd($request->all()); */
        $notas = $request->input('grade');
        $id_especification_grade = $request->input('id_esp_grade');
        $id_descriptiongrade_grade = $request->input('id_descrition_grade');
        $perdentage = $request->input('perdentage');
        for ($i=0; $i<count($id_especification_grade); $i++) {
            if ($notas[$i] != 0) {
                $control = Especification_grades::where('id',$id_especification_grade[$i])->first();
                if ($control->grade != 0) {
                    $update = Description_grades::where('id',$id_descriptiongrade_grade[$i])->first();
                    $update->grade = ($update->grade)-$control->grade;
                    $nota_convertida = intval((($notas[$i])*$perdentage[$i])/100);
                    $update->grade = ($update->grade)+$nota_convertida;
                    $update->save();
                }else {
                    $nota_convertida = intval((($notas[$i])*$perdentage[$i])/100);
                    $update = Description_grades::where('id',$id_descriptiongrade_grade[$i])->first();
                    $update->grade = ($update->grade)+$nota_convertida;
                    $update->save();
                }
            }else {
                $update = Description_grades::where('id',$id_descriptiongrade_grade[$i])->first();
                $esp_desactualizada = Especification_grades::where('id',$id_especification_grade[$i])->first();
                $update->grade = ($update->grade)-($esp_desactualizada->grade);
                $update->save();
            }
        }
        for ($h=0; $h<count($notas) ; $h++) {
            $update = Especification_grades::where('id',$id_especification_grade[$h])->first();
            $update->grade = intval((($notas[$h])*$perdentage[$h])/100);
            $update->save();
        }

        $message = "Notas calificadas con exito";

        return redirect('/home-admin')->with('message',$message);
    }
}
