<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DocumentReceived;
use App\Http\Requests\ReceptionBookRequest;

class ReceptionBookController extends Controller
{
    public function index(Request $request, $call_id){

        $title = "Libro de Recepcion";
        $applicants = DB::table('enroll_request')
                    ->select('enroll_request.id as id','users.name as applicant','auxiliaries.name as item')
                    ->join('users','users.id','=','enroll_request.user_id')
                    ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
                    ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
                    ->join('requests','requests.id','=','available_requests.request_id')
                    ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                    ->join('calls','calls.id','=','required_requests.call_id')
                    ->where('calls.id',$call_id)
                    ->where('enroll_request.reviewed',false)
                    ->get();
        return view('calls.secretary.reception_book',compact('title','call_id','applicants'));
    }

    public function store(ReceptionBookRequest $request)
    {
        DocumentReceived::create([
            'number_documents' => $request->count,
            'enroll_request_id' => $request->id_applicant
        ]);

        $message = "Se registro en el libro de recepcion";

        return redirect('/home-secretary')->with('message',$message);

    }
}
