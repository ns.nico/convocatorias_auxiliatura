<?php

namespace App\Http\Controllers;
use App\Models\Description_metir;
use App\Models\Points;
use App\Models\Especification;
use App\Models\Merit_rating;
use App\Models\AvailableMeritRating;
use App\Models\CallType;
use App\Models\AvailableDescription;
use App\Models\AvailableEspecifications;
use App\Models\AvailablePoints;
use App\Http\Requests\MaritRatingsRequest;

use Illuminate\Http\Request;

class AviableMeritRatingController extends Controller
{
    public function create(Request $request,$call_type,$merit_id)
    {   $title = 'Convocatoria - Calificación de Meritos';
        $points = Points::all();
        $especification = Especification::all();
        $descriptions = Description_metir::all();
        $availableMeritRating = AvailableMeritRating::all();
        $availablePoints = AvailablePoints::all();
        $availableDescription = AvailableDescription::all();
        $availableEspecifications = AvailableEspecifications::all();
        $type = CallType::all();
        /*  if ($call_type == 0) {
            $availableMeritRatings_select = AvailableMeritRating::all();
            $availableMeritRatings = AvailableMeritRating::where('call_types_id','1')->first();
        }else {
            $availableMeritRatings_select = AvailableMeritRating::where('call_types_id',$call_type)->get();
            $availableMeritRatings = AvailableMeritRating::where('call_types_id',$call_type)->first();
        }
 */
        /* $merits = Merit_rating::findOrFail($availableMeritRatings->merit_ratings_id); */
        $merits = Merit_rating::where('id' ,$merit_id)->first();
        return view('calls.admin.components.merits_ratings.create_merits_ratings',
        compact('title','call_type','merits','availableMeritRating','type','availablePoints','availableDescription','availableEspecifications'));
    }

    public function store( MaritRatingsRequest $request)
    {
        /* dd($request->all()); */

        $new_merit = Merit_rating::create([
            'name'=>$request->merit_ratings_name,
            'description'=>$request->merit_ratings_descrip,
            'percentage'=> 20,
            'merit_rating_type_id'=>$request->type,
        ]);

        $new_available_M_R = AvailableMeritRating::create([
            'call_types_id' => $request->type,
            'merit_ratings_id'=> $new_merit->id,
        ]);


        $var1 = $request->input('defec_description');
        $var2 = $request->input('defec_percentage');
        $var3 = $request->input('act_descrip');
        $var4 = $request->input('act_perce');
        $var5 = $request->input('descriptions_available_desc_id');
        $var6 = $request->input('availableEspecification_desc_id');
        $var7 = $request->input('availableEspecification_esp_id');
        $var8 = $request->input('defec_esp_description');
        $var9 = $request->input('defec_esp_percentage');
        $var10 = $request->input('act_esp_descrip');
        $var11 = $request->input('act_esp_perce');
        $var12 = $request->input('available_points_id');
        $var13 = $request->input('availablePoints_esp_id');
        $var14 = $request->input('default_points');
        $var15 = $request->input('default_points_description');
        $var16 = $request->input('act_points');
        $var17 = $request->input('act_points_description');


        /* dd($var10);*/
        for ($i=0; $i < count($var5); $i++) {
            if ($var1[$i] == $var3[$i] && $var2[$i] == $var4[$i] ) {
                AvailableDescription::create([
                    'merit_ratings_id' => $new_merit->id,
                    'description_metirs_id'=> $var5[$i],
                ]);
                for ($j=0; $j <count($var6) ; $j++) {
                    if ($var6[$j] == $var5[$i]) {
                        if ($var8[$j] == $var10[$j] && $var9[$j] == $var11[$j]) {
                            AvailableEspecifications::create([
                                'description_metirs_id' => $var6[$j],
                                'especifications_id'=> $var7[$j],
                                'merits_id'=> $new_merit->id,
                            ]);
                            for ($h=0; $h<count($var12) ; $h++) {
                                if ( $var13[$h] == $var7[$j] ) {
                                    if($var14[$h] == $var16[$h] && $var15[$h] == $var17[$h]){
                                        AvailablePoints::create([
                                            'especification_id' => $var7[$j],
                                            'points_id'=> $var12[$h],
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }else {
                                        $new_point = Points::create([
                                            'points' => $var16[$h],
                                            'description'=> $var17[$h],

                                        ]);

                                        AvailablePoints::create([
                                            'especification_id' => $var7[$j],
                                            'points_id'=> $new_point->id,
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }
                                }
                            }


                        }else {
                            $new_especitication = Especification::create([
                                'name' => $var10[$j],
                                'percentage'=> $var11[$j],
                                /* 'description_id' => $var5[$i], */
                            ]);

                            AvailableEspecifications::create([
                                'description_metirs_id' => $var6[$j],
                                'especifications_id'=> $new_especitication->id,
                                'merits_id'=> $new_merit->id,
                            ]);

                            for ($h=0; $h<count($var12) ; $h++) {
                                if ( $var13[$h] == $var7[$j] ) {
                                    if($var14[$h] == $var16[$h] && $var15[$h] == $var17[$h]){
                                        AvailablePoints::create([
                                            'especification_id' => $new_especitication->id,
                                            'points_id'=> $var12[$h],
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }else {
                                        $new_point = Points::create([
                                            'points' => $var16[$h],
                                            'description'=> $var17[$h],

                                        ]);

                                        AvailablePoints::create([
                                            'especification_id' => $new_especitication->id,
                                            'points_id'=> $new_point->id,
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }
                                }
                            }
                        }

                    }
                }

            }
            else {
                $new_description = Description_metir::create([
                    'name' => $var3[$i],
                    'percentage'=> $var4[$i],
                    'merit_id' => $new_merit->id,
                ]);
                AvailableDescription::create([
                    'merit_ratings_id' => $new_merit->id,
                    'description_metirs_id'=> $new_description->id,
                ]);

                for ($j=0; $j <count($var6) ; $j++) {
                    if ($var6[$j] == $var5[$i]) {
                        if ($var8[$j] == $var10[$j] && $var9[$j] == $var11[$j]) {
                            AvailableEspecifications::create([
                                'description_metirs_id' => $new_description->id,
                                'especifications_id'=> $var7[$j],
                                'merits_id'=> $new_merit->id,
                            ]);

                            for ($h=0; $h<count($var12) ; $h++) {
                                if ( $var13[$h] == $var7[$j] ) {
                                    if($var14[$h] == $var16[$h] && $var15[$h] == $var17[$h]){
                                        AvailablePoints::create([
                                            'especification_id' => $var7[$j],
                                            'points_id'=> $var12[$h],
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }else {
                                        $new_point = Points::create([
                                            'points' => $var16[$h],
                                            'description'=> $var17[$h],

                                        ]);

                                        AvailablePoints::create([
                                            'especification_id' => $var7[$j],
                                            'points_id'=> $new_point->id,
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }
                                }
                            }

                        }else {
                            $new_especitication = Especification::create([
                                'name' => $var10[$j],
                                'percentage'=> $var11[$j],
                               /*  'description_id' => $var5[$i], */
                            ]);

                            AvailableEspecifications::create([
                                'description_metirs_id' => $new_description->id,
                                'especifications_id'=> $new_especitication->id,
                                'merits_id'=> $new_merit->id,
                            ]);
                            for ($h=0; $h<count($var12) ; $h++) {
                                if ( $var13[$h] == $var7[$j] ) {
                                    if($var14[$h] == $var16[$h] && $var15[$h] == $var17[$h]){
                                        AvailablePoints::create([
                                            'especification_id' => $new_especitication->id,
                                            'points_id'=> $var12[$h],
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }else {
                                        $new_point = Points::create([
                                            'points' => $var16[$h],
                                            'description'=> $var17[$h],

                                        ]);

                                        AvailablePoints::create([
                                            'especification_id' => $new_especitication->id,
                                            'points_id'=> $new_point->id,
                                            'merits_id'=> $new_merit->id,
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return redirect('/call?type='.$request->call_type);
    }
}
