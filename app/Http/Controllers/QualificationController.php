<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\EnrollRequest;

class QualificationController extends Controller
{
    public function index(Request $request, $required_request_id){
        $title = 'Postulantes Habilitados';
        $requirementRequest = $required_request_id;
        $applicants = DB::table('enroll_request')
                        ->select('users.name','auxiliaries.name as item','auxiliaries.id','users.id as id_user', 'required_requests.call_id as call_id')
                        ->where('enroll_request.required_request_id','=',$required_request_id)
                        ->where('enroll_request.enabled','=','true')
                        ->join('users','users.id','=','enroll_request.user_id')
                        ->join('required_requests','required_requests.id','enroll_request.required_request_id')
                        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                        ->join('requests','requests.id','=','available_requests.request_id')
                        ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                        ->get();

        return view('commission.applicants',compact('title','applicants','requirementRequest'));
    }
}
