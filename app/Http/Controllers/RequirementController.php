<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequirementFormRequest;
use App\Models\Requirement as ModelRequirement;

class RequirementController extends Controller
{
    public function create(){

        $title = 'Convocatoria';
        return view('calls/requirements/requirement_create',compact('title'));
    }

    public function store(RequirementFormRequest $request)
    {
        ModelRequirement::create([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);
        return redirect('/call');

    }
}
