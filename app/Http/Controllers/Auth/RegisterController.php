<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Models\AssignedRole;
use App\Models\PersonalData;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'codeSis' => ['required','min:9','max:9','unique:personal_data'],
            'ci' => ['required','min:6','max:10','unique:personal_data'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (auth()->check()){
            $new_user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            AssignedRole::create([
                'user_id'=>$new_user->id,
                'rol_id'=>$data['role_de_user'],
            ]);

            return $new_user;
        }else {
            $new_user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            AssignedRole::create([
                'user_id'=>$new_user->id,
                'rol_id'=> 4,
            ]);
            PersonalData::create([
                'codeSis' => $data['codeSis'],
                'ci' => $data['ci'],
                'user_id' => $new_user->id
            ]);
            return $new_user;
        }
    }

    public function showRegistrationForm()
    {
        $title = "Registrarse";
            return view('auth.register', compact('title'));
    }
}
