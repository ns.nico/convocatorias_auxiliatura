<?php

namespace App\Http\Controllers;
use App\Models\AssignedRole;
use App\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AssignedRoleFormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\PersonalData;

class AssignedRoleController extends Controller
{   
    public function index(){
        $title = "Nuevo usuario";
        $rolls = Roles::all();

        return view('calls.admin.components.register_user.register_user',compact('title','rolls'));
    }

    protected function store(AssignedRoleFormRequest $request)
    {
        if (auth()->check()){
            $new_user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            AssignedRole::create([
                'user_id'=>$new_user->id,
                'rol_id'=>$request->role_de_user,
            ]);

            if($request->role_de_user == 4){
                PersonalData::create([
                    'codeSis' => $request->codeSis,
                    'ci' => $request->ci,
                    'user_id' => $new_user->id
                ]);
            }

            $message = "Se creo un nuevo usuario con exito";

            return redirect('/home-admin')->with('message',$message);
        }
    }
}
