<?php

namespace App\Http\Controllers;
use App\Models\Commission;
use App\Models\CommissionRequired;
use App\Models\CommissionEspecifications;
use App\Models\Call;
use App\Models\CommissionUsers;
use App\Models\CommissionType;
use App\Http\Controllers\CallController;

use Illuminate\Http\Request;
use App\Http\Requests\CommissionRequest;
use Illuminate\Support\Facades\DB;

class CommissionController extends Controller
{
    public function create(Request $request, $call_id){
        $title = "Comision";
        $commission_types = DB::table('commission_types')->get();
        $users = DB::table('roles')->select('users.id', 'users.name','roles.rol')
            ->join('assigned_roles', 'assigned_roles.rol_id', '=', 'roles.id')
            ->join('users','users.id','=','assigned_roles.user_id')
            ->where('roles.rol','=','Docente')
            ->orWhere('roles.rol','=','Administrador')
            ->get();
        $call_type = $this::getCallType($call_id);

        if($call_type->name == 'Auxiliatura de Docencia'){
            $items = $this::getItemsByCallId($call_id);
        }
        else{
            $items = $this::getItemsSpecificationsByCallId($call_id);
        }
        
        return view('commission.create_commission',compact('title','commission_types','users','call_id','items','call_type'));
    }

    public function store(CommissionRequest $request, $call_id){
        $call = Call::findOrFail($call_id);
        $commission_type = CommissionType::findOrFail($request->commission_type);

        $commission = Commission::create([
            'name' => $commission_type->name . $call->id,
            'commission_type_id' => $commission_type->id
        ]);
        
        $call_type = $this::getCallType($call_id);
        
        //CommissionRequired
        if($commission_type->name == 'Comision de Meritos'){
            $items = $this::getItemsByCallId($call_id);
            foreach($items as $item){
                CommissionRequired::create([
                    'request_id' => $item->id,
                    'commission_id' => $commission->id
                ]);
            }
        }
        else{
            if($call_type->name == 'Auxiliatura de Docencia'){
                CommissionRequired::create([
                    'request_id' => $request->item_id,
                    'commission_id' => $commission->id
                ]);
            }
            else{
                CommissionEspecifications::create([
                    'commission_id' => $commission->id,
                    'especification_id' => $request->item_id,
                    'call_id' => $call_id
                ]);
            }
        }
        //CommissionUsers
        foreach($request->input('users.*') as $id){
            $leader = $request->leader == $id;
            CommissionUsers::create([
                'leader' => $leader,
                'user_id' => $id,
                'commission_id' => $commission->id
            ]);
        }

        $message = "Se creo la comision con exito";
        return redirect('/home-admin')->with('message',$message);
    }

    public function show(Request $request, $commission_id){
        $title = "Detalle de la Comision";
        $commission = DB::table('commissions')
                        ->select('commission_types.name as type')
                        ->where('commissions.id',$commission_id)
                        ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                        ->get()[0];

        $users = DB::table('commission_users')
                    ->select('users.name as name','roles.rol as role', 'commission_users.leader as leader')
                    ->join('users','users.id','=','commission_users.user_id')
                    ->join('assigned_roles','assigned_roles.user_id','=','users.id')
                    ->join('roles','roles.id','=','assigned_roles.rol_id')
                    ->where('commission_users.commission_id',$commission_id)
                    ->get();

        $item = DB::table('commission_requireds')
                ->select('auxiliaries.name as name')
                ->where('commission_requireds.commission_id',$commission_id)
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->get();

        if(count($item) == 0){
            $item = DB::table('commission_especifications')
                ->select('especifications.name as name')
                ->join('especifications','especifications.id','=','commission_especifications.especification_id')
                ->where('commission_especifications.commission_id','=',$commission_id)
                ->get();
        }

        return view('commission.detail_commission',compact('title','commission','users','item'));
    }

    private function getItemsByCallId($call_id){
        return DB::table('required_requests')->select('required_requests.id', 'auxiliaries.name')
        ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
        ->join('requests','requests.id','=','available_requests.request_id')
        ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
        ->where('required_requests.call_id','=',$call_id)
        ->get();
    }

    private function getItemsSpecificationsByCallId($call_id){
        return DB::table('calls')
        ->select('especifications.id as id','especifications.name as name')
        ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
        ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
        ->join('merit_ratings','merit_ratings.id','=','available_merit_ratings.merit_ratings_id')
        ->join('available_descriptions','available_descriptions.merit_ratings_id','=','merit_ratings.id')
        ->join('description_metirs','description_metirs.id','=','available_descriptions.description_metirs_id')
        ->join('available_especifications','available_especifications.description_metirs_id','=','description_metirs.id')
        ->join('especifications','especifications.id','=','available_especifications.especifications_id')
        ->where([['calls.id',$call_id],
                ['merit_ratings.merit_rating_type_id',4]])
        ->distinct('especifications.name')->get();
    }

    private function getCallType($call_id){
        return DB::table('calls')
            ->select()
            ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
            ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
            ->join('call_types','call_types.id','=','available_merit_ratings.call_types_id')
            ->where('calls.id',$call_id)
            ->get()[0];
    }
}
