<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
//Models for function index
use App\Models\Template;
use App\Models\Roles;
//Models for function create
use App\Models\AcademicUnit;
use App\Models\Request as ModelRequest;
use App\Models\Requisite;
use App\Models\Document;
use App\Models\Date as ModelCallDates;
use App\Models\Event;
use App\Models\AvailableRequest;
use App\Models\AvailableRequisite;
use App\Models\AvailableDocument;
use App\Models\AvailableEvent;
use App\Models\Description_metir;
use App\Models\Points;
use App\Models\Especification;
use App\Models\Merit_rating;
use App\Models\AvailableMeritRating;
use App\Models\AvailableDescription;
use App\Models\AvailableEspecifications;
use App\Models\AvailablePoints;


//Models for function store
use App\Models\Call;
use App\Models\CallType;
use App\Models\RequiredRequest;
use App\Models\RequiredRequisite;
use App\Models\RequiredDocument;
use App\Models\RequiredCallMerits;

use App\Http\Requests\CallRequest;

class CallController extends Controller
{
    public const NUEVO = '0';

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $user_id = Auth::id();
        $title = 'Convocatoria';
        $templates = Template::all();
        $all_calls = Call::where('call_status_id',1)->paginate(10);
        $public_calls = Call::where('call_status_id',2)->paginate(10);
        $rolls = Roles::all();
        $commissions_merits = $this::getCommissionMeritsByUser($user_id,'Comision de Meritos');
        $commissions_knowledge = $this::getCommissionByUser($user_id,'Comision de Conocimiento');
        $commissions_knowledge_lab = $this::getCommissionByUserLab($user_id,'Comision de Conocimiento');

        $commissions_merits_nl = $this::getCommissionMeritsByUserNoLeader($user_id,'Comision de Meritos');
        $commissions_knowledge_nl = $this::getCommissionByUserNoLeader($user_id,'Comision de Conocimiento');
        $commissions_knowledge_lab_nl = $this::getCommissionByUserLabNoLeader($user_id,'Comision de Conocimiento');

        $users = DB::table('users')
                ->select('users.name as name','users.email as email','roles.rol as role')
                ->join('assigned_roles','assigned_roles.user_id','=','users.id')
                ->join('roles','roles.id','=','assigned_roles.rol_id')
                ->paginate(20);
        
        return view('calls.admin.index',
        compact('title','users','templates','all_calls','public_calls','rolls',
                'commissions_merits','commissions_knowledge','commissions_knowledge_lab',
                'commissions_merits_nl','commissions_knowledge_nl','commissions_knowledge_lab_nl'));
    }

    public function create(Request $request)
    {
        $call_type = $request->type;
        return $this::sendDataView($call_type);
    }

    protected function sendDataView($call_type)
    {
        $title = 'Nueva Convocatoria';
        $name = '';
        $description= '';
        $academic_units = AcademicUnit::all();

        if($call_type == $this::NUEVO){
            $requests = ModelRequest::all();
            $requisites = Requisite::all();
            $documents = Document::all();
            $events = Event::all();
            /* $merits_ratings = Merit_rating::all(); */
            $merits_ratings = Merit_rating::where('merit_rating_type_id', 1)->orWhere('merit_rating_type_id', 2)->get();
            $availableMeritRating = AvailableMeritRating::all();
            $type = CallType::all();
            $availablePoints = AvailablePoints::all();
            $availableDescription = AvailableDescription::all();
            $availableEspecifications = AvailableEspecifications::all();
            $availableMeritRatings_select = AvailableMeritRating::all();
        }
        else{
            $template = Template::findOrFail($call_type);
            $title .= ' '.$template->template_name;
            $name = $template->name;
            $description = $template->description;
            $availableMeritRating = AvailableMeritRating::all();
            $availablePoints = AvailablePoints::all();
            $availableDescription = AvailableDescription::all();
            $availableEspecifications = AvailableEspecifications::all();
            $availableMeritRatings_select = AvailableMeritRating::where('call_types_id', $call_type)->get();
            $availableMeritRatings = AvailableMeritRating::where('call_types_id',$call_type)->first();
            /* $merits = Merit_rating::findOrFail($availableMeritRatings->merit_ratings_id); */
            $merits_ratings = Merit_rating::where('merit_rating_type_id', 1)->orWhere('merit_rating_type_id', 2)->get();

            $requests = AvailableRequest::where('call_type_id',$call_type)->get();
            $requests= $requests->transform(function ($requests){
                return ModelRequest::findOrFail($requests->request_id);
            });
            $requisites = AvailableRequisite::where('call_type_id',$call_type)->get();
            $requisites= $requisites->transform(function ($requisites){
                return Requisite::findOrFail($requisites->requisite_id);
            });
            $documents = AvailableDocument::where('call_type_id',$call_type)->get();
            $documents= $documents->transform(function ($documents){
                return Document::findOrFail($documents->document_id);
            });
            $events = AvailableEvent::where('call_type_id',$call_type)->get();
            $events= $events->transform(function ($events){
                return Event::findOrFail($events->event_id);
            });
        }
        return view('calls.admin.create',
        compact('title', 'name', 'description', 'call_type','academic_units','requests','requisites', 'documents','events','merits_ratings','availableMeritRating','type','availablePoints','availableDescription','availableEspecifications','availableMeritRatings_select' ));
    }

    public function store(CallRequest $request, $call_type)
    {   /* dd($request->all()); */

        $new_call = Call::create([

            'name'=>$request->name,
            'description'=>$request->description,
            'academic_unit_id'=>$request->academic_unit,
            'management'=>$request->management,
            'year'=>$request->year,
            'call_status_id'=>1
        ]);

        RequiredCallMerits::create([
            'calls_id' => $new_call->id,
            'available_merit_id'=> $request->merito_disponible,
        ]);
        if ($call_type == 1) {
            RequiredCallMerits::create([
                'calls_id' => $new_call->id,
                'available_merit_id'=> 3,
            ]);
        }else {
            RequiredCallMerits::create([
                'calls_id' => $new_call->id,
                'available_merit_id'=> 4,
            ]);
        }

        if(($request->input('requests')) != null){
            $model = new RequiredRequest();
            $available_model = new AvailableRequest();
            $this->fillDatesIn('requests.*', $model, 'request_id', $available_model, 'available_request_id', $request,$new_call);
        }
        if(($request->input('requisites')) != null){
            $model = new RequiredRequisite();
            $available_model = new AvailableRequisite();
            $this->fillDatesIn('requisites.*', $model, 'requisite_id', $available_model, 'available_requisite_id', $request,$new_call);
        }
        if(($request->input('documents')) != null){
            $model = new RequiredDocument();
            $available_model = new AvailableDocument();
            $this->fillDatesIn('documents.*', $model, 'document_id', $available_model, 'available_document_id', $request,$new_call);
        }
        if(($request->input('dates')) != null){
            $model = new ModelCallDates();
            $this->insertDates($request->input('dates'), $model,'available_event_id',$request,$new_call);
        }

        $message = "Se creo la convocatoria con exito";

        return redirect('/home-admin')->with('message',$message);
    }

    public function edit(Request $request,$call_id){

        $call_type = $this::getCallType($call_id);
        $call = Call::findOrFail($call_id);
        $title = 'Editar Convocatoria';
        $name = $call->name;
        $description= $call->description;
        $management = $call->management;
        $year = $call->year;
        $call_academic = AcademicUnit::findOrFail($call->academic_unit_id);
        $academic_units = AcademicUnit::all();

        $availableMeritRating = AvailableMeritRating::all();
        $availablePoints = AvailablePoints::all();
        $availableDescription = AvailableDescription::all();
        $availableEspecifications = AvailableEspecifications::all();
        $availableMeritRatings_select = AvailableMeritRating::where('call_types_id', $call_type)->get();
        $availableMeritRatings = AvailableMeritRating::where('call_types_id',$call_type)->first();
        /* $merits = Merit_rating::findOrFail($availableMeritRatings->merit_ratings_id); */
        $merits_ratings = Merit_rating::where('merit_rating_type_id', 1)->orWhere('merit_rating_type_id', 2)->get();

        $call_requests = DB::table('calls')
                        ->select('requests.id','requests.quantity','requests.academic_hours','auxiliaries.code','auxiliaries.name')
                        ->join('required_requests','required_requests.call_id','=','calls.id')
                        ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
                        ->join('requests','requests.id','=','available_requests.request_id')
                        ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                        ->where('calls.id',$call_id)
                        ->get();

        $requests = DB::table('available_requests')
                    ->select('requests.id','requests.quantity','requests.academic_hours','auxiliaries.code','auxiliaries.name')
                    ->join('requests','requests.id','=','available_requests.request_id')
                    ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                    ->where('available_requests.call_type_id',$call_type)
                    ->get()->toArray();
        

        foreach($call_requests as $req){
            if(($key = array_search($req, $requests)) !== false ){
                unset($requests[$key]);
            }
        }

        $call_requisites = DB::table('calls')
                        ->select('requisites.id','requisites.name','requisites.description')
                        ->join('required_requisites','required_requisites.call_id','=','calls.id')
                        ->join('available_requisites','available_requisites.id','=','required_requisites.available_requisite_id')
                        ->join('requisites','requisites.id','=','available_requisites.requisite_id')
                        ->where('calls.id',$call_id)
                        ->get();

        $requisites = DB::table('available_requisites')
                    ->select('requisites.id','requisites.name','requisites.description')
                    ->join('requisites','requisites.id','=','available_requisites.requisite_id')
                    ->where('available_requisites.call_type_id',$call_type)
                    ->get()->toArray();

        foreach($call_requisites as $req){
            if(($key = array_search($req, $requisites)) !== false ){
                unset($requisites[$key]);
            }
        }

        $call_documents = DB::table('calls')
                        ->select('documents.id','documents.name','documents.description')
                        ->join('required_documents','required_documents.call_id','=','calls.id')
                        ->join('available_documents','available_documents.id','=','required_documents.available_document_id')
                        ->join('documents','documents.id','=','available_documents.document_id')
                        ->where('calls.id',$call_id)
                        ->get();

        $documents = DB::table('available_documents')
                    ->select('documents.id','documents.name','documents.description')
                    ->join('documents','documents.id','=','available_documents.document_id')
                    ->where('available_documents.call_type_id',$call_type)
                    ->get()->toArray();

        foreach($call_documents as $req){
            if(($key = array_search($req, $documents)) !== false ){
                unset($documents[$key]);
            }
        }

        $call_events = DB::table('calls')
                        ->select('events.id','events.name','dates.start','dates.end')
                        ->join('dates','dates.call_id','=','calls.id')
                        ->join('available_events','available_events.id','=','dates.available_event_id')
                        ->join('events','events.id','=','available_events.event_id')
                        ->where('calls.id',$call_id)
                        ->get();

        $events = DB::table('available_events')
                    ->select('events.id','events.name','events.description')
                    ->join('events','events.id','=','available_events.event_id')
                    ->where('available_events.call_type_id',$call_type)
                    ->get()->toArray();

        return view('calls.admin.edit',
        compact('title','call_id', 'name', 'description','management','year','call_type','call_academic', 
                            'call_requests', 'call_requisites','call_documents','academic_units','call_events',
                            'requests','requisites', 'documents','events',
                            'merits_ratings','availableMeritRating','type','availablePoints','availableDescription','availableEspecifications','availableMeritRatings_select' ));
    }

    public function update(CallRequest $request,$call_id)
    {
        $call = Call::findOrFail($call_id);
        $call->name = $request->name;
        $call->description = $request->description;
        $call->academic_unit_id = $request->academic_unit;
        $call->management = $request->management;
        $call->year = $request->year;
        $call->update();

        $call_requests = RequiredRequest::where('call_id',$call_id)->delete();
        if(($request->input('requests')) != null){
            $model = new RequiredRequest();
            $available_model = new AvailableRequest();
            $this->fillDatesIn('requests.*', $model, 'request_id', $available_model, 'available_request_id', $request,$call);
        }

        $call_requisites = RequiredRequisite::where('call_id',$call_id)->delete();
        if(($request->input('requisites')) != null){
            $model = new RequiredRequisite();
            $available_model = new AvailableRequisite();
            $this->fillDatesIn('requisites.*', $model, 'requisite_id', $available_model, 'available_requisite_id', $request,$call);
        }

        $call_documents = RequiredDocument::where('call_id',$call_id)->delete();
        if(($request->input('documents')) != null){
            $model = new RequiredDocument();
            $available_model = new AvailableDocument();
            $this->fillDatesIn('documents.*', $model, 'document_id', $available_model, 'available_document_id', $request,$call);
        }

        $call_events = ModelCallDates::where('call_id',$call_id)->delete();
        if(($request->input('dates')) != null){
            $model = new ModelCallDates();
            $this->insertDates($request->input('dates'), $model,'available_event_id',$request,$call);
        }

        $message = "Se actualizo la convocatoria con exito";

        return redirect('/home-admin')->with('message',$message);
    }

    public function publicCall(Request $request, $call_id)
    {
        $call = Call::findOrFail($call_id);
        $call->call_status_id = 2;
        $call->update();
        $message = "Se publico la convocatoria con exito";

        return redirect('/home-admin')->with('message',$message);
    }

    protected function fillDatesIn($array, $model, $model_id, $available_model, $available_model_id, $request, $call)
    {
        foreach($request->input($array) as $id){
            $id_available = $available_model::where($model_id,'=', $id)->first()->id;
            $model::create([
                'call_id' => $call->id,
                $available_model_id => $id_available
            ]);
        }
    }
    protected function insertDates($dates, $model, $model_id, $request, $call)
    {
        for($i = 0; $i < count($dates); $i+=3){
            $event = Event::firstWhere('name', $dates[$i]);
            $available_event = null;
            if($event === null){
                $event = Event::create([
                    'name' => $dates[$i],
                    'description' => 'nuevo',
                ]);
                $call_types = CallType::all();
                foreach($call_types as $type){
                    $available_event = AvailableEvent::create([
                        'event_id' => $event->id,
                        'call_type_id' => $type->id
                    ]);
                }
            }
            else{
                $available_event = AvailableEvent::firstWhere('event_id',$event->id);
            }
            $date_start = $dates[$i+1];
            $date_end = $dates[$i+2];
            $model::create([
                'start' => $date_start,
                'end' => $date_end,
                $model_id => $available_event->id,
                'call_id' => $call->id,
            ]);
        }
    }

    private function getCommissionByUser($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->get();
    }

    private function getCommissionByUserNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('commissions.id as id','auxiliaries.name','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->get();
    }

    private function getCommissionByUserLab($user_id, $commission_type){
        return DB::table('users')
                ->select('especifications.name','calls.name as call_name','calls.id as call_id','calls.year','calls.management','especifications.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_especifications','commission_especifications.commission_id','=','commissions.id')
                ->join('especifications','especifications.id','=','commission_especifications.especification_id')
                ->join('calls','calls.id','=','commission_especifications.call_id')
                ->get();
    }

    private function getCommissionByUserLabNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('commissions.id as id','especifications.name','calls.name as call_name','calls.id as call_id','calls.year','calls.management','especifications.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_especifications','commission_especifications.commission_id','=','commissions.id')
                ->join('especifications','especifications.id','=','commission_especifications.especification_id')
                ->join('calls','calls.id','=','commission_especifications.call_id')
                ->get();
    }

    private function getCommissionMeritsByUser($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.id as call_id','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->distinct('calls.id')
                ->get();
    }

    private function getCommissionMeritsByUserNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.id as call_id','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id','commissions.id as id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->distinct('calls.id')
                ->get();
    }

    private function getCallType($call_id){
        return DB::table('calls')
            ->select('call_types.id')
            ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
            ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
            ->join('call_types','call_types.id','=','available_merit_ratings.call_types_id')
            ->where('calls.id',$call_id)
            ->get()[0]->id;
    }
}
