<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AcademicUnitRequest;

use App\Models\AcademicUnit;

class AcademicUnitController extends Controller
{
    public function create(Request $request, $call_type){
        $title = "Nueva Unidad Academica";

        return view('calls.admin.components.academic_units.created_academic_units',compact('title','call_type'));
    }

    public function store(AcademicUnitRequest $request){
        
        AcademicUnit::create([
            'name' => $request->name
        ]);

        return redirect('/call?type='.$request->call_type);
    }
}
