<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Models\Call;
use App\Models\EnrollRequest;
use App\Models\RequiredRequest;
use App\Models\RequiredDocument;
use App\Models\DeliveredDocument;
use App\Models\Description_grades;
use App\Models\Especification_grades;
use App\User;

class CheckRequisiteController extends Controller
{
    public function index(){
        $title = "Convocatorias";
        $calls = Call::where('call_status_id',2)->paginate(10);
        return view('calls.secretary.home_secretary',compact('title','calls'));
    }

    public function checkRequisite(Request $request, $call_id){
        $call = Call::findOrFail($call_id);
        $title = $call->name;
        $enrolls = DB::table('enroll_request')
                    ->select('enroll_request.id as id','users.name as applicant','auxiliaries.name as item')
                    ->join('users','users.id','=','enroll_request.user_id')
                    ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
                    ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
                    ->join('requests','requests.id','=','available_requests.request_id')
                    ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                    ->join('calls','calls.id','=','required_requests.call_id')
                    ->where('calls.id',$call_id)
                    ->where('enroll_request.reviewed',false)
                    ->get();
        return view('calls.secretary.call_options',compact('title','call','enrolls'));
    }

    public function register(Request $request, $call_id, $enroll_id){
        $title = "Registrar Documentos";
        $documents = RequiredDocument::where('call_id',$call_id)->get();
        $call = Call::findOrFail($call_id);
        $enroll = EnrollRequest::findOrfail($enroll_id);
        $user = DB::table('users')
                ->select('users.name as name', 'personal_data.codeSis as codeSis', 'personal_data.ci as ci')
                ->join('personal_data','personal_data.user_id','=','users.id')
                ->where('users.id',$enroll->user()->id)
                ->get()[0];
        //dd($user);

        return view('calls.secretary.register_documents',compact('title','documents','call', 'user','enroll'));
    }

    public function store(Request $request,$call_id , $enroll_request_id){

        if($request->input('documents.*')){
            foreach($request->input('documents.*') as $id){
                DeliveredDocument::create([
                    'enroll_request_id' => $enroll_request_id,
                    'required_document_id' => $id
                ]);
            }
        }

        $documents = RequiredDocument::where('call_id',$call_id)->get();
        $enroll_request = EnrollRequest::findOrFail($enroll_request_id);
        $enroll_request->reviewed = true;
        if( $request->input('documents.*') && count($documents) == count($request->input('documents.*'))){
            $enroll_request->enabled = true;
            $call_type = $this::getCallType($enroll_request_id);
            if($call_type[0]->name == 'Auxiliatura de Docencia'){
                $meritsKnow = $this::getMeritsKnowDoc($enroll_request_id);
                foreach($meritsKnow as $mk ){
                    Description_grades::create([
                        'description_merit_id' => $mk->id,
                        'enroll_request_id' => $enroll_request_id,
                        'grade' => 0
                    ]);
                }
            }
            else{
                $code = $this::getCodeMerit($enroll_request_id);
                $meritsKnow = $this::getMeritsKnowLab($enroll_request_id,$code[0]->code);
                Description_grades::create([
                    'description_merit_id' => $meritsKnow[0]->des_id,
                    'enroll_request_id' => $enroll_request_id,
                    'grade' => 0
                ]);
                foreach($meritsKnow as $mk ){
                    Especification_grades::create([
                        'specification_id' => $mk->esp_id,
                        'enroll_request_id' => $enroll_request_id,
                        'grade' => 0
                    ]);
                }
            }

            /*-------------------------------- agregar notas tipo merito ---------------------------------------*/

            $type_call= DB::table('calls')
            ->select('available_merit_ratings.call_types_id')
            ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
            ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
            ->where('calls.id','=',$call_id)
            ->first();

            $merit_table= DB::table('calls')
            ->select('merit_ratings.id')
            ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
            ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
            ->join('merit_ratings','merit_ratings.id','=','available_merit_ratings.merit_ratings_id')
            ->where('calls.id','=',$call_id)
            ->where('merit_ratings.merit_rating_type_id','=',$type_call->call_types_id)
            ->first();

            $description_merit = DB::table('available_descriptions')
            ->select('description_metirs.id as description_id','description_metirs.name','description_metirs.percentage')
            ->join('description_metirs','description_metirs.id','=','available_descriptions.description_metirs_id')
            ->where('available_descriptions.merit_ratings_id','=',$merit_table->id)
            ->get();

            $specification_merit = DB::table('available_especifications')
            ->select('especifications.id as specifi_id','especifications.name','especifications.percentage','available_especifications.especifications_id','available_especifications.description_metirs_id')
            ->join('especifications','especifications.id','=','available_especifications.especifications_id')
            ->where('available_especifications.merits_id','=',$merit_table->id)
            ->get();

            foreach($description_merit as $description ){
                Description_grades::create([
                    'description_merit_id' => $description->description_id,
                    'enroll_request_id' => $enroll_request_id,
                    'grade' => 0
                ]);
            }

            foreach($specification_merit as $specification ){
                Especification_grades::create([
                    'specification_id' => $specification->specifi_id,
                    'enroll_request_id' => $enroll_request_id,
                    'grade' => 0
                ]);
            }
        }
        $enroll_request->save();


        $call = Call::findOrFail($call_id);

        return redirect()->route('call_options',$call->id);
    }

    private function getCallType($enroll_request_id){
        return DB::table('enroll_request')
        ->select('call_types.name')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
        ->join('call_types','call_types.id','=','available_requests.call_type_id')
        ->where('enroll_request.id',$enroll_request_id)
        ->get();
    }

    private function getMeritsKnowDoc($enroll_request_id){
        return  DB::table('enroll_request')
        ->select('description_metirs.id')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('calls','calls.id','=','required_requests.call_id')
        ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
        ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
        ->join('merit_ratings','merit_ratings.id','=','available_merit_ratings.merit_ratings_id')
        ->join('available_descriptions','available_descriptions.merit_ratings_id','=','merit_ratings.id')
        ->join('description_metirs','description_metirs.id','=','available_descriptions.description_metirs_id')
        ->where([['enroll_request.id',$enroll_request_id],
                ['merit_ratings.merit_rating_type_id',3]])
        ->get();
    }

    private function getMeritsKnowLab($enroll_request_id,$code){
        return DB::table('enroll_request')
        ->select('description_metirs.id as des_id','especifications.id as esp_id')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('calls','calls.id','=','required_requests.call_id')
        ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
        ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
        ->join('merit_ratings','merit_ratings.id','=','available_merit_ratings.merit_ratings_id')
        ->join('available_descriptions','available_descriptions.merit_ratings_id','=','merit_ratings.id')
        ->join('description_metirs','description_metirs.id','=','available_descriptions.description_metirs_id')
        ->join('available_especifications','available_especifications.description_metirs_id','=','description_metirs.id')
        ->join('especifications','especifications.id','=','available_especifications.especifications_id')
        ->where([['enroll_request.id',$enroll_request_id],
                ['description_metirs.name',$code],
                ['merit_ratings.merit_rating_type_id',4]])
        ->get();
    }

    private function getCodeMerit($enroll_request_id){
        return DB::table('enroll_request')
        ->select('auxiliaries.code')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
        ->join('requests','requests.id','=','available_requests.request_id')
        ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
        ->where('enroll_request.id',$enroll_request_id)
        ->get();
    }
}
