<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\RequiredRequisite;
use App\Models\AvailableRequisite;
use App\Models\Description_metir;
use App\Models\Points;
use App\Models\Especification;
use App\Models\AvailableDescription;
use App\Models\AvailableEspecifications;
use App\Models\AvailablePoints;
use App\Models\DescriptionGrade;
use App\Models\SpecificationGrade;

use App\Http\Requests\MeritQualificationRequest;

class MeritQualificationController extends Controller
{
    public function index( $call_id)
    {   /* dd($call_id); */

        $title = 'Calificación de Meritos';
        $list_of_applicants= DB::table('users')
        ->select('users.id','users.name','auxiliaries.name as auxi_name','enroll_request.id as enroll_id','auxiliaries.code')
        ->join('enroll_request','enroll_request.user_id','=','users.id')
        ->join('required_requests','required_requests.id','=','enroll_request.required_request_id')
        ->join('available_requests','available_requests.id','=','required_requests.available_request_id')
        ->join('requests','requests.id','=','available_requests.request_id')
        ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
        ->where('required_requests.call_id','=',$call_id)
        ->where('enroll_request.enabled','=','true')
        ->get();

        $primer_id = $list_of_applicants->first();
        /* dd($primer_id->enroll_id); */
        $type_call= DB::table('calls')
        ->select('available_merit_ratings.call_types_id')
        ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
        ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
        ->where('calls.id','=',$call_id)
        ->first();
        /* dd($type_call->call_types_id); */

        $merit_table= DB::table('calls')
        ->select('merit_ratings.id as id_merit')
        ->join('required_call_merit','required_call_merit.calls_id','=','calls.id')
        ->join('available_merit_ratings','available_merit_ratings.id','=','required_call_merit.available_merit_id')
        ->join('merit_ratings','merit_ratings.id','=','available_merit_ratings.merit_ratings_id')
        ->where('calls.id','=',$call_id)
        ->where('merit_ratings.merit_rating_type_id','=',$type_call->call_types_id)
        ->first();
        /* dd($merit_table->id_merit); */

        $description_merit = DB::table('available_descriptions')
        ->select('description_metirs.id as description_id','description_metirs.name','description_metirs.percentage')
        ->join('description_metirs','description_metirs.id','=','available_descriptions.description_metirs_id')
        ->where('available_descriptions.merit_ratings_id','=',$merit_table->id_merit)
        ->get();
        /* dd($description_merit->all()); */
        $specification_merit = DB::table('available_especifications')
        ->select('especifications.id as specifi_id','especifications.name','especifications.percentage','available_especifications.especifications_id','available_especifications.description_metirs_id')
        ->join('especifications','especifications.id','=','available_especifications.especifications_id')
        ->where('available_especifications.merits_id','=',$merit_table->id_merit)
        ->get();
        /* dd($specification_merit->all()); */

        $points_merit = DB::table('available_points')
        ->select('points.id as points_id','points.description','points.points','available_points.especification_id')
        ->join('points','points.id','=','available_points.points_id')
        ->where('available_points.merits_id','=',$merit_table->id_merit)
        ->get();
        /* dd($description_merit); */
        $description_grade = DescriptionGrade::all();
        $specification_grade = SpecificationGrade::all();

        return view('commission.components.merit_qualification', compact('title','list_of_applicants','description_merit','specification_merit','points_merit','primer_id','specification_grade','description_grade','call_id'));
    }
    protected function store(MeritQualificationRequest $request)
    {   /* dd($request->all()); */
        $specif_grade = $request->input('specif_grade');
        $id_specification_grade = $request->input('id_specification_grade');
        $id_description_grade = $request->input('id_description_grade');
        for ($i=0; $i <count($specif_grade) ; $i++) {
            if ($specif_grade[$i] == 0) {
                $update_especification = SpecificationGrade::where('id',$id_specification_grade[$i])->first();

                $update_description = DescriptionGrade::where('id',$id_description_grade[$i])->first();
                $nota_anterior = $update_especification->grade;
                $update_description->grade = (($update_description->grade)-$nota_anterior) + $specif_grade[$i];
                $update_description->save();

                $update_especification->grade = $specif_grade[$i];
                $update_especification->save();
            }else {
                $update_especification = SpecificationGrade::where('id',$id_specification_grade[$i])->first();
                if ($update_especification->grade == 0) {
                    $update_especification->grade = $specif_grade[$i];
                    $update_especification->save();

                    $update_description = DescriptionGrade::where('id',$id_description_grade[$i])->first();
                    $update_description->grade = ($update_description->grade)+$specif_grade[$i];
                    $update_description->save();
                }else {
                    $update_description = DescriptionGrade::where('id',$id_description_grade[$i])->first();
                    $nota_anterior = $update_especification->grade;
                    $update_description->grade = ($update_description->grade)-$nota_anterior + $specif_grade[$i];
                    $update_description->save();

                    $update_especification->grade = $specif_grade[$i];
                    $update_especification->save();
                }
            }

        }

        return redirect()->route('merit_qualification', $request->call_id);
    }
}
