<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TeacherHome extends Controller
{
    public function index(){
        $title = "Convocatorias";
        $user_id = Auth::id();
        
        $commissions_merits = $this::getCommissionMeritsByUser($user_id,'Comision de Meritos');
        $commissions_knowledge = $this::getCommissionByUser($user_id,'Comision de Conocimiento');
        $commissions_knowledge_lab = $this::getCommissionByUserLab($user_id,'Comision de Conocimiento');

        
        $commissions_merits_nl = $this::getCommissionMeritsByUserNoLeader($user_id,'Comision de Meritos');
        $commissions_knowledge_nl = $this::getCommissionByUserNoLeader($user_id,'Comision de Conocimiento');
        $commissions_knowledge_lab_nl = $this::getCommissionByUserLabNoLeader($user_id,'Comision de Conocimiento');
        
        return view('calls.teacher.home_teacher',compact('title',
        'commissions_knowledge','commissions_knowledge_lab','commissions_merits',
        'commissions_merits_nl','commissions_knowledge_nl','commissions_knowledge_lab_nl'));
    }

    private function getCommissionByUser($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->get();
    }

    private function getCommissionByUserNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('commissions.id as id','auxiliaries.name','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->get();
    }

    private function getCommissionByUserLab($user_id, $commission_type){
        return DB::table('users')
                ->select('especifications.name','calls.name as call_name','calls.id as call_id','calls.year','calls.management','especifications.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_especifications','commission_especifications.commission_id','=','commissions.id')
                ->join('especifications','especifications.id','=','commission_especifications.especification_id')
                ->join('calls','calls.id','=','commission_especifications.call_id')
                ->get();
    }

    private function getCommissionByUserLabNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('commissions.id as id','especifications.name','calls.name as call_name','calls.id as call_id','calls.year','calls.management','especifications.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_especifications','commission_especifications.commission_id','=','commissions.id')
                ->join('especifications','especifications.id','=','commission_especifications.especification_id')
                ->join('calls','calls.id','=','commission_especifications.call_id')
                ->get();
    }

    private function getCommissionMeritsByUser($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.id as call_id','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','true')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->distinct('calls.id')
                ->get();
    }

    private function getCommissionMeritsByUserNoLeader($user_id, $commission_type){
        return DB::table('users')
                ->select('auxiliaries.name','calls.id as call_id','calls.name as call_name','calls.year','calls.management','required_requests.id as request_id','commissions.id as id')
                ->where('users.id','=',$user_id)
                ->join('commission_users','commission_users.user_id','=','users.id')
                ->where('commission_users.leader','=','false')
                ->join('commissions','commissions.id','=','commission_users.commission_id')
                ->join('commission_types','commission_types.id','=','commissions.commission_type_id')
                ->where('commission_types.name','=',$commission_type)
                ->join('commission_requireds','commission_requireds.commission_id','=','commissions.id')
                ->join('required_requests','required_requests.id','commission_requireds.request_id')
                ->join('available_requests', 'available_requests.id', '=', 'required_requests.available_request_id')
                ->join('requests','requests.id','=','available_requests.request_id')
                ->join('auxiliaries','auxiliaries.id','=','requests.auxiliary_id')
                ->join('calls','calls.id','=','required_requests.call_id')
                ->distinct('calls.id')
                ->get();
    }
}
