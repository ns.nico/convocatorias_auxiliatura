<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EnrollRequestRequest;
use App\Models\EnrollRequest;
use App\Models\RequiredRequest;
use App\Models\Call;
use App\User;
use App\Models\AvailableRequest;
use App\Models\Request as ModelRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EnrollRequestController extends Controller
{
    public function index()
    {
        $title = 'Postularse';
        $calls = Call::where('call_status_id',2)->get();
        return view('calls.student.enroll', compact('title','calls'));
    }

    public function enroll(Request $request, $call_id)
    {
        $title = 'Postularse';
        $required_requests = RequiredRequest::where('call_id',$call_id)->get();
        return view('calls.student.enroll_request', compact('title','required_requests','call_id'));
    }

    public function store(EnrollRequestRequest $request)
    {
        $user_id = Auth::id();
        foreach($request->input('requests.*') as $required_request_id){
            $notEnrolled = $this->notEnrolled($user_id,
                                    $required_request_id,
                                    $request->input('call_id')
                                    );
            if($notEnrolled){
                EnrollRequest::create([
                    'reviewed' => false,
                    'enabled' => false,
                    'user_id'=>$user_id,
                    'required_request_id'=>$required_request_id,
                ]);
                $message = "Se postuló a la convocatoria con exito";
            }
            else{
                $message = "Error al postularse";
            }
        }
        return redirect('/enroll')->with('message',$message);
    }
    public function notEnrolled($user_id, $required_request_id, $call_id){
        $user = User::firstWhere('id', $user_id);
        $call = Call::firstWhere('id', $call_id);
        $required_request = RequiredRequest::firstWhere('id', $required_request_id);
        $exist = DB::table('enroll_request')
        ->join('required_requests', function ($join)  use ($user_id, $call_id, $required_request_id) {
            $join->on('enroll_request.required_request_id', '=', 'required_requests.id')
                 ->where([
                     ['enroll_request.user_id', '=', $user_id],
                     ['required_requests.call_id', '=', $call_id],
                     ['required_requests.id', '=', $required_request_id],
                     ]);
        })->exists();
        if($user and $required_request and $call and ! $exist){
            return true;
        }
        return false;
    }
}
