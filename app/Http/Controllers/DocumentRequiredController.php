<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocumentRequiredFromRequest;
use App\Models\DocumentRequired as ModelDocumentRequired;


class DocumentRequiredController extends Controller
{
    public function create(){

        $title = 'Convocatoria';
        return view('calls/document_requireds/document_required_create',compact('title'));
    }

    public function store(DocumentRequiredFromRequest $request)
    {
        ModelDocumentRequired::create([
            'name'=>$request->name,
            'description'=>$request->description,
        ]);
        return redirect('/call');

    }
}
