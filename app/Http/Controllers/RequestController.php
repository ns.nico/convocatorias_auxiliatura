<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Auxiliary;
use App\Http\Requests\RequestFormRequest;
use App\Models\Request as ModelRequest;
use App\Models\AvailableRequest;


class RequestController extends Controller
{
    public function create(Request $request, $call_type)
    {
        $title = 'Convocatoria - Requerimiento';
        return view('calls.admin.components.requests.create_requests',compact('title','call_type'));
    }

    public function store(RequestFormRequest $request)
    {
        $new_auxiliary = Auxiliary::create([
            'name' => $request->name,
            'code' => $request->code
        ]);
        $new_request = ModelRequest::create([
            'quantity'=>$request->quantity,
            'academic_hours'=>$request->academic_hours,
            'auxiliary_id' => $new_auxiliary->id
        ]);
        $call_type =  $request->call_type;
        if($call_type){
            AvailableRequest::create([
                'request_id' => $new_request->id,
                'call_type_id' => $request->call_type
            ]);
        }
        return redirect('/call?type='.$request->call_type);
    }
}
