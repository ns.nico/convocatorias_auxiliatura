<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignedRoleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Se requiere el nombre del Usuario.',
            'email.required' => 'Se requiere que ingrese un correo eletronico.',
            'email.email' => 'debe ingresar un correo electronico valido',
            'email.unique' => 'El correo Electonico ya fue registrado',
            'password.required' => 'Debe ingresar una contrasenia',
            'password.confirmed' => 'Las contrasenias ingresadas no coinciden',
            'password.min' => 'Las contrasenias debe tener almenos 8 caracteres',
        ];
    }
}
