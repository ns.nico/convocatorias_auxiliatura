<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnrollRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'requests'=> 'required',
            'call_id'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'requests.required' => 'Se requiere que se postule a al menos un item'
        ];
    }
}
