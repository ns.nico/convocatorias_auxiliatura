<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReceptionBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count'=> 'min:0|max:50',
        ];
    }

    public function messages()
    {
        return [
            'count.min' => 'Indique una cantidad aceptable',
            'count.max' => 'Indique una cantidad aceptable, maximo 50',
        ];
    }
}
