<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcademicUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|unique:auxiliaries|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Se requiere el nombre de la unidad academica',
            'name.unique' => 'El nombre de la unidad academica ya existe',
            'name.regex' => 'No debe contener caracteres especiales, ni numericos',
        ];
    }
}

