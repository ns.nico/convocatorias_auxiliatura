<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^([,0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([,0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'description' => 'required',
            'academic_unit' => 'required',
            'management' => 'required',
            'year' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Se requiere el nombre de la convocatoria.',
            'name.regex' => 'El nombre no debe contener caracteres especiales',
            'description.required' => 'Se requiere la descripcion de la convocatoria.',
            'academic_unit.required' => 'Se requiere la unidad academica a la que pertenece.',
            'management.required' => 'Se requiere el periodo de la convocatoria.',
            'year.required' => 'Se requiere el año de la convocatoria',
        ];
    }
}
