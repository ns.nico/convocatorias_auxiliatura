<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users' => "required|array|min:1",
            'leader' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'users.required' => 'Debe designar al menos un usuario a la commision',
            'users.min' => 'Debe designar al menos un usuario a la commision',
            'leader.required' => 'Debe asignar un lider a la comision',
        ];
    }
}
