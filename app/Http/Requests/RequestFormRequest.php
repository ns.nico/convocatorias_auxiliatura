<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity'=> 'required',
            'academic_hours'=> 'required',
            'name'=> 'required|unique:auxiliaries|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'code' => 'required|unique:auxiliaries|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/'
        ];
    }

    public function messages()
    {
        return [
            'quantity.required' => 'Indique la cantidad de auxiliares para la materia',
            'academic_hours.required' => 'Indique la cantidad de horas/mes',
            'name.required' => 'Se requiere el nombre de la materia',
            'name.unique' => 'El nombre de la materia ya existe, puede editarlo',
            'name.regex' => 'No debe contener caracteres especiales, ni numericos',
            'code.required' => 'Se requiere un codigo que identifique la materia',
            'code.unique' => 'El codigo de la materia ya existe, puede editarlo',
            'code.regex' => 'El codigo no debe contener caracteres especiales, ni numericos',
        ];
    }
}
