<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallStatus extends Model
{
    //
    public $timestamps = false;
    protected $table = "call_status";
    protected $fillable = ['status'];

}
