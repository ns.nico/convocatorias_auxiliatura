<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Points extends Model
{
    public $timestamps = false;
    public $table = "points";
    protected $fillable = ['points', 'description'];
}
