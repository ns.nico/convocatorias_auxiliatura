<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\EnrollRequest;
use App\Models\Especification;

class Especification_grades extends Model
{
    public $timestamps = false;
    protected $table = 'specification_grades';
    public function enroll_request(){
        return EnrollRequest::findOrFail($this->enroll_request_id);
    }
    public function specification(){
        return Especification::findOrFail($this->specification_id);
    }
    protected $fillable = ['specification_id', 'enroll_request_id','grade'];
}
