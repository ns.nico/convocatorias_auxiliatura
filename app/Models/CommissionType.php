<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionType extends Model
{
    //
    public $timestamps = false;
    protected $table = "commission_types";
    protected $fillable = ['name'];
}
