<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    public $timestamps = false;
    public $table = "dates";
    protected $fillable = ['call_id', 'available_event_id', 'start', 'end',];

}
