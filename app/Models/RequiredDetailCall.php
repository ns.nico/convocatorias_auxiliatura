<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequiredDetailCall extends Model
{
    //
    public $timestamps = false;
    protected $table = "required_details_call";
    protected $fillable = ['detail_call_id', 'call_id'];
}
