<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentCall extends Model
{
    //
    public $timestamps = false;
    public $table = "appointments_call";
    protected $fillable = ['appointment_id', 'call_id'];
}
