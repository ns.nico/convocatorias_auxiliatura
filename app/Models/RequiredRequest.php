<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AvailableRequest;
use App\Models\Call;

class RequiredRequest extends Model
{
    //
    public $timestamps = false;
    protected $table = 'required_requests';
    protected $fillable = ['available_request_id', 'call_id'];

    public function available_request(){
        return AvailableRequest::findOrFail($this->available_request_id);
    }
    public function calls(){
        return Call::findOrFail($this->call_id);
    }
}
