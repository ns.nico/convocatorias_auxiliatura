<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointGrade extends Model
{
    //
    public $timestamps = false;
    protected $table = "point_grades";
    protected $fillable = ['point_id', 'enroll_request_id', 'grade'];
}
