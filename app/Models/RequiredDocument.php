<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AvailableDocument;

class RequiredDocument extends Model
{
    //
    public $timestamps = false;
    protected $table = 'required_documents';
    protected $fillable = ['available_document_id', 'call_id'];

    public function available_document(){
        return AvailableDocument::findOrFail($this->available_document_id);
    }
}
