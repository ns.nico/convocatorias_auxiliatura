<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\RequiredRequest;

class EnrollRequest extends Model
{
    public $timestamps = false;
    protected $table = 'enroll_request';
    protected $fillable = ['reviewed', 'enabled','user_id', 'required_request_id'];

    public function user(){
        return User::findOrFail($this->user_id);
    }

    public function required_request(){
        return RequiredRequest::findOrFail($this->required_request_id);
    }
}
