<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Request as ModelRequest;

class AvailableRequest extends Model
{
    //
    public $timestamps = false;
    protected $table = "available_requests";
    protected $fillable = ['request_id', 'call_type_id'];

    public function request(){
        return ModelRequest::findOrFail($this->request_id);
    }
}
