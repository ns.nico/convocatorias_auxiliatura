<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentReceived extends Model
{
    //
    public $timestamps = false;

    public $table = "documents_received";

    protected $fillable = ['number_documents','date_received','enroll_request_id'];
}
