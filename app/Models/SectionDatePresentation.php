<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionDatePresentation extends Model
{
    //
    public $timestamps = false;
    protected $table = "section_date_presentation";
    protected $fillable = ['date_presentation_id', 'call_id'];
}
