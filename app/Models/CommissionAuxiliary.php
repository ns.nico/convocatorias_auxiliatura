<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionAuxiliary extends Model
{
    //
    public $timestamps = false;
    protected $table = "commission_auxiliaries";
    protected $fillable = ['commission_request_id', 'auxiliary_id'];
}
