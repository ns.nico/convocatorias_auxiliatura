<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MeritRatingType extends Model
{
    //
    public $timestamps = false;

    public $table = "merit_rating_types";

    protected $fillable = ['name'];
}
