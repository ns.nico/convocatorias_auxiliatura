<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merit_rating extends Model
{
    public $timestamps = false;

    public $table = "merit_ratings";

    protected $fillable = ['name','description','percentage','merit_rating_type_id'];

}
