<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsibleCall extends Model
{
    //
    public $timestamps = false;
    protected $table = 'responsible_call';
    protected $fillable = ['call_id', 'responsible_id'];
}
