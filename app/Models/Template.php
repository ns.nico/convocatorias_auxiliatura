<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    //
    public $timestamps = false;
    protected $table = "templates";
    protected $fillable = ['name', 'desciption', 'call_type_id'];
}
