<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatePresentation extends Model
{
    //
    public $timestamps = false;
    protected $table = "date_presentation";
    protected $fillable = ['name', 'description'];
}
