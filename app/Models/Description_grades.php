<?php

namespace App\Models;
use App\Models\EnrollRequest;
use App\Models\AvailableDescription;

use Illuminate\Database\Eloquent\Model;

class Description_grades extends Model
{
    public $timestamps = false;
    protected $table = 'description_grades';
    public function enroll_request(){
        return EnrollRequest::findOrFail($this->enroll_request_id);
    }
    public function description_metirs(){
        return Description_metir::findOrFail($this->description_merit_id);
    }
    protected $fillable = ['description_merit_id', 'enroll_request_id','grade'];
}
