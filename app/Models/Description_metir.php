<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Description_metir extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'percentage'];
}
