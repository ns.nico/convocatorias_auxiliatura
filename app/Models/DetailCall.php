<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailCall extends Model
{
    //
    public $timestamps = false;
    protected $table = "details_call";
    protected $fillable = ['name', 'description'];
}
