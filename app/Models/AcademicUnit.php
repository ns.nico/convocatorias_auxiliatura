<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicUnit extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];
}
