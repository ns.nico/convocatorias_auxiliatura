<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Description_metir;
use App\Models\Especification;

class AvailableEspecifications extends Model
{
    public $timestamps = false;
    public function especification(){
        return Especification::findOrFail($this->especifications_id);
    }
    public function description(){
        return Description_metir::findOrFail($this->description_metirs_id);
    }

    protected $table = "available_especifications";
    protected $fillable = ['description_metirs_id', 'especifications_id','merits_id'];

}
