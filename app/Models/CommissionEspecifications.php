<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionEspecifications extends Model
{
    public $timestamps = false;
    protected $table = "commission_especifications";
    protected $fillable = ['commission_id', 'especification_id','call_id'];
}
