<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourtCall extends Model
{
    //
    public $timestamps = false;
    public $table = "courts_call";
    protected $fillable = ['call_id', 'court_id'];
}
