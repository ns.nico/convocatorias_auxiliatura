<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalData extends Model
{
    //
    public $timestamps = false;
    protected $table = "personal_data";
    protected $fillable = ['codeSis', 'ci', 'user_id'];
}
