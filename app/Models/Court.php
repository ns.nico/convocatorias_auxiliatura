<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
    //
    public $timestamps = false;
    public $table = "courts";
    protected $fillable = ['name', 'descrpition'];
}
