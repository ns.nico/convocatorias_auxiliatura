<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    public $timestamps = false;
    protected $table = "commissions";
    protected $fillable = ['name', 'commission_type_id'];
}
