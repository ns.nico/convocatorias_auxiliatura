<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequiredRequisite extends Model
{
    //
    public $timestamps = false;
    protected $table = 'required_requisites';
    protected $fillable = ['available_requisite_id', 'call_id'];
}
