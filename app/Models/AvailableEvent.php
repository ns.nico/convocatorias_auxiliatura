<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailableEvent extends Model
{
    //
    public $timestamps = false;
    protected $table = "available_events";
    protected $fillable = ['event_id', 'call_type_id'];
}
