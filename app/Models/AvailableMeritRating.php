<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailableMeritRating extends Model
{
    public $timestamps = false;

    public $table = "available_merit_ratings";
    public function merits(){
        return Merit_rating::findOrFail($this->merit_ratings_id);
    }

    protected $fillable = ['call_types_id','merit_ratings_id'];

}
