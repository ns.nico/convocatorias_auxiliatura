<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionUsers extends Model
{
    //
    public $timestamps = false;
    protected $table = "commission_users";
    protected $fillable = ['leader', 'user_id', 'commission_id'];
}
