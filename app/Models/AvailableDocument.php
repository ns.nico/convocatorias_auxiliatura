<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document;

class AvailableDocument extends Model
{
    //
    public $timestamps = false;
    protected $table = "available_documents";
    protected $fillable = ['document_id', 'call_type_id'];

    public function document(){
        return Document::findOrFail($this->document_id);
    }
}
