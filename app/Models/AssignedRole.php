<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignedRole extends Model
{
    public $timestamps = false;
    public $table = "assigned_roles";
    protected $fillable = ['user_id', 'rol_id'];
}
