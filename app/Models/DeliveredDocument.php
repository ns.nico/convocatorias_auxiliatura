<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveredDocument extends Model
{
    //
    public $timestamps = false;
    protected $table = "documents_delivered";
    protected $fillable = ['enroll_request_id', 'required_document_id'];
}
