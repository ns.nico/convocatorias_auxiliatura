<?php

namespace App\Models;
use App\Models\Auxiliary;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public $timestamps = false;

    public function auxiliary(){
        return Auxiliary::findOrFail($this->auxiliary_id);
    }

    protected $fillable = ['quantity', 'academic_hours', 'auxiliary_id'];
}
