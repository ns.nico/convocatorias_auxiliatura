<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AcademicUnit;

class Call extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['name', 'description', 'academic_unit_id', 'management', 'year','call_status_id'];

    public function academic_unit(){
        return AcademicUnit::findOrFail($this->academic_unit_id);
    }
}
