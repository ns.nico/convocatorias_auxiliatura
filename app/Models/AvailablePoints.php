<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Especification;
use App\Models\Points;


class AvailablePoints extends Model
{
    public $timestamps = false;
    public function especification(){
        return Especification::findOrFail($this->especifications_id);
    }
    public function points(){
        return Points::findOrFail($this->points_id);
    }
    protected $table = "available_points";
    protected $fillable = ['especification_id', 'points_id','merits_id'];

}
