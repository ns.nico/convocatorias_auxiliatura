<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    //
    public $timestamps = false;
    protected $table = 'responsible';
    protected $fillable = ['name', 'position'];
}
