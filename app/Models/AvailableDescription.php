<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Merit_rating;
use App\Models\Description_metir;

class AvailableDescription extends Model
{
    public $timestamps = false;
    protected $table = "available_descriptions";
    public function merit(){
        return Merit_rating::findOrFail($this->merit_ratings_id);
    }
    public function description(){
        return Description_metir::findOrFail($this->description_metirs_id);
    }
    protected $fillable = ['merit_ratings_id', 'description_metirs_id'];

}
