<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DescriptionGrade extends Model
{
    //
    public $timestamps = false;
    protected $table = "description_grades";
    protected $fillable = ['description_merit_id', 'enroll_request_id', 'grade'];
}
