<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequiredCallMerits extends Model
{
    public $timestamps = false;
    protected $table = "required_call_merit";
    protected $fillable = ['calls_id', 'available_merit_id'];
}
