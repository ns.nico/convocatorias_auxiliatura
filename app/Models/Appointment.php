<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    public $timestamps = false;
    public $table = "appointments";
    protected $fillable = ['name', 'descrpition'];
}
