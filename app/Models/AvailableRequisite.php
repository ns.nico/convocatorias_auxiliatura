<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailableRequisite extends Model
{
    //
    public $timestamps = false;
    protected $table = "available_requisites";
    protected $fillable = ['requisite_id', 'call_type_id'];
}
