<?php

use Illuminate\Database\Seeder;
use App\Models\RequiredDocument;

class RequiredDocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RequiredDocument::create([
            'available_document_id' => 1,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 2,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 3,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 4,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 5,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 6,
            'call_id' => 1
        ]);
        RequiredDocument::create([
            'available_document_id' => 7,
            'call_id' => 1
        ]);

        //call type 2
        RequiredDocument::create([
            'available_document_id' => 8,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 9,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 10,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 11,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 12,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 13,
            'call_id' => 2
        ]);
        RequiredDocument::create([
            'available_document_id' => 14,
            'call_id' => 2
        ]);
    }
}
