<?php

use Illuminate\Database\Seeder;
use App\Models\AppointmentCall;

class AppointmentCallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AppointmentCall::create([
            'appointment_id' => 1,
            'call_id' => 1
        ]);

        AppointmentCall::create([
            'appointment_id' => 2,
            'call_id' => 2
        ]);
    }
}
