<?php

use Illuminate\Database\Seeder;
use App\Models\CommissionType;

class CommissionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CommissionType::create([
            'name' => 'Comision de Meritos',
        ]);

        CommissionType::create([
            'name' => 'Comision de Conocimiento',
        ]);
    }
}
