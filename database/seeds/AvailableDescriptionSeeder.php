<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableDescription;


class AvailableDescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AvailableDescription::create([

            'merit_ratings_id' => 1,
            'description_metirs_id'=> 1,
        ]);

        AvailableDescription::create([

            'merit_ratings_id' => 1,
            'description_metirs_id'=> 2,
        ]);

        AvailableDescription::create([

            'merit_ratings_id' => 1,
            'description_metirs_id'=> 3,

        ]);
        AvailableDescription::create([

            'merit_ratings_id' => 1,
            'description_metirs_id'=> 4,
        ]);

            /* laboratorio */

        AvailableDescription::create([

            'merit_ratings_id' => 2,
            'description_metirs_id'=> 5,
        ]);/* 5 */

        AvailableDescription::create([

            'merit_ratings_id' => 2,
            'description_metirs_id'=> 6,
        ]);/* 6 */

        AvailableDescription::create([

            'merit_ratings_id' => 2,
            'description_metirs_id'=> 7,
        ]);/* 7 */
        AvailableDescription::create([

            'merit_ratings_id' => 2,
            'description_metirs_id'=> 8,
        ]);/* 8 */

        AvailableDescription::create([

            'merit_ratings_id' => 2,
            'description_metirs_id'=> 9,
        ]);/* 9 */

        /* table test grades */
        AvailableDescription::create([

            'merit_ratings_id' => 3,
            'description_metirs_id'=> 10,
        ]);/* 10 */

        AvailableDescription::create([

            'merit_ratings_id' => 3,
            'description_metirs_id'=> 11,
        ]);/* 11 */

        /* table laboratory test grades */
        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 12,
        ]);/* 12 */

        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 13,
        ]);/* 13 */
        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 14,
        ]);/* 14 */

        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 15,
        ]);/* 15 */
        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 16,
        ]);/* 16 */

        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 17,
        ]);/* 17 */
        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 18,
        ]);/* 18 */
        AvailableDescription::create([

            'merit_ratings_id' => 4,
            'description_metirs_id'=> 19,
        ]);/* 19 */

    }
}
