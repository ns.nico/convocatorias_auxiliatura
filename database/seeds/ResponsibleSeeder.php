<?php

use Illuminate\Database\Seeder;
use App\Models\Responsible;

class ResponsibleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Responsible::create([
            'name' => 'Lic. Henrry Villarroel Tapia',
            'position' => 'DIR. ING. SISTEMAS'
        ]);
        Responsible::create([
            'name' => 'Lic. Yony Montoya Burgos',
            'position' => 'DIR. ING. INFORMATICA'
        ]);
        Responsible::create([
            'name' => 'Ing. Jimmy Villarroel Novillo',
            'position' => 'JEFE DPTO INFORMATICA Y SISTEMAS'
        ]);
        Responsible::create([
            'name' => 'Ing. Alfredo Cosio Papadopoli',
            'position' => 'DECANO - FCyT'
        ]);
    }
}
