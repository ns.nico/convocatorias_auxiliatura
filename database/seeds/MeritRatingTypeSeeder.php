<?php

use Illuminate\Database\Seeder;
use App\Models\MeritRatingType;

class MeritRatingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        MeritRatingType::create([
            'name' => 'Descripción de Meritos - Docencia',
        ]);
        MeritRatingType::create([
            'name' => 'Descripción de Meritos - Laboratorio',
        ]);

        MeritRatingType::create([
            'name' => 'Calificación de Conocimientos',
        ]);

        MeritRatingType::create([
            'name' => 'Calificación de Conocimientos Laboratorio',
        ]);
    }
}
