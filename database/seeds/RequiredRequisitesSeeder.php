<?php

use Illuminate\Database\Seeder;
use App\Models\RequiredRequisite;

class RequiredRequisitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RequiredRequisite::create([
            'available_requisite_id' => 1,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 2,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 3,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 4,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 5,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 6,
            'call_id' => 1
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 7,
            'call_id' => 2
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 8,
            'call_id' => 2
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 9,
            'call_id' => 2
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 10,
            'call_id' => 2
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 11,
            'call_id' => 2
        ]);

        RequiredRequisite::create([
            'available_requisite_id' => 12,
            'call_id' => 2
        ]);
    }
}
