<?php

use Illuminate\Database\Seeder;
use App\Models\DeliveredDocument;

class DeliveredDocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>1,
            'required_document_id'=>7
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>2,
            'required_document_id'=>7
        ]);


        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>3,
            'required_document_id'=>7
        ]);


        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>4,
            'required_document_id'=>7
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>5,
            'required_document_id'=>7
        ]);


        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>1
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>2
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>3
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>4
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>5
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>6
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>6,
            'required_document_id'=>7
        ]);

        //

        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>8
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>9
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>10
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>11
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>12
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>13
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>7,
            'required_document_id'=>14
        ]);
        
        //

        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>8
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>9
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>10
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>11
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>12
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>13
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>8,
            'required_document_id'=>14
        ]);
        
        //

        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>8
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>9
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>10
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>11
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>12
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>13
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>9,
            'required_document_id'=>14
        ]);
        
        //

        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>8
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>9
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>10
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>11
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>12
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>13
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>10,
            'required_document_id'=>14
        ]);
        
        //

        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>8
        ]);

        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>9
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>10
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>11
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>12
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>13
        ]);
        DeliveredDocument::create([
            'enroll_request_id' =>11,
            'required_document_id'=>14
        ]);
        
    }
}
