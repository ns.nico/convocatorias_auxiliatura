<?php

use Illuminate\Database\Seeder;
use App\Models\DatePresentation;

class DatePresentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DatePresentation::create([
            'name' => 'Presentación de documentos',
            'description'=>'Presentación de la documentación en sobre Manila cerrado y rotulado con los siguientes datos: Nombre y Apellido, dirección, teléfono y e-mail; {{date}} en Secretaria del Departamento de Informática - Sistemas.',
        ]);
    }
}
