<?php

use Illuminate\Database\Seeder;
use App\Models\Description_metir;

class DescriptionMetirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Description_metir::create([
            'name' => 'REDIMIENTO ACADEMICO',
            'percentage' => 65,

        ]);

        Description_metir::create([
            'name' => 'Experiencia general',
            'percentage' => 0,

        ]);

        Description_metir::create([
            'name' => 'Documentos de experiencia universitaria',
            'percentage' => 25,


        ]);
        Description_metir::create([
            'name' => 'Documentos de experiencia extrauniversitaria',
            'percentage' => 10,

        ]);

            /* laboratorio */

        Description_metir::create([
            'name' => 'RENDIMIENTO ACADÉMICO',
            'percentage' => 65,

        ]);/* 5 */

        Description_metir::create([
            'name' => 'EXPERIENCIA GENERAL',
            'percentage' => 0,

        ]);/* 6 */

        Description_metir::create([
            'name' => 'Documentos de experiencia en laboratorios',
            'percentage' => 20,

        ]);/* 7 */
        Description_metir::create([
            'name' => 'Producción',
            'percentage' => 5,

        ]);/* 8 */

        Description_metir::create([
            'name' => 'Documentos de experiencia extrauniversitaria y de capacitación',
            'percentage' => 10,

        ]);/* 9 */

        /* knowledge test */
        Description_metir::create([
            'name' => 'Examen de Conocimientos',
            'percentage' => 40,
        ]);/* 10 */

        /* knowledge oral test */
        Description_metir::create([
            'name' => 'Examen Oral',
            'percentage' => 60,
        ]);/* 11 */

        /* laboratory test */
        Description_metir::create([
            'name' => 'LCO-ADM',
            'percentage' => 100,
        ]);/* 12 */
        Description_metir::create([
            'name' => 'LDS-ATL',
            'percentage' => 100,
        ]);/* 13 */
        Description_metir::create([
            'name' => 'LDS-ADM',
            'percentage' => 100,
        ]);/* 14 */

        Description_metir::create([
            'name' => 'LDS-AUX',
            'percentage' => 100,
        ]);/* 15 */
        Description_metir::create([
            'name' => 'LM-ADM-SW',
            'percentage' => 100,
        ]);/* 16 */
        Description_metir::create([
            'name' => 'LM-AUX-SW',
            'percentage' => 100,
        ]);/* 17 */
        Description_metir::create([
            'name' => 'LM-ADM-HW',
            'percentage' => 100,
        ]);/* 18 */
        Description_metir::create([
            'name' => 'LM-AUX-HW',
            'percentage' => 100,
        ]);/* 19 */

    }
}
