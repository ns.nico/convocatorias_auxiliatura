<?php

use Illuminate\Database\Seeder;
use App\Models\AcademicUnit;

class AcademicUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicUnit::create([
            'name' => 'Departamento Informática Sistemas',
        ]);

        AcademicUnit::create([
            'name' => 'Departamento Matemáticas'
        ]);

        AcademicUnit::create([
            'name' => 'Departamento Física'
        ]);

        AcademicUnit::create([
            'name' => 'Departamento Biología'
        ]);
    }
}
