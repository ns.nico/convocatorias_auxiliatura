<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableRequisite;

class AvailableRequisitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AvailableRequisite::create([
            'requisite_id' => 1,
            'call_type_id' => 1
        ]);
        AvailableRequisite::create([
            'requisite_id' => 2,
            'call_type_id' => 1
        ]);
        AvailableRequisite::create([
            'requisite_id' => 3,
            'call_type_id' => 1
        ]);
        AvailableRequisite::create([
            'requisite_id' => 4,
            'call_type_id' => 1
        ]);
        AvailableRequisite::create([
            'requisite_id' => 5,
            'call_type_id' => 1
        ]);

        AvailableRequisite::create([
            'requisite_id' => 6,
            'call_type_id' => 1
        ]);
        AvailableRequisite::create([
            'requisite_id' => 7,
            'call_type_id' => 2
        ]);
        AvailableRequisite::create([
            'requisite_id' => 2,
            'call_type_id' => 2
        ]);
        AvailableRequisite::create([
            'requisite_id' => 3,
            'call_type_id' => 2
        ]);
        AvailableRequisite::create([
            'requisite_id' => 4,
            'call_type_id' => 2
        ]);
        AvailableRequisite::create([
            'requisite_id' => 5,
            'call_type_id' => 2
        ]);

        AvailableRequisite::create([
            'requisite_id' => 6,
            'call_type_id' => 2
        ]);
    }
}
