<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "name" => "admin",
                "email" => "neosoft2020@gmail.com",
                "password" => bcrypt('neo2020'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Activent",
                "email" => "Activent@gmail.com",
                "password" => bcrypt('Activent'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Adducholo",
                "email" => "Adducholo@gmail.com",
                "password" => bcrypt('Adducholo'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "AnnBWith",
                "email" => "AnnBWith@gmail.com",
                "password" => bcrypt('AnnBWith'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "AquaticRavager",
                "email" => "AquaticRavager@gmail.com",
                "password" => bcrypt('AquaticRavager'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Avicive",
                "email" => "Avicive@gmail.com",
                "password" => bcrypt('Avicive'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "BadChamp",
                "email" => "BadChamp@gmail.com",
                "password" => bcrypt('BadChamp'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "BuffyLink",
                "email" => "BuffyLink@gmail.com",
                "password" => bcrypt('BuffyLink'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "BWithMonster",
                "email" => "BWithMonster@gmail.com",
                "password" => bcrypt('BWithMonster'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Campygm",
                "email" => "Campygm@gmail.com",
                "password" => bcrypt('Campygm'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Cataciati",
                "email" => "Cataciati@gmail.com",
                "password" => bcrypt('Cataciati'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Cerridiu",
                "email" => "Cerridiu@gmail.com",
                "password" => bcrypt('Cerridiu'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "ChampKing",
                "email" => "ChampKing@gmail.com",
                "password" => bcrypt('ChampKing'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Clairisan",
                "email" => "Clairisan@gmail.com",
                "password" => bcrypt('Clairisan'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Commeni",
                "email" => "Commeni@gmail.com",
                "password" => bcrypt('Commeni'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "ConspiracyDoll",
                "email" => "ConspiracyDoll@gmail.com",
                "password" => bcrypt('ConspiracyDoll'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "Costoys",
                "email" => "Costoys@gmail.com",
                "password" => bcrypt('Costoys'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "secretary",
                "email" => "secretary@gmail.com",
                "password" => bcrypt('secretary'),
            ],
        ]);

        DB::table('users')->insert([
            [
                "name" => "teacher",
                "email" => "teacher@gmail.com",
                "password" => bcrypt('teacher'),
            ],
        ]);

    }
}
