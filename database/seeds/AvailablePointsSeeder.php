<?php

use Illuminate\Database\Seeder;
use App\Models\AvailablePoints;

class AvailablePointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   AvailablePoints::create([
            'especification_id' => 4,
            'points_id'=> 1,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 4,
            'points_id'=> 2,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 4,
            'points_id'=> 3,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 5,
            'points_id'=> 4,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 5,
            'points_id'=> 5,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 6,
            'points_id'=> 6,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 6,
            'points_id'=> 7,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 7,
            'points_id'=> 8,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 8,
            'points_id'=> 9,
            'merits_id'=>1,
            ]);

        AvailablePoints::create([
            'especification_id' => 11,
            'points_id'=> 10,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 11,
            'points_id'=> 11,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 12,
            'points_id'=> 12,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 13,
            'points_id'=> 13,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 14,
            'points_id'=> 14,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 15,
            'points_id'=> 15,
            'merits_id'=>2,
            ]);
        AvailablePoints::create([
            'especification_id' => 16,
            'points_id'=> 16,
            'merits_id'=>2,
            ]);

        AvailablePoints::create([
            'especification_id' => 16,
            'points_id'=> 17,
            'merits_id'=>2,
            ]);
    }
}
