<?php

use Illuminate\Database\Seeder;
use App\Models\RequiredRequest;

class RequiredRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RequiredRequest::create([
            'available_request_id' => 1,
            'call_id' => 1
        ]);
        RequiredRequest::create([
            'available_request_id' => 2,
            'call_id' => 1
        ]);
        RequiredRequest::create([
            'available_request_id' => 3,
            'call_id' => 1
        ]);
        RequiredRequest::create([
            'available_request_id' => 4,
            'call_id' => 1
        ]);

        // another call type
        RequiredRequest::create([
            'available_request_id' => 6,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 7,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 8,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 9,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 10,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 11,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 12,
            'call_id' => 2
        ]);
        RequiredRequest::create([
            'available_request_id' => 13,
            'call_id' => 2
        ]);

    }
}
