<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableMeritRating;

class AvailableMeritRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* teaching */
        AvailableMeritRating::create([
            'call_types_id' => 1,
            'merit_ratings_id' => 1,
        ]);
        /* laboratory */
        AvailableMeritRating::create([
            'call_types_id' => 2,
            'merit_ratings_id' => 2,
        ]);
        /*  teaching test */
        AvailableMeritRating::create([
            'call_types_id' => 1,
            'merit_ratings_id' => 3,
        ]);
        /* laboratory test */
        AvailableMeritRating::create([
            'call_types_id' => 2,
            'merit_ratings_id' => 4,
        ]);
    }
}
