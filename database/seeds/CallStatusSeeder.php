<?php

use Illuminate\Database\Seeder;
use App\Models\CallStatus;

class CallStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CallStatus::create([
            'status' => 'private',
        ]);
        CallStatus::create([
            'status' => 'public',
        ]);
        CallStatus::create([
            'status' => 'expired',
        ]);
    }
}
