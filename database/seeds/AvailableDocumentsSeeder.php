<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableDocument;

class AvailableDocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AvailableDocument::create([
            'document_id' => 1,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 2,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 3,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 4,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 5,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 6,
            'call_type_id' => 1
        ]);
        AvailableDocument::create([
            'document_id' => 7,
            'call_type_id' => 1
        ]);

        AvailableDocument::create([
            'document_id' => 1,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 2,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 3,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 4,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 5,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 6,
            'call_type_id' => 2
        ]);
        AvailableDocument::create([
            'document_id' => 7,
            'call_type_id' => 2
        ]);

    }
}
