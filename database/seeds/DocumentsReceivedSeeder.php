<?php

use Illuminate\Database\Seeder;
use App\Models\DocumentReceived;

class DocumentsReceivedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 1
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 2
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 3
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 4
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 5
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 6
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 7
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 8
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 9
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 10
        ]);
        DocumentReceived::create([
            'number_documents' => 7,
            'enroll_request_id' => 11
        ]);
    }
}
