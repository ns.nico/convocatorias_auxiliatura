<?php

use Illuminate\Database\Seeder;
use App\Models\EnrollRequest;

class EnrollRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        EnrollRequest::create([
            'required_request_id' => 1,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 2
        ]);

        EnrollRequest::create([
            'required_request_id' => 2,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 2
        ]);

        EnrollRequest::create([
            'required_request_id' => 2,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 3
        ]);

        EnrollRequest::create([
            'required_request_id' => 3,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 4
        ]);

        EnrollRequest::create([
            'required_request_id' => 4,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 5
        ]);

        EnrollRequest::create([
            'required_request_id' => 4,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 6
        ]);
        //
        EnrollRequest::create([
            'required_request_id' => 5,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 7
        ]);

        EnrollRequest::create([
            'required_request_id' => 6,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 7
        ]);

        EnrollRequest::create([
            'required_request_id' => 7,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 8
        ]);

        EnrollRequest::create([
            'required_request_id' => 8,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 9
        ]);

        EnrollRequest::create([
            'required_request_id' => 9,
            'reviewed'=>true,
            'enabled' => true,
            'user_id' => 10
        ]);

        EnrollRequest::create([
            'required_request_id' => 10,
            'reviewed'=>false,
            'user_id' => 11
        ]);

        EnrollRequest::create([
            'required_request_id' => 11,
            'reviewed'=>false,
            'user_id' => 12
        ]);

        EnrollRequest::create([
            'required_request_id' => 12,
            'reviewed'=>false,
            'user_id' => 13
        ]);
    }
}
