<?php

use Illuminate\Database\Seeder;
use App\Models\PointGrade;

class PointGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>1
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>1
        ]);

        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>2
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>2
        ]);

        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>3
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>3
        ]);

        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>4
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>4
        ]);

        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>5
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>5
        ]);

        //
        PointGrade::create([
            'point_id' =>1,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>2,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>3,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>4,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>5,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>6,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>7,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>8,
            'enroll_request_id'=>6
        ]);
        PointGrade::create([
            'point_id' =>9,
            'enroll_request_id'=>6
        ]);

        //
        PointGrade::create([
            'point_id' =>10,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>11,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>12,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>13,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>14,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>15,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>16,
            'enroll_request_id'=>7
        ]);
        PointGrade::create([
            'point_id' =>17,
            'enroll_request_id'=>7
        ]);

        //
        PointGrade::create([
            'point_id' =>10,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>11,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>12,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>13,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>14,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>15,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>16,
            'enroll_request_id'=>8
        ]);
        PointGrade::create([
            'point_id' =>17,
            'enroll_request_id'=>8
        ]);

        //
        PointGrade::create([
            'point_id' =>10,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>11,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>12,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>13,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>14,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>15,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>16,
            'enroll_request_id'=>9
        ]);
        PointGrade::create([
            'point_id' =>17,
            'enroll_request_id'=>9
        ]);

        //
        PointGrade::create([
            'point_id' =>10,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>11,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>12,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>13,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>14,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>15,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>16,
            'enroll_request_id'=>10
        ]);
        PointGrade::create([
            'point_id' =>17,
            'enroll_request_id'=>10
        ]);
    }
}
