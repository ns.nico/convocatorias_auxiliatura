<?php

use Illuminate\Database\Seeder;
use App\Models\SpecificationGrade;

class SpecificationGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>1
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>1
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>2
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>2
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>3
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>3
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>4
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>4
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>5
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>5
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>1,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>2,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>4,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>5,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>6,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>7,
            'enroll_request_id'=>6
        ]);
        SpecificationGrade::create([
            'specification_id' =>8,
            'enroll_request_id'=>6
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>9,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>10,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>11,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>12,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>13,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>14,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>15,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>16,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>17,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>18,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>19,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>22,
            'enroll_request_id'=>7
        ]);
        SpecificationGrade::create([
            'specification_id' =>25,
            'enroll_request_id'=>7
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>9,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>10,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>11,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>12,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>13,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>14,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>15,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>16,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>20,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>21,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>25,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>26,
            'enroll_request_id'=>8
        ]);
        SpecificationGrade::create([
            'specification_id' =>27,
            'enroll_request_id'=>8
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>9,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>10,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>11,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>12,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>13,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>14,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>15,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>16,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>20,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>21,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>25,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>26,
            'enroll_request_id'=>9
        ]);
        SpecificationGrade::create([
            'specification_id' =>27,
            'enroll_request_id'=>9
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>9,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>10,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>11,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>12,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>13,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>14,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>15,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>16,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>23,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>24,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>25,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>26,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>28,
            'enroll_request_id'=>10
        ]);
        SpecificationGrade::create([
            'specification_id' =>29,
            'enroll_request_id'=>10
        ]);

        //
        SpecificationGrade::create([
            'specification_id' =>9,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>10,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>11,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>12,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>13,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>14,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>15,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>16,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>24,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>26,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>28,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>29,
            'enroll_request_id'=>11
        ]);
        SpecificationGrade::create([
            'specification_id' =>30,
            'enroll_request_id'=>11
        ]);
    }
}
