<?php

use Illuminate\Database\Seeder;
use App\Models\RequiredDetailCall;

class RequiredDetailsCallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RequiredDetailCall::create([
            'detail_call_id' => 1,
            'call_id' => 1
        ]);

        RequiredDetailCall::create([
            'detail_call_id' => 2,
            'call_id' => 1
        ]);

        RequiredDetailCall::create([
            'detail_call_id' => 3,
            'call_id' => 1
        ]);

        RequiredDetailCall::create([
            'detail_call_id' => 1,
            'call_id' => 2
        ]);

        RequiredDetailCall::create([
            'detail_call_id' => 2,
            'call_id' => 2
        ]);

        RequiredDetailCall::create([
            'detail_call_id' => 3,
            'call_id' => 2
        ]);
    }
}
