<?php

use Illuminate\Database\Seeder;
use App\Models\PersonalData;

class PersonalDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PersonalData::create([
            'codeSis' =>17625608,
            'ci' =>145206,
            'user_id'=>2
        ]);
        PersonalData::create([
            'codeSis' =>2831912,
            'ci' =>467456,
            'user_id'=>3
        ]);
        PersonalData::create([
            'codeSis' =>3945737,
            'ci' =>598044,
            'user_id'=>4
        ]);
        PersonalData::create([
            'codeSis' =>11564392,
            'ci' =>65754,
            'user_id'=>5
        ]);
        PersonalData::create([
            'codeSis' =>6206688,
            'ci' =>287000,
            'user_id'=>6
        ]);
        PersonalData::create([
            'codeSis' =>9528689,
            'ci' =>15835,
            'user_id'=>7
        ]);
        PersonalData::create([
            'codeSis' =>12984682,
            'ci' =>599846,
            'user_id'=>8
        ]);
        PersonalData::create([
            'codeSis' =>1132525,
            'ci' =>383252,
            'user_id'=>9
        ]);
        PersonalData::create([
            'codeSis' =>15082192,
            'ci' =>117989,
            'user_id'=>10
        ]);
        PersonalData::create([
            'codeSis' =>16480478,
            'ci' =>396186,
            'user_id'=>11
        ]);
        PersonalData::create([
            'codeSis' =>9555732,
            'ci' =>124889,
            'user_id'=>12
        ]);
        PersonalData::create([
            'codeSis' =>8856745,
            'ci' =>64331,
            'user_id'=>13
        ]);
    }
}
