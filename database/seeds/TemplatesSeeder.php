<?php

use Illuminate\Database\Seeder;
use App\Models\Template;

class TemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Template::create([
            'template_name' => 'Docencia',
            'name' => 'CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS DE DOCENCIA',
            'description' => 'El Departamento de {Nombre del departamento} junto a las Carreras de {Carreras que pertenecen'.
                ' al departamento} de la Facultad de {Nombre de la Facultad}, convoca al concurso de méritos y '.
                'examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los '.
                'requerimientos que se tienen programados para la gestión {Año de la gestión} ',
            'call_type_id' => 1
        ]);

        Template::create([
            'template_name' => 'Laboratorio',
            'name' => 'CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTARA AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO',
            'description' => 'El Departamento de {Nombre del departamento} junto a las Carreras de {Carreras que pertenecen'.
                ' al departamento} de la Facultad de {Nombre de la Facultad}, convoca al concurso de méritos y '.
                'examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los '.
                'requerimientos que se tienen programados para la gestión {Año de la gestión}',
            'call_type_id' => 2
        ]);
    }
}
