<?php

use Illuminate\Database\Seeder;
use App\Models\Document;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Document::create([
            'name' => 'Presentar solicitud escrita',
            'description' => 'Presentar Solicitud escrita para la(s) auxiliatura(s) a la(s) que se postula, dirigida a la Jefatura de Departamento.'
        ]);

        Document::create([
            'name' => 'Certificado de condición de estudiante',
            'description' => 'Presentar certificado de condición de estudiante expedido por el Departamento de Registros e Inscripciones.'
        ]);
        Document::create([
            'name' => 'Kardex actualizado a la gestión 1/2020',
            'description' => 'Kardex actualizado a la gestión 1/2020 (periodos cumplidos a la fecha), expedido por oficina de Kardex de la Facultad de Ciencias y Tecnología'
        ]);

        Document::create([
            'name' => 'Fotocopia Carnet de Identidad.',
            'description' => 'Fotocopia del carnet de identidad.'
        ]);

        Document::create([
            'name' => 'Certificado de no tener deudas de libros.',
            'description' => 'Certificado expedido por la biblioteca de la Facultad De Ciencias y Tecnología de no tener deudas de libros.'
        ]);

        Document::create([
            'name' => 'Resumen Curriculum Vitae',
            'description' => 'Presentar resumen de currículum Vitae de acuerdo al subtítulo 6.- DE LA CALIFICACIÓN DE MÉRITOS de esta convocatoria'
        ]);

        Document::create([
            'name' => 'Curriculum Vitae',
            'description' => 'Presentar documentación que respalde el currículum vitae, ORGANIZADO Y SEPARADO de acuerdo a la tabla de calificación de méritos'
        ]);

    }
}
