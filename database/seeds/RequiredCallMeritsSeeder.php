<?php

use Illuminate\Database\Seeder;
use App\Models\RequiredCallMerits;

class RequiredCallMeritsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RequiredCallMerits::create([
            'calls_id' => 1,
            'available_merit_id' => 1,
        ]);
        RequiredCallMerits::create([
            'calls_id' => 1,
            'available_merit_id' => 3,
        ]);
        RequiredCallMerits::create([
            'calls_id' => 2,
            'available_merit_id' => 2,
        ]);
        RequiredCallMerits::create([
            'calls_id' => 2,
            'available_merit_id' => 4,
        ]);
    }
}
