<?php

use Illuminate\Database\Seeder;
use App\Models\Especification;

class EspecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Especification::create([
            'name' => 'Promedio de aprobación de la materia a la que postula (incluye reprobadas y abandonos):',
            'percentage' => 35,
            ]);
        Especification::create([
            'name' => 'Promedio general de materias',
            'percentage' => 30,
            ]);
        Especification::create([
            'name' => 'La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final y se ponderará de la siguiente manera.',
            'percentage' => 0,
            ]);
        Especification::create([
            'name' => 'Auxiliar docente en materias del área troncal:',
            'percentage' => 15,
            ]);
        Especification::create([
            'name' => 'Auxiliar en otras ramas o carreras:',
            'percentage' => 5,
            ]);
        Especification::create([
            'name' => 'Disertación cursillos y/o participación en Proyectos:',
            'percentage' => 5,
            ]);
        Especification::create([
            'name' => 'Experiencia como operador, programador, analista de sistemas, cargo directivo en centro de cómputo',
            'percentage' => 5,
            ]);

        Especification::create([
            'name' => 'Experiencia docente en colegios, institutos, etc',
            'percentage' => 5,
            ]);

        /* laboratorio */
        Especification::create([
            'name' => 'Promedio general de las materias cursadas (Incluye reprobadas y abandonos)',
            'percentage' => 35,
        ]);/* 9  */

        Especification::create([
            'name' => 'Promedio general de materias en el periodo académico anterior',
            'percentage' => 30,
        ]);/* 10 */

        Especification::create([
            'name' => 'Auxiliar de Laboratorio Departamento de Informática - Sistemas del item respectivo',
            'percentage' => 12,
        ]);/* 11 */

        Especification::create([
            'name' => 'Auxiliares de Practicas Laboratorio Departamento de Informática - Sistemas',
            'percentage' => 6,
        ]);/* 12 */
        Especification::create([
            'name' => 'Otros auxiliares en laboratorios de computación',
            'percentage' => 2,
        ]);/* 13 */

        Especification::create([
            'name' => 'Disertación cursos y/o participación en Proyectos',
            'percentage' => 5,
        ]);/* 14 */

        Especification::create([
            'name' => 'Experiencia como operador, programador, analista de sistemas, cargo directivo en centro de cómputo.',
            'percentage' => 6,
        ]);/* 15 */

        Especification::create([
            'name' => 'Experiencia docente en colegios, institutos, etc',
            'percentage' => 4,
        ]);/* 16 */

        /* laboratory test */
        Especification::create([
            'name' => 'ADM LINUX',
            'percentage' => 25,
        ]);/* 17 */
        Especification::create([
            'name' => 'REDES NIVEL INTERMEDIO',
            'percentage' => 25,
        ]);/* 18 */
        Especification::create([
            'name' => 'POSTGRES, MYSQL NIVEL INTERMEDIO',
            'percentage' => 20,
        ]);/* 19 */
        Especification::create([
            'name' => 'PROGRAMACION PARA INTERNET, LENGUAJES DE PROGRAMACION (JSP, JAVASCRIPT, CSS, HTML, PHP, DELPHI)',
            'percentage' => 30,
        ]);/* 20 */
        Especification::create([
            'name' => 'MODELAJE DE APLICACIONES WEB (UML),PROCESO UNIFICADO ESTRUCTURADO',
            'percentage' => 20,
        ]);/* 21 */
        Especification::create([
            'name' => 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE',
            'percentage' => 20,
        ]);/* 22 */
        Especification::create([
            'name' => 'ELECTRÓNICA APLICADA Teórico',
            'percentage' => 20,
        ]);/* 23 */
        Especification::create([
            'name' => 'ELECTRÓNICA APLICADA Practico',
            'percentage' => 25,
        ]);/* 24 */
        Especification::create([
            'name' => 'DIDÁCTICA',
            'percentage' => 10,
        ]);/* 25 */
        Especification::create([
            'name' => 'ADM LINUX',
            'percentage' => 10,
        ]);/* 26 */
        Especification::create([
            'name' => 'POSTGRES, MYSQL NIVEL INTERMEDIO',
            'percentage' => 30,
        ]);/* 27 */
        Especification::create([
            'name' => 'REDES NIVEL INTERMEDIO',
            'percentage' => 10,
        ]);/* 28 */
        Especification::create([
            'name' => 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE',
            'percentage' => 25,
        ]);/* 29 */
        Especification::create([
            'name' => 'ELECTRÓNICA APLICADA Teórico',
            'percentage' => 30,
        ]);/* 30 */
        Especification::create([
            'name' => 'ELECTRÓNICA APLICADA Practico',
            'percentage' => 35,
        ]);/* 31 */
        Especification::create([
            'name' => 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE',
            'percentage' => 30,
        ]);/* 32 */
        Especification::create([
            'name' => 'ENSAMBLAJE Y MANTENIMIENTO DE COMPUTADORA EN HARDWARE Y SOFTWARE',
            'percentage' => 35,
        ]);/* 33 */
    }
}
