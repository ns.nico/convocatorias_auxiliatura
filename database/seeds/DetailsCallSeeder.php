<?php

use Illuminate\Database\Seeder;
use App\Models\DetailCall;

class DetailsCallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DetailCall::create([
            'name' => 'Nombre Apellido Dirección Telefono, e-mail',
            'description'=>'Nombre y apellidos completos, dirección, teléfono(s) y e-mail del postulante',
        ]);

        DetailCall::create([
            'name' => 'Código(s) de item',
            'description'=>'Código(s) de item de la(s) auxiliatura(s) a la(s) que se postula.',
        ]);
        DetailCall::create([
            'name' => 'Nombre(s) de la(s) auxiliatura(s)',
            'description'=>'Nombre(s) de la(s) auxiliatura(s) a la(s) que se presenta',
        ]);
    }
}
