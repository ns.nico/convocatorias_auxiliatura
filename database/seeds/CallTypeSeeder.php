<?php

use Illuminate\Database\Seeder;
use App\Models\CallType;

class CallTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CallType::create([
            'name' => 'Auxiliatura de Docencia',
        ]);

        CallType::create([
            'name' => 'Auxiliatura de Laboratorio',
        ]);

    }
}
