<?php

use Illuminate\Database\Seeder;
use App\Models\Auxiliary;

class AuxiliarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Auxiliary::create([
            'name' => 'Introducción a la Programación',
            'code' => 'IP-DOC'
        ]);

        Auxiliary::create([
            'name' => 'Elementos de Programación ',
            'code' => 'EP-DOC'
        ]);

        Auxiliary::create([
            'name' => 'Teoría de Grafos',
            'code' => 'TG-DOC'
        ]);

        Auxiliary::create([
            'name' => 'Computación I',
            'code' => 'COM I-DOC'
        ]);

        Auxiliary::create([
            'name' => 'Redes de Computadoras',
            'code' => 'RC-DOC'
        ]);

        Auxiliary::create([
            'name' => 'Administrador de Laboratorio de Cómputo',
            'code' => 'LCO-ADM'
        ]);
        Auxiliary::create([
            'name' => 'Administrador de Laboratorio de Desarrollo',
            'code' => 'LDS-ADM'
        ]);
        Auxiliary::create([
            'name' => 'Auxiliar de Laboratorio de Desarrollo',
            'code' => 'LDS-AUX'
        ]);
        Auxiliary::create([
            'name' => 'Administrador de Laboratorio de Mantenimiento de Software',
            'code' => 'LM-ADM-SW'
        ]);
        Auxiliary::create([
            'name' => 'Auxiliar de Laboratorio de Mantenimiento de Software',
            'code' => 'LM-AUX-SW'
        ]);
        Auxiliary::create([
            'name' => 'Administrador de Laboratorio de Mantenimiento de Hardware',
            'code' => 'LM-ADM-HW'
        ]);
        Auxiliary::create([
            'name' => 'Auxiliar de Laboratorio de Mantenimiento de Hardware',
            'code' => 'LM-AUX-HW'
        ]);
        Auxiliary::create([
            'name' => 'Auxiliar de Terminal de Laboratorio de Cómputo',
            'code' => 'LDS-ATL'
        ]);
    }
}
