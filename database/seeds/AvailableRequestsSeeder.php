<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableRequest;

class AvailableRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AvailableRequest::create([
            'request_id' => 1,
            'call_type_id' => 1
        ]);

        AvailableRequest::create([
            'request_id' => 2,
            'call_type_id' => 1
        ]);

        AvailableRequest::create([
            'request_id' => 3,
            'call_type_id' => 1
        ]);

        AvailableRequest::create([
            'request_id' => 4,
            'call_type_id' => 1
        ]);

        AvailableRequest::create([
            'request_id' => 5,
            'call_type_id' => 1
        ]);

        AvailableRequest::create([
            'request_id' => 6,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 7,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 8,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 9,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 10,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 11,
            'call_type_id' => 2
        ]);

        AvailableRequest::create([
            'request_id' => 12,
            'call_type_id' => 2
        ]);
        AvailableRequest::create([
            'request_id' => 13,
            'call_type_id' => 2
        ]);
    }
}
