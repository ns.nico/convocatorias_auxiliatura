<?php

use Illuminate\Database\Seeder;
use App\Models\AssignedRole;

class AssignedRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssignedRole::create([
            'user_id' => 1,
            'rol_id' => 1
        ]);
        AssignedRole::create([
            'user_id' => 2,
            'rol_id' => 2
        ]);
        AssignedRole::create([
            'user_id' => 3,
            'rol_id' => 3
        ]);
        AssignedRole::create([
            'user_id' => 4,
            'rol_id' => 3
        ]);
        AssignedRole::create([
            'user_id' => 5,
            'rol_id' => 3
        ]);
        AssignedRole::create([
            'user_id' => 18,
            'rol_id' => 2
        ]);
        AssignedRole::create([
            'user_id' => 19,
            'rol_id' => 3
        ]);
    }
}
