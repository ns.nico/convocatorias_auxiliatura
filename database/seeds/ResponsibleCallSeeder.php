<?php

use Illuminate\Database\Seeder;
use App\Models\ResponsibleCall;

class ResponsibleCallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ResponsibleCall::create([
            'call_id' => 1,
            'responsible_id' => 1
        ]);
        ResponsibleCall::create([
            'call_id' => 1,
            'responsible_id' => 2
        ]);
        ResponsibleCall::create([
            'call_id' => 1,
            'responsible_id' => 3
        ]);
        ResponsibleCall::create([
            'call_id' => 1,
            'responsible_id' => 4
        ]);

        ResponsibleCall::create([
            'call_id' => 2,
            'responsible_id' => 1
        ]);
        ResponsibleCall::create([
            'call_id' => 2,
            'responsible_id' => 2
        ]);
        ResponsibleCall::create([
            'call_id' => 2,
            'responsible_id' => 3
        ]);
        ResponsibleCall::create([
            'call_id' => 2,
            'responsible_id' => 4
        ]);
    }
}
