<?php

use Illuminate\Database\Seeder;
use App\Models\Merit_rating;
class MeritRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Merit_rating::create([
            'name' => 'Descripción de Meritos - Docencia',
            'description' => 'La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final y se ponderará de la siguiente manera.',
            'percentage' => 20,
            'merit_rating_type_id' => 1,
        ]);
        Merit_rating::create([
            'name' => 'Descripcion de Meritos - Laboratorio',
            'description' => 'La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final y se ponderará de la siguiente manera.',
            'percentage' => 20,
            'merit_rating_type_id' => 2,
        ]);

        Merit_rating::create([
            'name' => 'Calificación de Conocimientos',
            'description' => 'La calificación de conocimientos se realiza sobre la base de 100 puntos equivalentes al 80% de la calificación final',
            'percentage' => 80,
            'merit_rating_type_id' => 3,
        ]);

        Merit_rating::create([
            'name' => 'Calificación de Conocimientos Laboratorio',
            'description' => 'La calificación de conocimientos se realiza sobre la base de 100 puntos equivalentes al 80% de la calificación final',
            'percentage' => 80,
            'merit_rating_type_id' => 4,
        ]);
    }
}
