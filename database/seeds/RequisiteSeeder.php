<?php

use Illuminate\Database\Seeder;
use App\Models\Requisite;

class RequisiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Requisite::create([
            'name' => 'Ser estudiante regular',
            'description' => 'Ser estudiante regular y con rendimiento académico de las carreras de Licenciatura en Ingeniería Informática o Licenciatura en Ingeniería de Sistemas, que cursa regularmente en la Universidad. Para las materias de Introducción a la Programación y Elementos de Programación y Estructura de Datos, podrán presentarse además estudiantes de Ing. Electrónica. Para la materia de Computación I podrán presentarse además estudiantes de Ing. Industrial, Ing. Mecánica, Ing. Eléctrica, Ing. Electro-Mecánica e Ing. Matemática. Estudiante regular es aquel que está inscrito en la gestión académica vigente y cumple los requisitos exigidos para seguir una carrera universitaria y el rendimiento académico, haber aprobado más de la mitad de las materias curriculares quecorresponde al semestre anterior, certificado por el departamento de Registros e Inscripciones'
        ]);

        Requisite::create([
            'name' => 'Haber concluido el pensum con la totalidad de materias',
            'description' => 'O haber concluido el pensum con la totalidad de materias, teniendo pendiente tan solo la aprobación de la Modalidad de Graduación, pudiendo postular a la Auxiliatura Universitaria dentro del siguiente periodo académico (dos años o cuatro semestres), a partir de la fecha de conclusión de pensum de materias. Este periodo de dos años adicionales a los que contempla la conclusión del pensum de materias no podrá ampliarse bajo circunstancia alguna, aún en caso de encontrarse cursando otra carrera.'
        ]);

        Requisite::create([
            'name'=>'Participación sin Titulo Profesional',
            'description'=>'Queda expresamente prohibido la participación de estudiantes que hubiesen obtenido ya un título profesional en alguna de las carreras de la Universidad Mayor de San Simon o de cualquier otra del Sistema de la Universidad Boliviana (RCU No. 63/2018). Aún en caso de encontrarse cursando otra carrera con admisión especial. (Certificación emitida por el Departamento de Registros e Inscripciones).'
        ]);

        Requisite::create([
            'name'=>'Materias aprobadas hasta del semestre al que postula',
            'description'=>'Haber Aprobado la totalidad de las materias del semestre a la materia a la que se postula.'
        ]);

        Requisite::create([
            'name' => 'No tener deudas de libros',
            'description' => 'No tener deudas de libros en la biblioteca de la FCyT.'
        ]);
        
        Requisite::create([
            'name'=>'Participacion y aprobación del concurso',
            'description'=>'Participar y aprobar el Concurso de Méritos y proceso de pruebas de selección y admisión, conforme a convocatoria.'
        ]);

        Requisite::create([
            'name' => 'Estudiante Regular Laboratorio',
            'description' => 'Ser estudiante regular y con rendimiento de las carreras de Licenciatura en Ingeniería Informática o Licenciatura en Ingeniería de Sistemas y/o afín, que cursa regularmente en la universidad. Para administrador de Laboratorio de Mantenimiento de Hardware podrán presentarse además estudiantes de Ing. Electrónica. Estudiante regular es aquel que está inscrito en la gestión académica vigente y cumple los requisitos exigidos para seguir una carrera universitaria y el rendimiento académico, haber aprobado más de la mitad de las materias curriculares que corresponde al semestre anterior, certificado por el departamento de Registros e Inscripciones.'
        ]);
    }
}
