<?php

use Illuminate\Database\Seeder;
use App\Models\CommissionRequired;

class CommissionRequiredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CommissionRequired::create([
            'request_id' => 1,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 2,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 3,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 4,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 1,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 2,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 3,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 4,
            'commission_id' => 2
        ]);
        //

        CommissionRequired::create([
            'request_id' => 5,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 6,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 7,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 8,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 9,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 10,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 11,
            'commission_id' => 1
        ]);
        CommissionRequired::create([
            'request_id' => 12,
            'commission_id' => 1
        ]);

        CommissionRequired::create([
            'request_id' => 5,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 6,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 7,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 8,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 9,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 10,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 11,
            'commission_id' => 2
        ]);
        CommissionRequired::create([
            'request_id' => 12,
            'commission_id' => 2
        ]);
    }
}
