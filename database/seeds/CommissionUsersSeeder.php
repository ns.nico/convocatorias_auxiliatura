<?php

use Illuminate\Database\Seeder;
use App\Models\CommissionUsers;

class CommissionUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CommissionUsers::create([
            'leader' => true,
            'user_id' => 14,
            'commission_id' => 1
        ]);

        CommissionUsers::create([
            'user_id' => 15,
            'commission_id' => 1
        ]);
        CommissionUsers::create([
            'user_id' => 16,
            'commission_id' => 1
        ]);
        //
        CommissionUsers::create([
            'leader' => true,
            'user_id' => 17,
            'commission_id' => 2
        ]);

        CommissionUsers::create([
            'user_id' => 15,
            'commission_id' => 2
        ]);
        CommissionUsers::create([
            'user_id' => 14,
            'commission_id' => 2
        ]);

    }
}
