<?php

use Illuminate\Database\Seeder;
use App\Models\Request;

class RequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Request::create([
            'quantity' => 9,
            'academic_hours' => 8,
            'auxiliary_id' => 1
        ]);

        Request::create([
            'quantity' => 4,
            'academic_hours' => 8,
            'auxiliary_id' => 2
        ]);

        Request::create([
            'quantity' => 1,
            'academic_hours' => 8,
            'auxiliary_id' => 3
        ]);

        Request::create([
            'quantity' => 2,
            'academic_hours' => 8,
            'auxiliary_id' => 4
        ]);

        Request::create([
            'quantity' => 2,
            'academic_hours' => 8,
            'auxiliary_id' => 5
        ]);

        Request::create([
            'quantity' => 7,
            'academic_hours' => 80,
            'auxiliary_id' => 6
        ]);
        Request::create([
            'quantity' => 2,
            'academic_hours' => 80,
            'auxiliary_id' => 7
        ]);
        Request::create([
            'quantity' => 2,
            'academic_hours' => 56,
            'auxiliary_id' => 8
        ]);
        Request::create([
            'quantity' => 1,
            'academic_hours' => 80,
            'auxiliary_id' => 9
        ]);
        Request::create([
            'quantity' => 4,
            'academic_hours' => 32,
            'auxiliary_id' => 10
        ]);
        Request::create([
            'quantity' => 1,
            'academic_hours' => 80,
            'auxiliary_id' => 11
        ]);
        Request::create([
            'quantity' => 4,
            'academic_hours' => 32,
            'auxiliary_id' => 12
        ]);
        Request::create([
            'quantity' => 1,
            'academic_hours' => 80,
            'auxiliary_id' => 13
        ]);
    }
}
