<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableEspecifications;

class AvailableEspecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        AvailableEspecifications::create([
            'description_metirs_id' => 1,
            'especifications_id'=> 1,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 1,
            'especifications_id'=> 2,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 2,
            'especifications_id'=> 3,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 3,
            'especifications_id'=> 4,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 3,
            'especifications_id'=> 5,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 3,
            'especifications_id'=> 6,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 4,
            'especifications_id'=> 7,
            'merits_id' => 1,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 4,
            'especifications_id'=> 8,
            'merits_id' => 1,
            ]);

            /* LABORATORIO */
        AvailableEspecifications::create([
            'description_metirs_id' => 5,
            'especifications_id'=> 9,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 5,
            'especifications_id'=> 10,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 7,
            'especifications_id'=> 11,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 7,
            'especifications_id'=> 12,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 7,
            'especifications_id'=> 13,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 8,
            'especifications_id'=> 14,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 9,
            'especifications_id'=> 15,
            'merits_id' => 2,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 9,
            'especifications_id'=> 16,
            'merits_id' => 2,
            ]);
        
        /* LCOADM LDSATL laboratory test */
        AvailableEspecifications::create([
            'description_metirs_id' => 12,
            'especifications_id'=> 17,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 12,
            'especifications_id'=> 18,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 12,
            'especifications_id'=> 19,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 12,
            'especifications_id'=> 22,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 12,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);
        /* LDSATL laboratory test */
        AvailableEspecifications::create([
            'description_metirs_id' => 13,
            'especifications_id'=> 17,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 13,
            'especifications_id'=> 18,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 13,
            'especifications_id'=> 19,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 13,
            'especifications_id'=> 22,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 13,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);
        /* LDSADM laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 14,
            'especifications_id'=> 26,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 14,
            'especifications_id'=> 27,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 14,
            'especifications_id'=> 20,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 14,
            'especifications_id'=> 21,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 14,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);
        
        /* LDS-AUX laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 15,
            'especifications_id'=> 26,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 15,
            'especifications_id'=> 27,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 15,
            'especifications_id'=> 20,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 15,
            'especifications_id'=> 21,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 15,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);
        
        /* LM-ADM-SW laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 26,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 28,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 29,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 23,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 24,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 16,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);

        /* LM-AUX-SW laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 17,
            'especifications_id'=> 26,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 17,
            'especifications_id'=> 28,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 17,
            'especifications_id'=> 29,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 17,
            'especifications_id'=> 30,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 17,
            'especifications_id'=> 24,
            'merits_id' => 4,
            ]);
        
        /* LM-ADM-HW laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 18,
            'especifications_id'=> 28,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 18,
            'especifications_id'=> 32,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 18,
            'especifications_id'=> 29,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 18,
            'especifications_id'=> 23,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 18,
            'especifications_id'=> 25,
            'merits_id' => 4,
            ]);
        
        /* LM-AUX-HW laboratory test*/
        AvailableEspecifications::create([
            'description_metirs_id' => 19,
            'especifications_id'=> 33,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 19,
            'especifications_id'=> 30,
            'merits_id' => 4,
            ]);
        AvailableEspecifications::create([
            'description_metirs_id' => 19,
            'especifications_id'=> 31,
            'merits_id' => 4,
            ]);
    }
}
