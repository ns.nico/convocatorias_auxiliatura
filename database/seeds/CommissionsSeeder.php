<?php

use Illuminate\Database\Seeder;
use App\Models\Commission;

class CommissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Commission::create([
            'name' => 'Comision de Meritos',
            'commission_type_id' => 1
        ]);

        Commission::create([
            'name' => 'Comision de Conocimiento',
            'commission_type_id' => 2
        ]);
    }
}
