<?php

use Illuminate\Database\Seeder;
use App\Models\AvailableEvent;

class AvailableEventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AvailableEvent::create([
            'event_id' => 1,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 2,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 3,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 4,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 5,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 6,
            'call_type_id' => 1
        ]);

        AvailableEvent::create([
            'event_id' => 1,
            'call_type_id' => 2
        ]);

        AvailableEvent::create([
            'event_id' => 2,
            'call_type_id' => 2
        ]);

        AvailableEvent::create([
            'event_id' => 3,
            'call_type_id' => 2
        ]);

        AvailableEvent::create([
            'event_id' => 4,
            'call_type_id' => 2
        ]);

        AvailableEvent::create([
            'event_id' => 5,
            'call_type_id' => 2
        ]);

        AvailableEvent::create([
            'event_id' => 6,
            'call_type_id' => 2
        ]);
    }
}
