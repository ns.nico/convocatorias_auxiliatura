<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Date;

class DatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Date::create([
            'call_id' => 1,
            'available_event_id' => 1,
            'start' => Carbon::create('2020', '01', '16'),
            'end' => Carbon::create('2020', '02', '19'),
        ]);
        Date::create([
            'call_id' => 1,
            'available_event_id' => 2,
            'start' => Carbon::create('2020', '02', '07'),
            'end' => Carbon::create('2020', '02', '07'),
        ]);
        Date::create([
            'call_id' => 1,
            'available_event_id' => 3,
            'start' => Carbon::create('2020', '02', '07'),
            'end' => Carbon::create('2020', '02', '07'),
        ]);
        Date::create([
            'call_id' => 1,
            'available_event_id' => 4,
            'start' => Carbon::create('2020', '02', '13'),
            'end' => Carbon::create('2020', '02', '13'),
        ]);
        Date::create([
            'call_id' => 1,
            'available_event_id' => 5,
            'start' => Carbon::create('2020', '02', '10'),
            'end' => Carbon::create('2020', '02', '10'),
        ]);
        Date::create([
            'call_id' => 1,
            'available_event_id' => 6,
            'start' => Carbon::create('2020', '02', '19'),
            'end' => Carbon::create('2020', '02', '19'),
        ]);

        // call type 2
        Date::create([
            'call_id' => 2,
            'available_event_id' => 1,
            'start' => Carbon::create('2020', '01', '16'),
            'end' => Carbon::create('2020', '02', '19'),
        ]);
        Date::create([
            'call_id' => 2,
            'available_event_id' => 2,
            'start' => Carbon::create('2020', '02', '07'),
            'end' => Carbon::create('2020', '02', '07'),
        ]);
        Date::create([
            'call_id' => 2,
            'available_event_id' => 3,
            'start' => Carbon::create('2020', '02', '07'),
            'end' => Carbon::create('2020', '02', '07'),
        ]);
        Date::create([
            'call_id' => 2,
            'available_event_id' => 4,
            'start' => Carbon::create('2020', '02', '13'),
            'end' => Carbon::create('2020', '02', '13'),
        ]);
        Date::create([
            'call_id' => 2,
            'available_event_id' => 5,
            'start' => Carbon::create('2020', '02', '07'),
            'end' => Carbon::create('2020', '02', '19'),
        ]);
        Date::create([
            'call_id' => 2,
            'available_event_id' => 6,
            'start' => Carbon::create('2020', '02', '19'),
            'end' => Carbon::create('2020', '02', '19'),
        ]);
    }
}
