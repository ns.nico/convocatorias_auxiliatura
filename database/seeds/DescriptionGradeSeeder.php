<?php

use Illuminate\Database\Seeder;
use App\Models\DescriptionGrade;

class DescriptionGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>1
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>1
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>1
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>1
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>1
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>2
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>2
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>2
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>2
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>2
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>3
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>3
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>3
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>3
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>3
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>4
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>4
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>4
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>4
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>4
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>5
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>5
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>5
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>5
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>5
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>1,
            'enroll_request_id'=>6
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>3,
            'enroll_request_id'=>6
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>4,
            'enroll_request_id'=>6
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>10,
            'enroll_request_id'=>6
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>11,
            'enroll_request_id'=>6
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>5,
            'enroll_request_id'=>7
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>6,
            'enroll_request_id'=>7
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>7,
            'enroll_request_id'=>7
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>8,
            'enroll_request_id'=>7
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>9,
            'enroll_request_id'=>7
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>12,
            'enroll_request_id'=>7
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>5,
            'enroll_request_id'=>8
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>6,
            'enroll_request_id'=>8
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>7,
            'enroll_request_id'=>8
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>8,
            'enroll_request_id'=>8
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>9,
            'enroll_request_id'=>8
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>14,
            'enroll_request_id'=>8
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>5,
            'enroll_request_id'=>9
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>6,
            'enroll_request_id'=>9
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>7,
            'enroll_request_id'=>9
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>8,
            'enroll_request_id'=>9
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>9,
            'enroll_request_id'=>9
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>15,
            'enroll_request_id'=>9
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>5,
            'enroll_request_id'=>10
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>6,
            'enroll_request_id'=>10
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>7,
            'enroll_request_id'=>10
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>8,
            'enroll_request_id'=>10
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>9,
            'enroll_request_id'=>10
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>16,
            'enroll_request_id'=>10
        ]);

        //
        DescriptionGrade::create([
            'description_merit_id' =>5,
            'enroll_request_id'=>11
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>6,
            'enroll_request_id'=>11
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>7,
            'enroll_request_id'=>11
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>8,
            'enroll_request_id'=>11
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>9,
            'enroll_request_id'=>11
        ]);
        DescriptionGrade::create([
            'description_merit_id' =>17,
            'enroll_request_id'=>11
        ]);

    }
}
