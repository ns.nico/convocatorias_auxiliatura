<?php

use Illuminate\Database\Seeder;
use App\Models\CommissionAuxiliary;

class CommissionAuxiliarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 6
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 7
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 8
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 9
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 10
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 11
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 12
        ]);
        CommissionAuxiliary::create([
            'commission_id' => 2,
            'auxiliary_id' => 13
        ]);
    }
}
