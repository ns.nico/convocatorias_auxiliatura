<?php

use Illuminate\Database\Seeder;
use App\Models\Roles;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'rol' => 'Administrador',
        ]);
        Roles::create([
            'rol' => 'Secretario',
        ]);
        Roles::create([
            'rol' => 'Docente',
        ]);
        Roles::create([
            'rol' => 'Estudiante',
        ]);

    }
}
