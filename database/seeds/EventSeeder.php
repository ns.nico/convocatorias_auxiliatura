<?php

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::create([
            'name' => 'Publicación de convocatoria',
            'description' => 'Publicación de la convocatoria'
        ]);
        Event::create([
            'name' => 'Presentación de documentos',
            'description' => 'Entrega de los documentos requeridos.'
        ]);
        Event::create([
            'name' => 'Publicación de habilitados',
            'description' => 'Publicación de la tabla de habilitados'
        ]);
        Event::create([
            'name' => 'Reclamos',
            'description' => 'Recepcion de reclamos'
        ]);
        Event::create([
            'name' => 'Rol de pruebas',
            'description' => 'Pruebas'
        ]);
        Event::create([
            'name' => 'Publicación de Resultados',
            'description' => 'Publicación de la tabla de resultados'
        ]);
    }
}
