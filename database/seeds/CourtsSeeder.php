<?php

use Illuminate\Database\Seeder;
use App\Models\Court;

class CourtsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Court::create([
            'name' => 'Tribunales',
            'description' => 'Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante y para la comisión de conocimientos 1 docente por asignatura convocada más un estudiante veedor.',
        ]);
        Court::create([
            'name' => 'Tribunales',
            'description' => 'Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante, de la misma manera para la comisión de conocimientos cada consejo designará 1 docente y un estudiante veedor por cada temática',
        ]);
    }
}
