<?php

use Illuminate\Database\Seeder;
use App\Models\SectionDatePresentation;

class SectionDatePresentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SectionDatePresentation::create([
            'date_presentation_id' => 1,
            'call_id' => 1
        ]);

        SectionDatePresentation::create([
            'date_presentation_id' => 1,
            'call_id' => 2
        ]);
    }
}
