<?php

use Illuminate\Database\Seeder;
use App\Models\CourtCall;

class CourtsCallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CourtCall::create([
            'court_id' => 1,
            'call_id' => 1
        ]);
        CourtCall::create([
            'court_id' => 2,
            'call_id' => 2
        ]);
    }
}
