<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->calls([
            AcademicUnitSeeder::class,
            DocumentSeeder::class,
            EventSeeder::class,
            RequisiteSeeder::class,
            CallTypeSeeder::class,
            CallStatusSeeder::class,
            AuxiliarySeeder::class,
            RequestSeeder::class,
            UserSeeder::class,
            MeritRatingTypeSeeder::class,
            MeritRatingSeeder::class,
            DescriptionMetirSeeder::class,
            AvailableDescriptionSeeder::class,
            EspecificationSeeder::class,
            AvailableEspecificationSeeder::class,
            PointsSeeder::class,
            AvailablePointsSeeder::class,
            AvailableDocumentsSeeder::class,
            AvailableRequisitesSeeder::class,
            AvailableEventsSeeder::class,
            AvailableRequestsSeeder::class,
            CallsSeeder::class,
            RequiredRequisitesSeeder::class,
            RequiredDocumentsSeeder::class,
            RequiredRequestsSeeder::class,
            DatesSeeder::class,
            TemplatesSeeder::class,
            AvailableMeritRatingSeeder::class,
            RequiredCallMeritsSeeder::class,
            DetailsCallSeeder::class,
            RequiredDetailsCallSeeder::class,
            DatePresentationSeeder::class,
            SectionDatePresentationSeeder::class,
            CourtsSeeder::class,
            CourtsCallSeeder::class,
            AppointmentSeeder::class,
            AppointmentCallSeeder::class,
            EnrollRequestSeeder::class,
            DeliveredDocumentsSeeder::class,
            RolesSeeder::class,
            AssignedRoleSeeder::class,
            CommissionTypeSeeder::class,
            CommissionsSeeder::class,
            CommissionRequiredSeeder::class,
            CommissionUsersSeeder::class,
            DescriptionGradeSeeder::class,
            SpecificationGradeSeeder::class,
            PointsSeeder::class,
            CommissionAuxiliarySeeder::class,
            DocumentsReceivedSeeder::class,
            ResponsibleSeeder::class,
            ResponsibleCallSeeder::class,
            PersonalDataSeeder::class,
        ]);
    }

    protected function calls(array $tables)
    {
        foreach($tables as $table){
            $this->call($table);
        }
    }
}
