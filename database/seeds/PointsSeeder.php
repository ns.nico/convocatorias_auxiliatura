<?php

use Illuminate\Database\Seeder;
use App\Models\Points;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Points::create([
            'points' => 2,
            'description' => 'semestre y materia de aux. titular',
        ]);

                    Points::create([
            'points' => 1,
            'description' => 'semestre y materia de aux. invitado',
                    ]);

        Points::create([
            'points' => 1,
            'description' => 'semestre y materia de aux. de practicas',
                    ]);

        Points::create([
            'points' => 1,
            'description' => 'semestre x materia de aux. invitado o titular',
                    ]);
        Points::create([
            'points' => 1,
            'description' => 'semestre x materia de aux. de practicas',
                    ]);
        Points::create([
            'points' => 3,
            'description' => 'por dirección de cursillo',
                    ]);
        Points::create([
            'points' => 2,
            'description' => 'por participación en proyectos',
                    ]);
        Points::create([
            'points' => 1,
            'description' => 'cargo/semestre',
                    ]);
        Points::create([
            'points' => 1,
            'description' => 'cargo/semestre y certificado',
                    ]);

        /* points laboratorio */
        Points::create([
            'points' => 2,
            'description' => 'semestre Auxiliar titular.',

        ]);
        Points::create([
            'points'=> 1,
            'description' => 'semestre Auxiliar Invitado.',

        ]);
        Points::create([
            'points' => 1,
            'description' => 'semestre Auxiliar.',

        ]);
        Points::create([
            'points' => 1,
            'description' => 'semestre Auxiliar.',

        ]);
        Points::create([
            'points' => 2,
            'description' => 'certificado',

        ]);
        Points::create([
            'points' => 2,
            'description' => 'por certificado',

        ]);
        Points::create([
            'points' => 2,
            'description' => 'certificado aprobación',

        ]);
        Points::create([
            'points' => 1,
            'description' => 'certificado asistencia',

        ]);


    }
}
