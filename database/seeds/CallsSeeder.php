<?php

use Illuminate\Database\Seeder;
use App\Models\Call;

class CallsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Call::create([
            'name' => 'CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS DE DOCENCIA',
            'description' => 'El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. De Sistemas de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.',
            'academic_unit_id' => 1,
            'management' => 1,
            'year' => 2020,
            'call_status_id' => 2
        ]);
        Call::create([
            'name' => 'CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO',
            'description' => 'El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. de Sistemas, de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.',
            'academic_unit_id' => 2,
            'management' => 2,
            'year' => 2020,
            'call_status_id' => 2
        ]);
    }
}
