<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequiredCallMeritsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('required_call_merit', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('calls_id');
            $table->foreign('calls_id')->references('id')->on('calls');

            $table->unsignedInteger('available_merit_id');
            $table->foreign('available_merit_id')->references('id')->on('available_merit_ratings');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* Schema::dropIfExists('available_call_merits'); */
        Schema::dropIfExists('required_call_merit');
    }
}
