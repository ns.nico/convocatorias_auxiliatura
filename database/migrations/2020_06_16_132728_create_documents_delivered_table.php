<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsDeliveredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_delivered', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date_received')->default(date("Y-m-d H:i:s"));

            $table->unsignedInteger('enroll_request_id');
            $table->foreign('enroll_request_id')->references('id')->on('enroll_request')->onDelete('cascade');

            $table->unsignedInteger('required_document_id');
            $table->foreign('required_document_id')->references('id')->on('required_documents')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_delivered');
    }
}
