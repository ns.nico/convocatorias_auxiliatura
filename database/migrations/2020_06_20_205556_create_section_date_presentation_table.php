<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionDatePresentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_date_presentation', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('date_presentation_id');
            $table->foreign('date_presentation_id')->references('id')->on('date_presentation')->onDelete('cascade');

            $table->unsignedInteger('call_id');
            $table->foreign('call_id')->references('id')->on('calls')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_date_presentation');
    }
}
