<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_grades', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('point_id');
            $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');

            $table->unsignedInteger('enroll_request_id');
            $table->foreign('enroll_request_id')->references('id')->on('enroll_request')->onDelete('cascade');

            $table->unsignedInteger('grade')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_grades');
    }
}
