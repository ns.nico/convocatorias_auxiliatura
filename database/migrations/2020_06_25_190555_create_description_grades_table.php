<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescriptionGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('description_grades', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('description_merit_id');
            $table->foreign('description_merit_id')->references('id')->on('description_metirs')->onDelete('cascade');

            $table->unsignedInteger('enroll_request_id');
            $table->foreign('enroll_request_id')->references('id')->on('enroll_request')->onDelete('cascade');

            $table->unsignedInteger('grade')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('description_grades');
    }
}
