<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('call_id');
            $table->foreign('call_id')->references('id')->on('calls');

            $table->unsignedInteger('available_event_id');
            $table->foreign('available_event_id')->references('id')->on('available_events');

            $table->dateTime('start')->nullable(false);
            $table->dateTime('end')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dates');
    }
}
