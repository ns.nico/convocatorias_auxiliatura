<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableMeritRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_merit_ratings', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('call_types_id');
            $table->foreign('call_types_id')->references('id')->on('call_types');

            $table->unsignedInteger('merit_ratings_id');
            $table->foreign('merit_ratings_id')->references('id')->on('merit_ratings');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_merit_ratings');
    }
}
