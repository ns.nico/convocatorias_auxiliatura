<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtsCallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courts_call', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('court_id');
            $table->foreign('court_id')->references('id')->on('courts')->onDelete('cascade');

            $table->unsignedInteger('call_id');
            $table->foreign('call_id')->references('id')->on('calls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courts_call');
    }
}
