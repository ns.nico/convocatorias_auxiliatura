<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsibleCallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsible_call', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('call_id');
            $table->foreign('call_id')->references('id')->on('calls')->onDelete('cascade');

            
            $table->unsignedInteger('responsible_id');
            $table->foreign('responsible_id')->references('id')->on('responsible')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responsible_call');
    }
}
