<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->id();
            $table->text('name')->nullable(false);
            $table->text('description');

            $table->unsignedInteger('academic_unit_id');
            $table->foreign('academic_unit_id')->references('id')->on('academic_units')->onDelete('cascade');

            $table->unsignedInteger('management');
            $table->unsignedInteger('year');

            $table->unsignedInteger('call_status_id')->default(1);
            $table->foreign('call_status_id')->references('id')->on('call_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
