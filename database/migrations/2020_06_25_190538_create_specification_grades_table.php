<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecificationGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specification_grades', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('specification_id');
            $table->foreign('specification_id')->references('id')->on('especifications')->onDelete('cascade');

            $table->unsignedInteger('enroll_request_id');
            $table->foreign('enroll_request_id')->references('id')->on('enroll_request')->onDelete('cascade');

            $table->unsignedInteger('grade')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specification_grades');
    }
}
