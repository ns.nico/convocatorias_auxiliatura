<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableRequisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_requisites', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('requisite_id');
            $table->foreign('requisite_id')->references('id')->on('requisites')->onDelete('cascade');

            $table->unsignedInteger('call_type_id');
            $table->foreign('call_type_id')->references('id')->on('call_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_requisites');
    }
}
