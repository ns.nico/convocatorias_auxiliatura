<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_descriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('merit_ratings_id');
            $table->foreign('merit_ratings_id')->references('id')->on('merit_ratings');

            $table->unsignedInteger('description_metirs_id');
            $table->foreign('description_metirs_id')->references('id')->on('description_metirs');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_descriptions');
    }
}
