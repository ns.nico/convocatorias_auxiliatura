<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailablePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_points', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('especification_id');
            $table->foreign('especification_id')->references('id')->on('especifications');
            $table->unsignedInteger('points_id');
            $table->foreign('points_id')->references('id')->on('points');
            $table->unsignedInteger('merits_id')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_points');
    }
}
