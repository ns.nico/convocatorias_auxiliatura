<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableEspecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_especifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('description_metirs_id');
            $table->foreign('description_metirs_id')->references('id')->on('description_metirs');
            $table->unsignedInteger('especifications_id');
            $table->foreign('especifications_id')->references('id')->on('especifications');
            $table->unsignedInteger('merits_id')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_especifications');
    }
}
