<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionAuxiliariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_auxiliaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('commission_id');
            $table->foreign('commission_id')->references('id')->on('commissions')->onDelete('cascade');

            $table->unsignedInteger('auxiliary_id');
            $table->foreign('auxiliary_id')->references('id')->on('auxiliaries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_auxiliaries');
    }
}
