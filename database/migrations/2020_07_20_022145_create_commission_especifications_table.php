<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionEspecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_especifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('commission_id');
            $table->foreign('commission_id')->references('id')->on('commissions')->onDelete('cascade');

            $table->unsignedInteger('especification_id');
            $table->foreign('especification_id')->references('id')->on('especifications')->onDelete('cascade');

            $table->unsignedInteger('call_id');
            $table->foreign('call_id')->references('id')->on('calls')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_especifications');
    }
}
