<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_requests', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('request_id');
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');

            $table->unsignedInteger('call_type_id');
            $table->foreign('call_type_id')->references('id')->on('call_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_requests');
    }
}
