<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware_admin' => 'admin','middleware_secretary' => 'secretary','middleware_teacher'=>'teacher'], function () {
    Route::get('/home-admin', 'CallController@index');
    Route::get('/call', 'CallController@create')->name('call');
    Route::post('/call/{call_type}', 'CallController@store')->name('call_store');
    Route::get('/call-edit/{call_id}','CallController@edit')->name('call_edit');
    Route::post('/call-edit/{call_id}','CallController@update')->name('call_update');

    Route::get('/academic-unit/{call_type}', 'AcademicUnitController@create')->name('academic_unit');
    Route::post('/academic-unit', 'AcademicUnitController@store')->name('academic_unit_store');

    Route::get('/request/{call_type}', 'RequestController@create')->name('request');
    Route::post('/request', 'RequestController@store')->name('request_store');

    Route::get('/requisite/{call_type}', 'RequisiteController@create')->name('requisite');
    Route::post('/requisite', 'RequisiteController@store')->name('requisite_store');

    Route::get('/document-required/{call_type}', 'DocumentController@create')->name('document');
    Route::post('/document-required', 'DocumentController@store')->name('document_store');

    Route::get('/merits_rating/{call_type}/{merit_id}', 'AviableMeritRatingController@create')->name('merits');
    Route::post('/merits_rating', 'AviableMeritRatingController@store')->name('merit_store');

    Route::get('/enroll', 'EnrollRequestController@index')->name('enroll');
    Route::get('/enroll/{call_id}', 'EnrollRequestController@enroll')->name('enroll_request');
    Route::post('/enroll', 'EnrollRequestController@store')->name('enroll_store');

    Route::get('/roll', 'AssignedRoleController@index')->name('roll');
    Route::post('/roll', 'AssignedRoleController@store')->name('roll_store');

    /* vistas para usuario Secretaria */
    Route::get('/home-secretary','CheckRequisiteController@index');
    Route::get('/home-secretary','CheckRequisiteController@index')->name('home-secretary');
    Route::get('/call-options/{call_id}','CheckRequisiteController@checkRequisite')->name('call_options');
    Route::get('/register-documents/{call_id}/{enroll_id}','CheckRequisiteController@register')->name('register_document');
    Route::post('/register-documents/{call_id}/{enroll_id}','CheckRequisiteController@store')->name('document_store_secretary');
    Route::get('/reception_book/{call_id}','ReceptionBookController@index')->name('reception_book');
    Route::post('/reception_book','ReceptionBookController@store')->name('reception_book_store');

    /**Vistas para el usuario docente */
    Route::get('/home-teacher','TeacherHome@index')->name('home-teacher');

    /* vistas para comission */
    Route::get('/commission/{call_id}','CommissionController@create')->name('commission');
    Route::post('/commission/{call_id}','CommissionController@store')->name('commission_store');
    Route::get('/knowledge-rating/{requirementRequest}/{item_id}/{id_user}','KnowledgeRatingController@index')->name('docencia_qualification');
    Route::post('/knowledge-rating','KnowledgeRatingController@store')->name('doc_qualification_store');
    Route::get('/item-rating/{item_id}/{id_user}','ItemRatingController@index')->name('item_rating');
    Route::post('/item-rating','ItemRatingController@store')->name('item_rating_store');
    Route::get('/merit-qualification/{id_call}','MeritQualificationController@index')->name('merit_qualification');
    Route::post('/merit-qualification','MeritQualificationController@store')->name('merit_qualification_store');

    Route::get('/qualification-knowledge/{required_request_id}','QualificationController@index')->name('qualification_k');

    /* routes for selected call */
    Route::get('/seecall/{call_id}','SelectedCallController@index')->name('seecall');

    /*public a call*/
    Route::PUT('public_call/{call_id}','CallController@publicCall')->name('public_call');

    Route::get('/detail-commission/{commission_id}','CommissionController@show')->name('commission_show');
});


Route::get('/pdf/{call_id}','PdfController@create_call')->name('pdf_download');
Route::get('/pdf_list/{call_id}','PdfController@create_list_enabled_students')->name('pdf_list_download');
Route::get('/pdf_knowledge_list_docencia/{call_id}/{id_auxiliary}','PdfController@create_knowledge_list_docencia')->name('pdf_knowledge_results_list_doc');
Route::get('/pdf_knowledge_list_laboratorio/{call_id}/{id_auxiliary}','PdfController@create_knowledge_list_laboratorio')->name('pdf_knowledge_results_list_lab');
Route::get('/pdf_end_list/{call_id}/{id_auxiliary}/{type_call}','PdfController@create_end_note_list_download')->name('pdf_end_note');
