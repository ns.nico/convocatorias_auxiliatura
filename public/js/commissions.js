let commission_type = document.getElementById('commission_type')
let div_items = document.getElementById('items')
reloadItems()

function reloadItems(){
    if( div_items != null && commission_type.value == 1){
        div_items.style.display = 'none'
    }
    else if(div_items != null && commission_type.value != 1){
        div_items.style.display = 'block'
    }
}

/**
 * events for secretary in register reception book
 */

let applicant_selected = document.getElementById('applicant')
if(applicant_selected != null){
    div_selected = document.getElementById(applicant_selected.value)
    if(div_selected != null){
        div_selected.style.display = 'block'
    }
}

function onChangeApplicants(applicants,selected){
    if(applicants != null){
        applicants.forEach(app => {
            element_div = document.getElementById(app.id);
            if(app.id == selected){
                element_div.style.display = 'block'
            }
            else element_div.style.display = 'none'
        });
    }
}

function onChangeRole(role){
    let div_codeSis = document.getElementById('codeSis');
    let div_ci = document.getElementById('ci');
    if(role == 4){
        div_codeSis.style.display = 'block';
        div_ci.style.display = 'block';
    }
    else{
        div_codeSis.style.display = 'none';
        div_ci.style.display = 'none';
    }
    let input_ci = document.getElementById('CI');
    let input_codeSis = document.getElementById('codigoSIS');
    input_ci.value = '';
    input_codeSis.value = '';
}

function setDisabledRadioBtn( element ){
    let radio = document.getElementById('leader'+element.value);
    if(radio.checked)  radio.checked = false;
    element.checked ? radio.disabled = false : radio.disabled = true
}