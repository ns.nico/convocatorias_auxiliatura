function off(id_div,array_name,id){
    var div_element = $('#'+ id_div);
    div_element.parent().parent().find('#rejected').append(div_element);
    div_element.find('.close i').removeClass('icon-minus');
    div_element.find('.close i').addClass('icon-plus');
    div_element.find('.close i').removeClass('icon-mdf-s');
    div_element.find('.close i').addClass('icon-mdf-e');
    div_element.removeClass('alert-success');
    div_element.addClass('alert-error');
    div_element.removeAttr('onclick');
    div_element.attr('onClick', 'on("'+id_div+'","'+array_name+'","'+id+'")');
    div_element.find('input').remove();
};

function on(id_div,array_name,id){
    var div_element = $('#'+ id_div);
    div_element.parent().parent().find('#accepted').append(div_element);
    div_element.find('.close i').removeClass('icon-plus');
    div_element.find('.close i').addClass('icon-minus');
    div_element.find('.close i').removeClass('icon-mdf-e');
    div_element.find('.close i').addClass('icon-mdf-s');
    div_element.removeClass('alert-error');
    div_element.addClass('alert-success');
    div_element.removeAttr('onclick');
    div_element.attr('onClick', 'off("'+id_div+'","'+array_name+'","'+id+'")');
    div_element.append('<input type="hidden" name="'+array_name+'[]" value="'+id+'">');
};

var id_field_date = 100;
function addFieldDate(e){
    $("#datesCalls").append(
        fieldDates()
    );
    id_field_date=id_field_date+2;
}
function fieldDates(){
    let id_field_end = id_field_date+1
    today = get_today();
    return '<div>'+
        '<div class="span2"><label for="">Evento</label><input class="input-block-level" list="list_events" type="text" name="dates[]" required></input></div>'+
        '<div class="span2"><label for="">Fecha Inicio</label><input id="'+id_field_date+'" class="input-block-level" type="date" name="dates[]" min="'+today+'" onChange="update_date_min(event);" required></input></div>'+
        '<div class="span2"><label for="">Fecha Fin</label><input id="'+ id_field_end +'"class="input-block-level" type="date" name="dates[]" min="'+today+'" onChange="update_date_max(event);" required></input></div>'+
        '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
    '</div>'
}

function get_today(){
  let date = new Date();
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = date.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function update_date_min(event){
  console.log(event.target.id);
  console.log(parseInt(event.target.id, 10)+1);
  let input = document.getElementById(parseInt(event.target.id, 10)+1);
  input.setAttribute("min", event.target.value);
}

function update_date_max(event){
  console.log(event.target.id);
  console.log(parseInt(event.target.id,10)-1);
  let input = document.getElementById(parseInt(event.target.id,10)-1);
  input.setAttribute("max", event.target.value);
}

function nextItem(){
  var ul_element = document.getElementsByClassName('cat')[0];
  for(var i=0;i<ul_element.children.length;i++){
    var a_element = ul_element.children[i].children[0]
    if(a_element.className == 'selected-a'){
      a_element.classList.remove('selected-a')
      ul_element.children[i].classList.remove('active')
      var next_element = ul_element.children[i].nextElementSibling
      next_element.classList.add('active')
      var a_next_element = next_element.children[0]
      a_next_element.classList.add('selected-a')
      break
    }
  }
}

function prevItem(){
    var ul_element = document.getElementsByClassName('cat')[0];
    for(var i=0;i<ul_element.children.length;i++){
      var a_element = ul_element.children[i].children[0]
      if(a_element.className == 'selected-a'){
        a_element.classList.remove('selected-a')
        ul_element.children[i].classList.remove('active')
        var next_element = ul_element.children[i].previousElementSibling
        next_element.classList.add('active')
        var a_next_element = next_element.children[0]
        a_next_element.classList.add('selected-a')
        break
      }
    }
  }

  function back(){
    history.back(-1);
  }